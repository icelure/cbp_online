<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Forgot_password extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct() {
        parent::__construct();
        
        if(isset($this->session->userdata('user')->logged_in) && $this->session->userdata('user')->logged_in == true) {
        	redirect(base_url().'plans');
        }
        $this->load->model('User_model');
    }

	public function index()
	{
		if(isset($_POST['loginForm']) && $_POST['loginForm']=="postForm"){
			$this->doLogin();
		}
		$data['page'] = $this->uri->segment(1);
		$this->load->template_top_nav('Forgot_password',$data);
	}

	private function doLogin(){
		
		
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required');
		

		if ($this->form_validation->run() == FALSE)
        {
            
        }
        else
        {
        	
            $user_info = $this->User_model->loginProcedure($this->input->post('email'),$this->input->post('password'));

            if(!empty($user_info))
      		{
      			if($user_info->status=='a'){

      				unset($user_info->password);
      				$user_info->logged_in = true;
      				$this->session->set_userdata('user', $user_info);
      				
      				$response=array(
      					'status'=>'success',
      					'message' => "You've logged in successfully"
      				);
      				$this->session->set_flashdata('response', $response);
      				redirect(base_url().'Plans');

      			}else{
      				$response=array(
      					'status'=>'failed',
      					'message' => 'Account not active' 
      				);
      				$this->session->set_flashdata('response', $response);
      			}

      		}else{
      			$response=array(
      					'status'=>'failed',
      					'message' => 'Invalid email or password'
      			);
      			$this->session->set_flashdata('response', $response);
      		}
      		redirect(base_url().'login');
        }
	}

	public function email_exists($email){
		return $this->User_model->checkEmailExist($email);
	}
	
	public function verify($code){
		if($code !== ""){
			$check_user = $this->User_model->verify_account($code);

			if($check_user){
				$response=array(
					'status'=>'failed',
					'message' => 'You have successfully created an account,please check your mail for instructions to activate your account'
				);
				$this->session->set_flashdata('response', $response);
	      	}else{
	      		$response=array(
					'status'=>'failed',
					'message' => 'Sorry this link has been expired'
				);
				$this->session->set_flashdata('response', $response);
	      	}
	      	redirect(base_url().'register');
		}
	}
}
