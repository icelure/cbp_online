<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Expenses extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct() {
		parent::__construct();
		$this->load->model('Expenses_model');
		if(!isset($this->session->userdata('user')->logged_in) || $this->session->userdata('user')->logged_in !== true) {
			redirect(base_url().'login');
		}
		$this->load->model('profile_model');

	}

	public function index()
	{
		$data['user'] = $this->profile_model->get_detail_by_id($this->session->userdata('user')->id);
		$data['page'] = $this->uri->segment(1);
		$var = $this->session->userdata;
		$user_id = $var['user']->id;
		$list = $this->Expenses_model->get_all_expenses($user_id);
		$expense_summary = $this->Expenses_model->get_expense_summary($user_id);
		$cost_increase_percentage = $this->Expenses_model->get_cost_increase($user_id);
		$total_expenses = $this->Expenses_model->get_total_expense($user_id);
		$data['list']=$list;
		$data['expense_summary'] = $expense_summary;
		$data['cost_increase_percentage'] = $cost_increase_percentage;
		$data['total'] = $total_expenses;

		$this->load->template_left_nav('expenses_view',$data);

	}
	public function ajax_list()
	{
		$var = $this->session->userdata;
		$user_id = $var['user']->id;
		$list = $this->Expenses_model->get_all_expenses($user_id);
        
        
        // Currency
        $query = $this->db->query('select currency from company_detail where user_id="' . $user_id . '"');
        $currency = $query->row()->currency;
        
        $cur = '';
        
        if ($currency == "AUD" || $currency == "USD") {
            $cur = "$";
        } else if ($currency == "EUR") {
            $cur = "€";
        } else if ($currency == "INR") {
            $cur = "₹";
        }
        // Currency        
        
        $data = array();
		$no = 0;
		foreach ($list as $expense) {
			$no++;

			$row = array();
			$row[] = $expense['id'];
			$row[] = $expense['description'];
			$row[] = '$'.$expense['weekly_cost'];
			$row[] = '$'.$expense['monthly_cost'];
			$row[] = '$'.$expense['quarterly_cost'];
			$row[] = '$'.$expense['yearly_cost'];
			//add html for action
			$data[] = $row;
		}

		$output = array(
			"draw" => NULL,
			"recordsTotal" => $this->Expenses_model->count_all(),
			"recordsFiltered" => $this->Expenses_model->count_filtered(),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}
	public function ajax_add()
	{
		$var = $this->session->userdata;
		$user_id = $var['user']->id;
		$data = array(
			'user_id' => $user_id,
			'description' => $this->input->post('description'),
			'weekly_cost' => $this->input->post('weekly_cost'),
			'monthly_cost' => $this->input->post('monthly_cost'),
			'quarterly_cost' => $this->input->post('quarterly_cost'),
			'yearly_cost' => $this->input->post('yearly_cost'),
			'purpose' => $this->input->post('purpose')
		);
		$insert = $this->Expenses_model->save($data);
		echo json_encode(array("status" => TRUE));
	}
	public function ajax_edit($id)
	{
		$data = $this->Expenses_model->get_by_id($id);
		echo json_encode($data);
	}
	public function ajax_update()
	{
		$var = $this->session->userdata;
		$user_id = $var['user']->id;
		$data = array(
			'user_id' => $user_id,
			'description' => $this->input->post('description'),
			'weekly_cost' => $this->input->post('weekly_cost'),
			'monthly_cost' => $this->input->post('monthly_cost'),
			'quarterly_cost' => $this->input->post('quarterly_cost'),
			'yearly_cost' => $this->input->post('yearly_cost'),
			'purpose' => $this->input->post('purpose')
		);
		$this->Expenses_model->update(array('id' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}
	public function ajax_delete($id)
	{
		$this->Expenses_model->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}

	function ajax_expense_summary(){
		$var = $this->session->userdata;
		$user_id = $var['user']->id;
		$expense_summary = $this->Expenses_model->get_expense_summary($user_id);
		$cost_increase_percentage = $this->Expenses_model->get_cost_increase($user_id);
		$total_expenses = $this->Expenses_model->get_total_expense($user_id);
		$data['expense_summary'] = $expense_summary;
		$data['cost_increase_percentage'] = $cost_increase_percentage;
		$data['total'] = $total_expenses[0];
		
		echo json_encode($data);

	}

	function updateExpensesIncrease(){
		$var = $this->session->userdata;
		$user_id = $var['user']->id;

		$data = array(
			'user_id' => $user_id,
			'marketing_increase' => $this->input->get('m'),
			'public_reactions' => $this->input->get('p'),
			'administration_cost' => $this->input->get('a'),
			'other_increase' => $this->input->get('o')
			
		);
		$this->Expenses_model->updateExpenseIncrease($data);

		echo "1";
		exit;

	} 
}
