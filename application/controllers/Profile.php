<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

    public function __construct() {
        parent::__construct();

        if (!isset($this->session->userdata('user')->logged_in) || $this->session->userdata('user')->logged_in !== true) {
            redirect(base_url() . 'login');
        }
        $this->load->model('profile_model');
    }

    public function index(){

    	if(isset($_POST['editForm']) && $_POST['editForm']=="postForm"){
			$this->doEdit();
		}

		$this->load->model('Company_setup_model');
		$data['company_detail'] = $this->Company_setup_model->get_company_detail();

		$data['user'] = $this->profile_model->get_detail_by_id($this->session->userdata('user')->id);

    	$data['page'] = $this->uri->segment(1);
    	$this->load->template_left_nav('Profile_view',$data);
    }

    private function doEdit(){

    	$this->form_validation->set_rules('f_name', 'First Name', 'trim|required');
		$this->form_validation->set_rules('l_name', 'Last Name', 'trim|required');
		$this->form_validation->set_rules('phone', 'Business Phone', 'trim|required|is_natural');
		$this->form_validation->set_rules('country', 'Country', 'trim|callback__jsselect');
		$this->form_validation->set_rules('avatar-1', 'Photo', 'callback__image_upload');

		if ($this->form_validation->run() == TRUE){

			$id = $this->session->userdata('user')->id;

			$file = $this->avatar['file_name'];

			$photo_data = array('photo'=>$file);

			$data = array(
				'f_name'=>$this->input->post('f_name'),
				'l_name'=>$this->input->post('l_name'),
				'email'=>$this->input->post('email'),
				'phone'=>$this->input->post('phone'),
				'country'=>$this->input->post('country'),
			);

			$full_data = array_merge($data, $photo_data);

			$this->profile_model->edit_profile($full_data, $id);

			$response=array(
				'status'=>'success',
				'message' => "Changes already been saved !!!"
			);
			$this->session->set_flashdata('response', $response);
      				
		}else{

			$this->session->set_flashdata(array(

				'f_name'=>form_error('f_name'),
				'l_name'=>form_error('l_name'),
				'email'=>form_error('email'),
				'phone'=>form_error('phone'),
				'country'=>form_error('country'),
				'avatar-1'=>form_error('avatar-1'),
			));

		}

		redirect(base_url().'profile');

    }

    public function _image_upload(){

		if ($_FILES['avatar-1']['size'] != 0){

            $config['upload_path'] = 'assets/uploads/users/';
		 	$config['file_name'] = $_FILES['avatar-1']['name'];
	        $config['allowed_types'] = 'gif|jpg|png|jpeg';
	        $config['max_size'] = '500';
	        $config['overwrite'] = TRUE;
	        $config['max_width'] = '120';
			$config['max_height'] = '120';

			//Load upload library and initialize configuration
            $this->load->library('upload',$config);


			if (!$this->upload->do_upload('avatar-1')) {

				$this->form_validation->set_message('_image_upload', $this->upload->display_errors());

				
				return false;

			}else{

				$newimage = $_FILES['avatar-1']['name'];

				$data['user'] = $this->profile_model->get_detail_by_id($this->session->userdata('user')->id);
            	$oldimage = $data['user']->photo;

	            if ($newimage != $oldimage){

	            	if ($oldimage != ""){

	            		unlink('assets/uploads/users/'.$oldimage);
	            	}


	            }

				$this->avatar =  $this->upload->data();

				return true;
			}

		}else{

			$data['user'] = $this->profile_model->get_detail_by_id($this->session->userdata('user')->id);


			$this->avatar = array('file_name'=> (string) $data['user']->photo);
		}

	}

    public function _jsselect($field){

		if (is_numeric($field) || empty($field)) {

			$this->form_validation->set_message('_jsselect', 'Field %s is required');

			return false;

		}

		return true;
	}

    public function restoreDefault($id){

		$user = $this->profile_model->get_detail_by_id($id);

		$photo_data = array(
            'photo'=>""
        );


		if ($user->photo != ""){

			unlink($_SERVER['DOCUMENT_ROOT'].'/cbp_online/assets/uploads/users/'.$user->photo);

		}

		$this->profile_model->edit_profile($photo_data, $id);

		$response=array(
			'status'=>'success',
			'message' => "Photo has been delete !!!"
		);
		$this->session->set_flashdata('response', $response);

	}

}
?>