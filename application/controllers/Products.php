<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends CI_Controller {

    public function __construct() {
        parent::__construct();

        if (!isset($this->session->userdata('user')->logged_in) || $this->session->userdata('user')->logged_in !== true) {
            redirect(base_url() . 'login');
        }
        $this->load->model('Product_model', 'product');

        $this->load->model('profile_model');
    }

    public function index() {
        // echo "test";

        $data['page'] = $this->uri->segment(1);
        $this->load->model('Company_setup_model');
        $this->db->where(array('user_id'=>$this->session->userdata('user')->id));
        $data['company_detail'] = $this->Company_setup_model->get_company_detail();

        $data['user'] = $this->profile_model->get_detail_by_id($this->session->userdata('user')->id);
        //     var_dump($data['page']);
        $query = $this->db->query('select currency from company_detail where user_id="' . $data['user']->id . '"');
        $data['currency'] = $query->row()->currency;

        $this->load->template_left_nav('Products', $data);
    }

    public function ajax_list() {
        $list = $this->product->get_datatables();
        $var = $this->session->userdata;
        $user_id = $var['user']->id;
        $query = $this->db->query('select sales_income_increase from general_assumption where user_id="' . $user_id . '"');
        $Service_income = $query->row()->sales_income_increase;
        if ($Service_income === NULL) {
            $Service_income = 0;
        }
        // Currency
        $query = $this->db->query('select currency from company_detail where user_id="' . $user_id . '"');
        $currency = $query->row()->currency;

        $cur = '';

        if ($currency == "AUD" || $currency == "USD") {
            $cur = "$";
        } else if ($currency == "EUR") {
            $cur = "€";
        } else if ($currency == "INR") {
            $cur = "₹";
        }
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $product) {
            $no++;
            $Total = $product->Qty * $product->UnitCost;
            $row = array();
            $row["localproductid"] = $product->id;
            $row["localproductthumbnail"] = $product->ThumbNail; //'<a href=""><img class="img" src=' . $product->ThumbNail . ' width="50" height="50">';
            $row["localproductdescription"] = $product->Description;
            $row["qty"] = $product->Qty;
            $row["unitcost"] = $cur . $product->UnitCost;

            $row["totalcost"] = $cur . $Total;
            $row["markup"] = $product->MarkUpOnCost;
            $row["rrr"] = $cur . ($product->UnitCost * $product->MarkUpOnCost / 100) + $product->UnitCost; //(($Total * $product->MarkUpOnCost) / 100) + $Total;
            $row["totalweeklyrevenue"] = $cur . $product->Qty * (($product->UnitCost * $product->MarkUpOnCost / 100) + $product->UnitCost);
            //add html for action
//            $row[] = '<a  class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_product(' . "'" . $product->id . "'" . ')"><i class="glyphicon glyphicon-pencil"></i></a>
//                      <a  class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_localproduct(' . "'" . $product->id . "'" . ')"><i class="glyphicon glyphicon-trash"></i></a>';

            $data[] = $row;
        }
        // echo $data[];
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->product->count_all(),
            "recordsFiltered" => $this->product->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function localproductsummary() {

        $var = $this->session->userdata;
        $user_id = $var['user']->id;
        $query = $this->db->query('select sales_income_increase from general_assumption where user_id="' . $user_id . '"');
        $Service_income = $query->row()->sales_income_increase;
        if ($Service_income === NULL) {
            $Service_income = 0;
        }
        // Currency
        $query = $this->db->query('select currency from company_detail where user_id="' . $user_id . '"');
        $currency = $query->row()->currency;

        $cur = '';

        if ($currency == "AUD" || $currency == "USD") {
            $cur = "$";
        } else if ($currency == "EUR") {
            $cur = "€";
        } else if ($currency == "INR") {
            $cur = "₹";
        }

        $list = $this->product->get_datatables();

        $data = array();
        $no = $_POST['start'];
        $sumTotal = 0;
        $totalQty = 0;
        foreach ($list as $product) {
            $no++;
            $row = array();
            $row[] = '<img src=' . $product->ThumbNail . ' width="50" height="50" class="img-circle">'; //$product->ThumbNail;
            $row[] = $product->id;
            $row[] = $product->Description;
            $row[] = $product->Qty;
            $totalQty += (int) $product->Qty;
            $row[] = $product->UnitCost;
            $row[] = $cur . number_format($product->Qty * $product->UnitCost, 0); // Total
            $row[] = $product->MarkUpOnCost.'%';
            $row[] = $cur . number_format(($product->Qty * $product->UnitCost)*$product->MarkUpOnCost/100, 0); //RRP 
            $row[] = $cur . number_format($product->Qty * $product->UnitCost, 0); // Total Cost
            $row[] = $cur . number_format((($product->Qty * $product->UnitCost)*$product->MarkUpOnCost/100)*($product->Qty * $product->UnitCost),0);//Total Revenue
            /*$Weekly = $product->Qty * (($product->UnitCost * $product->MarkUpOnCost / 100) + $product->UnitCost);
            $row[] = $cur . number_format($Weekly);

            $row[] = $cur . number_format(round((($Weekly * 52) / 12), 0)); // Monthly
            $row[] = $cur . number_format(($Weekly * 52) / 4); // Quaterly
            $row[] = $cur . number_format($Weekly * 52);    // Year1
            $row[] = number_format($Weekly * 52);   // Year2
            $row[] = number_format($Weekly * 52);   // Year3
            $sumTotal += (int) ($Weekly * 52);*/
            //add html for action

            $data[] = $row;
        }
        // echo $data[];
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->product->count_all(),
            "recordsFiltered" => $this->product->count_filtered(),
            "data" => $data,
            "Service_income" => $Service_income,
            "sumTotal" => number_format($sumTotal),
            "currency" => $cur,
            "totalQty"=>$totalQty
        );
        //output to json format
        echo json_encode($output);
    }

    public function update_sales_income_increase() {
        $sincome = $_POST['si'];
        //$email = $this->session->userdata('user')->email;
        $var = $this->session->userdata;
        $id = $var['user']->id;
        $query1 = $this->db->query('update general_assumption set sales_income_increase="' . $sincome . '" where user_id="' . $id . '"');
        if (!$query1['error']) {
            $result = array('status' => 'yes');
            echo json_encode($result, TRUE);
        } else {
            $result = array('status' => 'no');
            echo json_encode($result, TRUE);
        }
    }

    public function ajax_edit($id) {
        $data = $this->product->get_by_id($id);
        echo json_encode($data);
    }

    public function ajax_add() {

        $var = $this->session->userdata;
        $user_id = $var['user']->id;

        $maxid = 0;

        $row = $this->db->query('SELECT MAX(id) AS `maxid` FROM `localproduct`')->row();
        $ThumbNail;
        if ($row) {
            $maxid = $row->maxid + 1;
        }

        if (!($_FILES["flnFile"]["name"] == NULL)) {
            $target_dir = "local_product_thumbnails/";
            if (!file_exists($target_dir)) {
                mkdir($target_dir);
            }

            $target_file = $target_dir . $user_id . "_" . $maxid . "_" . basename($_FILES["flnFile"]["name"]);
            $uploadOk = 1;
            $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
            $check = getimagesize($_FILES["flnFile"]["tmp_name"]);
            if ($check !== false) {
                //echo "File is an image - " . $check["mime"] . ".";
                $uploadOk = 1;
            } else {
                //echo "File is not an image.";
                $uploadOk = 0;
            }
            if ($_FILES["flnFile"]["size"] > 500000) {
                //echo "Sorry, your file is too large.";
                $uploadOk = 0;
            }
            if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
                //echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                $uploadOk = 0;
            }

            if ($uploadOk == 0) {
                //echo "Sorry, your file was not uploaded.";
            } else {
                if (move_uploaded_file($_FILES["flnFile"]["tmp_name"], $target_file)) {
                    //echo "The file " . basename($_FILES["flnFile"]["name"]) . " has been uploaded.";
                } else {
                    //echo "Sorry, there was an error uploading your file.";
                }
                $ThumbNail = $target_file;
            }
        } else {
            $ThumbNail = 'assets/images/no_image.png';
        }
        $data = array(
            'Description' => $this->input->post('product_description'),
            'Qty' => $this->input->post('Quantity'),
            'UnitCost' => $this->input->post('unit_cost'),
            'MarkUpOnCost' => $this->input->post('markup_on_cost'),
            'weekly_qty'=>$this->input->post('weekly_qty'),
            'monthly_qty'=>$this->input->post('monthly_qty'),
            'yearly_qty'=>$this->input->post('yearly_qty'),
            'ThumbNail' => $ThumbNail,
        );
        $insert = $this->product->save($data);
        echo json_encode(array('status' => TRUE));
    }

    public function ajax_update() {

        $id = $this->input->post('id');
        $ThumbnailPath = $this->db->query('SELECT ThumbNail  FROM `localproduct` where id = ' . $id)->row();
        $data;
        if (!($_FILES["flnFile"]["name"] == NULL)) {
            $var = $this->session->userdata;
            $user_id = $var['user']->id;

            $target_dir = "local_product_thumbnails/";
            if (!file_exists($target_dir)) {
                mkdir($target_dir);
            }
            $target_file = $target_dir . $user_id . "_" . $id . "_" . basename($_FILES["flnFile"]["name"]);

            $uploadOk = 1;
            $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
            $check = getimagesize($_FILES["flnFile"]["tmp_name"]);
            if ($check !== false) {
                //echo "File is an image - " . $check["mime"] . ".";
                $uploadOk = 1;
            } else {
                //echo "File is not an image.";
                $uploadOk = 0;
            }
            if ($_FILES["flnFile"]["size"] > 500000) {
                //echo "Sorry, your file is too large.";
                $uploadOk = 0;
            }
            if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
                //echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                $uploadOk = 0;
            }

            if ($uploadOk == 0) {
                //echo "Sorry, your file was not uploaded.";
            } else {

                unlink($ThumbnailPath);
                $ThumbnailPath = $target_file;
                if (move_uploaded_file($_FILES["flnFile"]["tmp_name"], $target_file)) {
                    //echo "The file " . basename($_FILES["flnFile"]["name"]) . " has been uploaded.";
                } else {
                    //echo "Sorry, there was an error uploading your file.";
                }
            }

            $data = array(
                'Description' => $this->input->post('product_description'),
                'Qty' => $this->input->post('Quantity'),
                'UnitCost' => $this->input->post('unit_cost'),
                'MarkUpOnCost' => $this->input->post('markup_on_cost'),
                'weekly_qty'=>$this->input->post('weekly_qty'),
                'monthly_qty'=>$this->input->post('monthly_qty'),
                'yearly_qty'=>$this->input->post('yearly_qty'),
                'ThumbNail' => $target_file,
            );
        } else {

            $data = array(
                'Description' => $this->input->post('product_description'),
                'Qty' => $this->input->post('Quantity'),
                'UnitCost' => $this->input->post('unit_cost'),
                'MarkUpOnCost' => $this->input->post('markup_on_cost'),
                'weekly_qty'=>$this->input->post('weekly_qty'),
                'monthly_qty'=>$this->input->post('monthly_qty'),
                'yearly_qty'=>$this->input->post('yearly_qty'),
            );
        }

        $this->product->update(array('id' => $this->input->post('id')), $data);
        echo json_encode(array("status" => TRUE));
    }

    public function ajax_delete($id) {
        $this->product->delete_by_id($id);
        $ThumbnailPath = $this->db->query('SELECT ThumbNail  FROM `localproduct` where id = ' . $id)->row();
        unlink('local_product_thumbnails/' . $ThumbnailPath);
        echo json_encode(array("status" => TRUE));
    }

}
