<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'libraries/swift_mailer/swift_required.php';
class Register extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct() {
        parent::__construct();
        if($this->uri->segment(1)!=='logout'){
        	if(isset($this->session->userdata('user')->logged_in) && $this->session->userdata('user')->logged_in == true) {
        	redirect(base_url().'plans');
       	 }
        }
        $this->load->model('User_model');
    }

	public function index()
	{
		if(isset($_POST['registerForm']) && $_POST['registerForm']=="postForm"){
			$this->doRegister();
		}
		$data['page'] = $this->uri->segment(1);
		$this->load->template_top_nav('Register',$data);
	}

	private function doRegister(){

		$this->form_validation->set_rules('f_name', 'First Name', 'trim|required');
		$this->form_validation->set_rules('l_name', 'Last Name', 'trim|required');
		$this->form_validation->set_rules('email', 'Email Address', 'trim|required|callback_email_exists');
		$this->form_validation->set_message('email_exists', 'Email Address already exists');
		$this->form_validation->set_rules('phone', 'Business Phone', 'trim|required|is_natural');
		$this->form_validation->set_rules('country', 'Country', 'trim|callback__jsselect');
		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|matches[confirm_password]');
		$this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|matches[password]');


		if ($this->form_validation->run() == TRUE){

			$token = md5(time());

			$data = array(

				'f_name'=>$this->input->post('f_name'),
				'l_name'=>$this->input->post('l_name'),
				'email'=>$this->input->post('email'),
				'phone'=>$this->input->post('phone'),
				'country'=>$this->input->post('country'),
				'username'=>$this->input->post('username'),
				'password'=>md5($this->input->post('password')),
				'token'=>$token

			);

			//do database save

			$uid = $this->User_model->insert_user($data);

			//================

			if($uid > 0)
      		{
      			$template_data=array(
      				"name" => $this->input->post('f_name').$this->input->post('l_name'),
      				"confirmation_link" => "http://organicstem.com/verify_to_cbp.php?token=".$token
      			);

      			$mail_content= $this->load->view('email_template/email_confirmation',$template_data,true);

            $subject = "Activate your account";

            $this->send_switft_mail($subject,$mail_content,$this->input->post('email'));

  				$response=array(
  					'status'=>'success',
  					'message' => 'You have successfully created an account,please check your mail for instructions to activate your account'
  				);
  				$this->session->set_flashdata('response', $response);

      		}else{
      			$response=array(
      					'status'=>'failed',
      					'message' => 'There is some problem in creating your account,please try again later.'
      			);
      			$this->session->set_flashdata('response', $response);
      		}


		}else{

			$this->session->set_flashdata(array(

				'f_name'=>form_error('f_name'),
				'l_name'=>form_error('l_name'),
				'email'=>form_error('email'),
				'phone'=>form_error('phone'),
				'country'=>form_error('country'),
				'username'=>form_error('username'),
				'password'=>form_error('password'),
				'confirm_password'=>form_error('confirm_password'),

			));

		}

		redirect(base_url().'register');

		/*$this->form_validation->set_rules('name', 'Full Name', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		$this->form_validation->set_rules('cpassword', 'Password Confirmation', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|callback_email_exists');
		$this->form_validation->set_message('email_exists', 'Email already exists');

		if ($this->form_validation->run() == FALSE)
        {

        }
        else
        {
        	$token = md5(time());

            $data= array(
            	"name" => $this->input->post('name'),
            	"email" => $this->input->post('email'),
            	"password" => md5($this->input->post('password')),
            	"token" => $token
            );

            $uid = $this->User_model->insert_user($data);

            if($uid > 0)
      		{
      			$template_data=array(
      				"name" => $this->input->post('name'),
      				"confirmation_link" => "http://organicstem.com/verify_to_cbp.php?token=".$token
      			);

      			$mail_content= $this->load->view('email_template/email_confirmation',$template_data,true);

            $subject = "Activate your account";

            $this->send_switft_mail($subject,$mail_content,$this->input->post('email'));

  				$response=array(
  					'status'=>'success',
  					'message' => 'You have successfully created an account,please check your mail for instructions to activate your account'
  				);
  				$this->session->set_flashdata('response', $response);

      		}else{
      			$response=array(
      					'status'=>'failed',
      					'message' => 'There is some problem in creating your account,please try again later.'
      			);
      			$this->session->set_flashdata('response', $response);
      		}
      		redirect(base_url().'register');
        }*/
	}

	public function _jsselect($field){

		if (is_numeric($field) || empty($field)) {

			$this->form_validation->set_message('_jsselect', 'Field %s is required');

			return false;

		}

		return true;
	}

	public function email_exists($email){
		return $this->User_model->checkEmailExist($email);
	}

	public function verify($code){
		if($code !== ""){
			$check_user = $this->User_model->verify_account($code);

			if($check_user){
				$response=array(
					'status'=>'success',
					'message' => 'Your account is now active,login to continue'
				);
				$this->session->set_flashdata('response', $response);
	      	}else{
	      		$response=array(
					'status'=>'failed',
					'message' => 'Sorry this link has been expired'
				);
				$this->session->set_flashdata('response', $response);
	      	}
	      	redirect(base_url().'login');
		}
	}

	public function logout(){
		session_destroy();
		redirect(base_url().'login');
	}

  public function send_switft_mail($subject,$mail_content,$email){

    $mailer = new Swift_Mailer(
      Swift_SmtpTransport::newInstance('box1085.bluehost.com',465,'ssl')
        ->setUsername('info@completebusinessplans.com')
        ->setPassword('Info@123')
      );

    $message = Swift_Message::newInstance($subject)
      ->setFrom(array('info@completebusinessplans.com' => 'CBP Online'))
      ->setTo(array($email => 'CBP Online'))
      ->setBody($mail_content, 'text/html');


    if(!$mailer->send($message)){
      echo "error";
    }
    }
}