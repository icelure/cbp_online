<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'libraries/swift_mailer/swift_required.php';

class Login extends CI_Controller {
	function __construct() {
        parent::__construct();

        /*if(isset($this->session->userdata('user')->logged_in) && $this->session->userdata('user')->logged_in == true) {

        	redirect(base_url().'plans');
        }*/

        $this->load->model('User_model');
    }

	public function index()
	{

		if(isset($_POST['loginForm']) && $_POST['loginForm']=="postForm"){
			$this->doLogin();
		}
		if(isset($_POST['fpForm']) && $_POST['fpForm']=="postfpForm"){
			$this->forgotpass();
		}
		if(isset($_POST['rpForm']) && $_POST['rpForm']=="postrpForm"){
			$this->resetpass();
		}

		$data['page'] = $this->uri->segment(1);
		$this->load->template_top_nav('Login',$data);
	}

	private function doLogin(){
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required');


		if ($this->form_validation->run() == FALSE)
        {

        }
        else
        {

            $user_info = $this->User_model->loginProcedure($this->input->post('email'),$this->input->post('password'));

            if(!empty($user_info))
      		{
      			if($user_info->status=='a' && $user_info->isActive=='y'){

      				unset($user_info->password);
      				$user_info->logged_in = true;
      				$this->session->set_userdata('user', $user_info);

      				$response=array(
      					'status'=>'success',
      					'message' => "You've logged in successfully"
      				);
      				$this->session->set_flashdata('response', $response);

      				$curdate=strtotime(date('Y-m-d H:i:s'));
      				$plan_end_date = strtotime($user_info->plan_end_date);

      				if($user_info->type == 'a'){
      					redirect(base_url().'Dashboard');
      				}
      				elseif(($user_info->subscribed == 'y' && $curdate < $plan_end_date)){
      					redirect(base_url().'User_dashboard');
      				}else{
      					redirect(base_url().'Plans');
      				}


      			}else{
      				$response=array(
      					'status'=>'failed',
      					'message' => 'Account not active'
      				);
      				$this->session->set_flashdata('response', $response);
      			}

      		}else{
      			$response=array(
      					'status'=>'failed',
      					'message' => 'Invalid email or password'
      			);
      			$this->session->set_flashdata('response', $response);
      		}
      		redirect(base_url().'login');
        }
	}

	public function email_exists($email){
		return $this->User_model->checkEmailExist($email);
	}

	public function verify($code){
		if($code !== ""){
			$check_user = $this->User_model->verify_account($code);

			if($check_user){
				$response=array(
					'status'=>'failed',
					'message' => 'You have successfully created an account,please check your mail for instructions to activate your account'
				);
				$this->session->set_flashdata('response', $response);
	      	}else{
	      		$response=array(
					'status'=>'failed',
					'message' => 'Sorry this link has been expired'
				);
				$this->session->set_flashdata('response', $response);
	      	}
	      	redirect(base_url().'register');
		}
	}

	public function forgotpass(){
		$this->form_validation->set_rules('fp_email', 'Email', 'trim|required');
		if ($this->form_validation->run() == FALSE)
        {

        }
        else
        {
        	$uid = $this->User_model->get_user_by_email($this->input->post('fp_email'));

        	if($uid > 0 && $uid !== false){
        		$token = md5(time());
        		$data = array(
        			"token" => $token
        		);

        		if($this->User_model->update_token($uid,$data)){
        			$template_data=array(
      				"reset_link" => "http://organicstem.com/redirect_to_cbp.php?email=".urlencode($this->input->post("fp_email"))."&token=".$token
	      			);

	      			$mail_content= $this->load->view('email_template/forgot_password',$template_data,true);

	      			$subject = "Forgot your password";

	      			$this->send_switft_mail($subject,$mail_content,$this->input->post('fp_email'));

	      			$response=array(
					'status'=>'success',
					'message' => 'Plese check your mail for instructions to reset your password.'
					);
        		}

        	}else{
        		$response=array(
					'status'=>'failed',
					'message' => 'No account associated with this email address or account is inactive.'
				);

        	}
        	$this->session->set_flashdata('response', $response);
        	redirect(base_url().'login');
        }
	}

	public function reset_password($email,$token){
		if($email !== "" && $token !== ""){
			$data = array(
			"email" => $email,
			"token" => $token
			);
			$data['page'] = $this->uri->segment(1);
			$this->load->template_top_nav('Reset_password',$data);
		}else{
			redirect(base_url().'login');
		}
	}

	public function resetpass(){
		$email = urldecode($this->input->post('email'));
		$token = $this->input->post('token');
		$password = $this->input->post('new_password');
		$this->form_validation->set_rules('new_password', 'Password', 'trim|required|matches[confirm_password]');
		$this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|matches[new_password]');

		if ($this->form_validation->run() == FALSE)
        {
        	$response=array(
						'status'=>'failed',
						'message' => validation_errors(),
				);
        	$this->session->set_flashdata('response', $response);
            redirect(base_url().'reset_password/'.urlencode($email).'/'.$token);
        }
        else
        {
        	$uid = $this->User_model->get_user_by_email($email);
        	if($uid > 0){
        		$data= array(
        		'password' => md5($password),
        		'token' => $token
	        	);
	        	if($this->User_model->update_password($data,$uid,$token)){
	        		$response=array(
						'status'=>'success',
						'message' => 'Password changed successfully.'
					);
	        	}else{
	        		$response=array(
						'status'=>'failed',
						'message' => 'There is some problem in resetting your password,please try again'
					);
	        	}
        	}else{
        		$response=array(
						'status'=>'failed',
						'message' => 'There is some problem in resetting your password,please try again'
				);
        	}

        	$this->session->set_flashdata('response', $response);
        	redirect(base_url().'User_dashboard');
        }

	}

	public function send_switft_mail($subject,$mail_content,$email){

    	$mailer = new Swift_Mailer(
		  Swift_SmtpTransport::newInstance('box1085.bluehost.com',465,'ssl')
		    ->setUsername('info@completebusinessplans.com')
		    ->setPassword('Info@123')
		  );

		$message = Swift_Message::newInstance($subject)
		  ->setFrom(array('info@completebusinessplans.com' => 'CBP Online'))
		  ->setTo(array($email => 'CBP Online'))
		  ->setBody($mail_content, 'text/html');


		if(!$mailer->send($message)){
			echo "error";
		}
    }

}