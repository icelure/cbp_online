<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bankloan_setup extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct() {
        parent::__construct();
        $this->load->model('Bankloan_setup_model');
        if(!isset($this->session->userdata('user')->logged_in) || $this->session->userdata('user')->logged_in !== true) {
            redirect(base_url().'login');
   		}
        
    }

	public function index()
	
	{	
	
	$data['is_post'] = false;
	if(isset($_POST['btn'])){
			
		
			
		$data=$this->checkreq();
		$data['is_post'] = true;
			
			// $data['baseurl']= base_url();
			
			}
			$this->load->template_left_nav('Bankloan_setup',$data);
	 
       //  $this->load->template_left_nav('Bankloan_setup',$data);
			
                	//$this->load->view('Planning_setup', $data);
		//$this->load->template_left_nav('Planning_setup',$data);
		
		
	
		
	}
	public function checkreq(){
	$loan_amount=$this->input->post('amount');
	$loan_length=$this->input->post('apr');
	$annual_interest=$this->input->post('years');
	$pay_periodicity=$this->input->post('pay_periodicity');
	$pay_periodicity1=$this->input->post('pay_periodicity');
	$periods = array(
					52 => 'Weekly',
					26 => 'Bi-weekly',
					12 => 'Monthly',
					6 => 'Bi-monthly',
					4 => 'Quarterly',
					2 => 'Semi-annually',
					1 => 'Annually'
					);

	
	$pay_periods = '';
	$periodicity     = $periods[$pay_periodicity];
	
	foreach($periods as $value => $name)
	{
		$selected = ($pay_periodicity == $value) ? 'selected' : '';
	}
	
	
	$c_balance         = $loan_amount;
		$total_periods     = $loan_length * $pay_periodicity;
		$interest_percent  = $annual_interest / 100;
		$period_interest   = $interest_percent / $pay_periodicity;
	    $c_period_payment  = $loan_amount * ($period_interest / (1 - pow((1 + $period_interest), -($total_periods))));
		$total_paid        = number_format($c_period_payment * $total_periods, 2, '.', ' ');
		$total_interest    = number_format($c_period_payment * $total_periods - $loan_amount, 2, '.', ' ');
		$total_principal   = number_format($loan_amount, 2, '.', ' ');

		$loan_amount     = number_format($loan_amount, 2, '.', ' ');
		$annual_interest = number_format($annual_interest, 2, '.', ' ');
	    $period_payment  = number_format($c_period_payment, 2, '.', ' ');
	
	$amortization_table_rows = '';
		for($period = 1; $period <= $total_periods; $period++)
		{
			$c_interest  = $c_balance * $period_interest;
			$c_principal = $c_period_payment - $c_interest;
			$c_balance  -= $c_principal;
			
			$interest  = number_format($c_interest, 2, '.', ' ');
			$principal = number_format($c_principal, 2, '.', ' ');
			$balance   = number_format($c_balance, 2, '.', ' ');
			
			$evenrow_row_modifier = ($period % 2) ? '' : 'class=evenrow';

			 $amortization_table_rows .='<tr>
	<td align=center class=bordered>'.$period.'</td>
	<td align=right class=bordered>'.$interest.'</td>
	<td align=right class=bordered>$'.$principal.'</td>
	<td align=right class=bordered>$'.$balance.'</td>
</tr>';
			
		}
		
		
	$data['loan_length']=$loan_length;
	$data['loan_amount']=$loan_amount;
	$data['annual_interest']=$annual_interest;
	$data['total_paid']=$total_paid;
	$data['total_interest']=$total_interest;
	$data['total_periods']=$total_periods;
	$data['amortization_table_rows']=$amortization_table_rows;
		$data['total_interest']=$total_interest;
		$data['total_principal']=$total_principal;
		$data['pay_periodicity1']=$pay_periodicity1;
	
	
	$data1=array();
		$id=$this->session->userdata('user')->id;
	
	$data1['user_id']=$id;
	$data1['loan_amount']=$loan_amount;
	$data1['loan_length']=$loan_length;
	$data1['annual_interest']=	$annual_interest;
	$data1['total_paid']=$total_paid;
	$data1['total_interest']=$total_interest;
		$data1['total_period']=$total_periods;
	$status=$this->Bankloan_setup_model->saves_record($data1,$id);
	
	
	return $data;
	/* echo $loan_length."|";
	echo $loan_amount."|";
	echo $annual_interest."|";
	echo $total_paid."|";
	echo $total_interest."|"; 
	echo $total_periods ."|";
	echo $amortization_table_rows."|";
	echo $total_interest.'|';
	echo $total_principal.'|'; */
	
	
	
	}


	   
	   
	   
	
	
}
