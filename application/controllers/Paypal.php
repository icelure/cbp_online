<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Paypal extends CI_Controller 
{
     function  __construct(){
        parent::__construct();
        $this->load->library('paypal_lib');
        $this->load->model('Plan_model');
     }
     
     function success(){
        //get the transaction data
        /*$paypalInfo = $this->input->get();

          
        $data['item_number'] = $paypalInfo['item_number']; 
        $data['txn_id'] = $paypalInfo["tx"];
        $data['payment_amt'] = $paypalInfo["amt"];
        $data['currency_code'] = $paypalInfo["cc"];
        $data['status'] = $paypalInfo["st"];*/
        /*print_r($data);
        exit;*/

        $response=array(
            'status'=>'success',
            'message' => 'You have successfully completed payment process.'
        );
        
        $this->session->set_flashdata('response', $response);
        $this->session->userdata('user')->subscribed = "y";
        $this->session->userdata('user')->plan_end_date = date('Y-m-d H:i:s',strtotime("+1 month"));
        $this->Plan_model->update_subscription();
        redirect(base_url().'user_dashboard');
     }
     
     function cancel(){
        $this->load->view('paypal/cancel');
     }
     
     function ipn(){
        //paypal return transaction details array
        $paypalInfo    = $this->input->post();

        $data['user_id'] = $paypalInfo['custom'];
        $data['plan_id']    = (int)$paypalInfo["item_number"];
        $data['txn_id']    = $paypalInfo["txn_id"];
        $data['payment_gross'] = $paypalInfo["payment_gross"];
        $data['currency_code'] = $paypalInfo["mc_currency"];
        $data['payer_email'] = $paypalInfo["payer_email"];
        $data['payment_status']    = $paypalInfo["payment_status"];
        $data['createdDate']    = date('Y-m-d H:i:s');

        $paypalURL = $this->paypal_lib->paypal_url;        
        $result    = $this->paypal_lib->curlPost($paypalURL,$paypalInfo);
        
        //check whether the payment is verified
        if(eregi("VERIFIED",$result)){
            //insert the transaction data into the database
            $this->Plan_model->insertTransaction($data);
        }
    }
}