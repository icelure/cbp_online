<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Startup extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct() {
        parent::__construct();
        $this->load->model('Startup_model');
        if(!isset($this->session->userdata('user')->logged_in) || $this->session->userdata('user')->logged_in !== true) {
            redirect(base_url().'login');
   		}
        
    }
    
    

	public function index()
	{	
	$var = $this->session->userdata;
		
		if(isset($_POST['submtForm']) && $_POST['submtForm']=='cmpnySettings'){
			$this->setupCompany();
		}
		
		$data['page'] = $this->uri->segment(1);
		$var = $this->session->userdata;
		//echo '<pre/>';print_r($var['user']->id);exit;
		$user_id = $var['user']->id;
		$data['monthly_expense'] = $this->db->get_where('monthly_expense_budget', array('user_id' => $user_id))->result_array();
		$data['one_time_cost'] = $this->db->get_where('one_time_cost', array('user_id' => $user_id))->result_array();
		$data['director'] = $this->db->get_where('director', array('user_id' => $user_id))->result_array();
		$data['is_post'] = false;
		
if(isset($_POST['btn'])){

$data=array_merge($data,$this->checkreq());

$data['is_post'] = true;

}$user_id = $var['user']->id;
	$data['everydata']=$this->Startup_model->fetch_record($user_id); 	
		// echo '<pre/>';print_r($data);
		$this->load->template_left_nav('Startup',$data);
	}
	public function abc(){
	
		$var = $this->session->userdata;
		$user_id = $var['user']->id;
		$myarray=$this->Startup_model->fetch_record($user_id);
		
		exit;
		
		}
	public function checkreq(){
	$loan_amount=$this->input->post('amount');
	$loan_length=$this->input->post('apr');
	$annual_interest=$this->input->post('years');
	$pay_periodicity=$this->input->post('pay_periodicity');
	$pay_periodicity1=$this->input->post('pay_periodicity');
	$periods = array(
					52 => 'Weekly',
					26 => 'Bi-weekly',
					12 => 'Monthly',
					6 => 'Bi-monthly',
					4 => 'Quarterly',
					2 => 'Semi-annually',
					1 => 'Annually'
					);

	
	$pay_periods = '';
	$periodicity     = $periods[$pay_periodicity];
	
	foreach($periods as $value => $name)
	{
		$selected = ($pay_periodicity == $value) ? 'selected' : '';
	}
	
	
	$c_balance         = $loan_amount;
		$total_periods     = $loan_length * $pay_periodicity;
		$interest_percent  = $annual_interest / 100;
		$period_interest   = $interest_percent / $pay_periodicity;
	    $c_period_payment  = $loan_amount * ($period_interest / (1 - pow((1 + $period_interest), -($total_periods))));
		$total_paid        = number_format($c_period_payment * $total_periods, 2, '.', ' ');
		$total_interest    = number_format($c_period_payment * $total_periods - $loan_amount, 2, '.', ' ');
		$total_principal   = number_format($loan_amount, 2, '.', ' ');

		$loan_amount     = number_format($loan_amount, 2, '.', ' ');
		$annual_interest = number_format($annual_interest, 2, '.', ' ');
	    $period_payment  = number_format($c_period_payment, 2, '.', ' ');
	
	$amortization_table_rows = '';
		for($period = 1; $period <= $total_periods; $period++)
		{
			$c_interest  = $c_balance * $period_interest;
			$c_principal = $c_period_payment - $c_interest;
			$c_balance  -= $c_principal;
			
			$interest  = number_format($c_interest, 2, '.', ' ');
			$principal = number_format($c_principal, 2, '.', ' ');
			$balance   = number_format($c_balance, 2, '.', ' ');
			
			$evenrow_row_modifier = ($period % 2) ? '' : 'class=evenrow';

			 $amortization_table_rows .='<tr>
	<td align=center class=bordered>'.$period.'</td>
	<td align=right class=bordered>'.$interest.'</td>
	<td align=right class=bordered>$'.$principal.'</td>
	<td align=right class=bordered>$'.$balance.'</td>
</tr>';
			
		}
		
		
	$data['loan_length']=$loan_length;
	$data['loan_amount']=$loan_amount;
	$data['annual_interest']=$annual_interest;
	$data['total_paid']=$total_paid;
	$data['total_interest']=$total_interest;
	$data['total_periods']=$total_periods;
	$data['amortization_table_rows']=$amortization_table_rows;
		$data['total_interest']=$total_interest;
		$data['total_principal']=$total_principal;
		$data['pay_periodicity1']=$pay_periodicity1;
	//print_r($data);
	
	$data1=array();
		$id=$this->session->userdata('user')->id;
	
	$data1['user_id']=$id;
	$data1['loan_amount']=$loan_amount;
	$data1['loan_length']=$loan_length;
	$data1['annual_interest']=	$annual_interest;
	$data1['total_paid']=$total_paid;
	$data1['total_interest']=$total_interest;
		$data1['total_period']=$total_periods;
	$status=$this->Startup_model->saves_record($data1,$id);
	
	
	return $data;
	
	
	
	
	}

	public function ajax_summary(){ ?>
		<table id="" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th colspan='2'>Summary Working Capital</th>                    
                </tr>
            </thead>
            <tbody>
			<tr>
				<td width='80%'>Directors Capital Investment</td>
				<td><?php 
				$var = $this->session->userdata;
				$user_id = $var['user']->id;
				$this->db->from('director');
				$this->db->where('user_id',$user_id);
				$query = $this->db->get();
				$dir = $query->result();
				//echo '<pre>';print_r($dir);echo '</pre>';
				foreach($dir as $d){
					$dir_total = $dir_total + $d->amount;
					
				}
				echo number_format($dir_total);
				?></td>
			</tr>
			<tr>
				<td width='80%'>Business Loan Calculator</td>
				<td>-</td>
			</tr>
			<tr>
				<th width='80%'>Total</th>
				<th><? echo number_format($dir_total); ?></th>
			</tr>
            </tbody>
        </table>

		<table id="" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th colspan='2'>Summary Startup Costs</th>                    
                </tr>
            </thead>
            <tbody>
			<tr>
				<td width='80%'>Land & Buildings</td>
				<td><?php 
				$var = $this->session->userdata;
				$user_id = $var['user']->id;
				$this->db->from('one_time_cost');
				$this->db->where('user_id',$user_id);
				$this->db->where('one_time_cost','Land_Buildings');
				$query = $this->db->get();
				$Land = $query->result();
				//echo '<pre>';print_r($dir);echo '</pre>';
				foreach($Land as $la){
					$la_total = $la_total + $la->total_cost;
					
				}
				if($la_total){
				echo number_format($la_total);
				}
				?></td>
			</tr>
			<tr>
				<td width='80%'>Plant & Equipment</td>
				<td><?php 
				$var = $this->session->userdata;
				$user_id = $var['user']->id;
				$this->db->from('one_time_cost');
				$this->db->where('user_id',$user_id);
				$this->db->where('one_time_cost','Plant_Equipment');
				$query = $this->db->get();
				$Land = $query->result();
				//echo '<pre>';print_r($dir);echo '</pre>';
				foreach($Land as $la){
					$pe_total = $pe_total + $la->total_cost;
					
				}
				if($pe_total){
				echo number_format($pe_total);
				}
				?></td>
			</tr>
			<tr>
				<td width='80%'>Security Deposit(s)</td>
				<td><?php 
				$var = $this->session->userdata;
				$user_id = $var['user']->id;
				$this->db->from('one_time_cost');
				$this->db->where('user_id',$user_id);
				$this->db->where('one_time_cost','Security_Deposit');
				$query = $this->db->get();
				$Land = $query->result();
				//echo '<pre>';print_r($dir);echo '</pre>';
				foreach($Land as $sd){
					$sd_total = $sd_total + $sd->total_cost;
					
				}
				if($sd_total){
				echo number_format($sd_total);
				}
				?></td>
			</tr>
			<tr>
				<td width='80%'>One -Time Costs</td>
				<td><?php 
				$var = $this->session->userdata;
				$user_id = $var['user']->id;
				$this->db->from('one_time_cost');
				$this->db->where('user_id',$user_id);
				$this->db->where('one_time_cost','ONE_TIME_COSTS');
				$query = $this->db->get();
				$Land = $query->result();
				//echo '<pre>';print_r($dir);echo '</pre>';
				foreach($Land as $ot){
					$ot_total = $ot_total + $ot->total_cost;
					
				}
				if($ot_total){
				echo number_format($ot_total);
				}
				?></td>
			</tr>
			<tr>
				<th width='80%'>Total</th>
				<th><?php echo number_format($la_total + $pe_total + $sd_total + $ot_total); ?></th>
			</tr>
            </tbody>
        </table>
<table id="" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th width='80%'>Monthly Cost Budget Allowance</th>
					<th><?php 
				$var = $this->session->userdata;
				$user_id = $var['user']->id;
				$this->db->from('monthly_expense_budget');
				$this->db->where('user_id',$user_id);				
				$query = $this->db->get();
				$monthly_budget = $query->result();
				//echo '<pre>';print_r($monthly_budget);echo '</pre>';
				foreach($monthly_budget as $mb){
					$mb_total = $mb_total + $mb->total_cost;
					
				}
				if($mb_total){
				echo number_format($mb_total);
				}
				?></th>                    
                </tr>
            </thead>
            
        </table>
		<table id="" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th width='80%'>Opening Bank Balance</th>
					<th><?php echo number_format($dir_total + $mb_total + $la_total + $pe_total + $sd_total + $ot_total);?></th>                    
                </tr>
            </thead>
            
        </table>
	<?php }
}
