<?php
Class Plan_model extends CI_Model
{
	//insert transaction data
    public function insertTransaction($data = array()){
        $insert = $this->db->insert('payments',$data);
        if($insert){

            $plan_start_date = date('Y-m-d H:i:s');
            if($data['plan_id'] == 2){
                $plan_end_date = date('Y-m-d H:i:s',strtotime("+1 year"));
            }else{
                $plan_end_date = date('Y-m-d H:i:s',strtotime("+1 month"));
            }
        	
            $data2=array(
        		"subscribed" => "y",
                "plan_start_date" => $plan_start_date,
                "plan_end_date" => $plan_end_date
        	);
            
        	$this->db->where('id',$data['user_id']);
        	if($this->db->update('user',$data2)){
        		return true;
        	}
        }
    }

    public function update_subscription(){
        $data2=array(
            "subscribed" => "y"
        );
        $this->db->where('id',$this->session->userdata('user')->id);
        if($this->db->update('user',$data2)){
            return true;
        }
    }

    public function get_all_plans($type=''){
        if($type!=''){
            $this->db->where('type',$type);
        }
        $query = $this->db->get('plans');

        $plans =array();

        if($query->num_rows() >= 1){
            foreach ($query->result_array() as $plan) {
                $plans[] = $plan;
            }
        }
        
        return $plans;
    }

    public function update_plan($id,$type,$data){
        $this->db->where('id',$id);
        $this->db->where('type',$type);
        if($this->db->update('plans',$data)){
            return true;
        }else{
            return false;
        }

    }
}
?>