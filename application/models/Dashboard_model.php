<?php
Class Dashboard_model extends CI_Model
{
	public function no_of_user(){
		$this->db->select();
		$this->db->where('type','u');
		$this->db->from('user');
		return $this->db->get()->num_rows();
	}

	public function no_of_payments(){
		$this->db->select();
		$this->db->from('payments');
		return $this->db->get()->num_rows();
	}
}
?>