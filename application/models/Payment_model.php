<?php
Class Payment_model extends CI_Model
{
	public function get_all_payments(){
		$this->db->select('p.*,u.name');
		$this->db->from('payments p');
		$this->db->join('user u','p.user_id=u.id');
		$query = $this->db->get();
		$payments =array();

		if($query->num_rows() > 1){
			foreach ($query->result_array() as $pay) {
				$payments[] = $pay;
			}
		}
		return $payments;
	}
}
?>