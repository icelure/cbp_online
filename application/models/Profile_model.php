<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile_model extends CI_Model {

	private $table = 'user';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function get_detail_by_id($id){

		$this->db->from($this->table);

		$this->db->where(array('id'=>$id));

		$query = $this->db->get();

		return $query->row();

	}

	public function edit_profile($data, $id){
		
		$this->db->set($data);
		
		$this->db->where(array('id'=>$id));
		
		$this->db->update($this->table);

	}
}
?>