<div class="container">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Login
    <small>@ CBP Online</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Log in</li>
  </ol>
</section>
<style type="text/css">

#error {
    display: inline-block;
    width: 30em;
    margin-right: .5em;
    padding-top: 1px;
    color: red;
}
</style>




<!-- Main content -->
<section class="content">
  <div class="table-responsive">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
            <?php
              $flashdata= $this->session->flashdata('response');
              if(!empty($flashdata)){
                if($flashdata['status'] == 'success'){
            ?>
                  <div class="callout callout-success">
                    <?php echo $flashdata['message']; ?>
                  </div>
            <?php
                }
                if($flashdata['status'] == 'failed'){
            ?>
                <div class="callout callout-danger">
                  <?php echo $flashdata['message']; ?>
                </div>
            <?php
                }
              }
            ?>

            <p id="gStartTd">Login to your account</p>
            <p class="tdDetail">
               <span class="chkstyle" style="color:#505050;">New Customer ? <a id="Body_chkExistUser" href="<?php echo base_url().'register/';?>"> <label for="Body_chkExistUser">Sign Up and start planning today.</label></a></span>
            </p>
        </div>
        <?php echo form_open(base_url().'login', 'method="post"');?>
        <div class="box-body no-padding">
            <div class="col-md-6" id="login_cont">
              <div class="form-group">
                  <div class="input-group">
                  <div class="input-group-addon">
                        <i class="fa fa-envelope"></i>
                  </div>
                  <input type="email" name="email" class="form-control" placeholder="Email address">
                  </div>

                  <label id="error"><?php echo $this->session->flashdata('email');?></label>
              </div>
              <div class="form-group">
                  <div class="input-group">
                  <div class="input-group-addon">
                        <i class="fa fa-key"></i>
                  </div>
                  <input type="password" name="password" class="form-control" placeholder="Password">
                  </div>

                  <label id="error"><?php echo $this->session->flashdata('password');?></label>
              </div>
              <div class="form-group">
                  <input type="checkbox" id="remember_me" name="remember_me"  data-mini="true"> <label style="color: #505050; font-size: 13px; font-weight: normal;font-family: arial; padding-top: 15px">Remember Me</label>
                </div>
              <div class="form-group">
                <input type="hidden" name="loginForm" value="postForm">
                <input type="submit" name="btnSignup" value="Login >" id="btnSignup" class="btnStyle btn btn-success" style="width:200px;"/>
              </div>
              <?php echo form_close(); ?>
              <a href="javascript:void(0);" id="fp_link">I forget my password</a><br>
            </div>
            <?php echo form_open(base_url().'login', 'method="post"');?>
            <div class="col-md-6" id="fp_cont" style="display: none">
              <div class="form-group">
                  <div class="input-group">
                  <div class="input-group-addon">
                        <i class="fa fa-envelope"></i>
                  </div>
                  <input type="email" name="fp_email" id="fp_email" class="form-control" placeholder="Email address">
                  </div>
                  <label id="error"><?php echo $this->session->flashdata('fp_email');?></label>
              </div>
              <div class="form-group">
                <input type="hidden" name="fpForm" value="postfpForm">
                <button type="submit" class="btn btn-success btn-flat">Retieve Email</button>
              </div>
              <?php echo form_close(); ?>
              <a href="javascript:void(0);" id="lg_link">Login to your account?</a><br>
            </div>
            <div class="col-md-6">
              <span class="information">
                  <p class="pHeader"><img src="<?=asset_url()?>/img/key.png" alt=""> Only Pay for what you need</p>
                  <p class="pContent">Companies plan every day. From how many employees a retail store might need on a given day to how a Fortune 500 company will invest its profits, Planning is one of the fundamental functions of business. </p>

                  <p class="pHeader"><img src="<?=asset_url()?>/img/import.png" alt=""> Mobile enabled</p>
                  <p class="pContent">Companies plan every day. From how many employees a retail store might need on a given day to how a Fortune 500 company will invest its profits, Planning is one of the fundamental functions of business.</p>

                  <p class="pHeader"><img src="<?=asset_url()?>/img/area_chart.png" alt=""> Powerful and easy to use</p>
                  <p class="pContent">In this moduel,you will start by setting up your comapany details,general business,payroll,and imports assumptions,click on the 'Next' button to continue,please note that all values must be filled in for you to move to the next view</p>

                  <p class="pHeader"><img src="<?=asset_url()?>/img/approval.png" alt=""> Powerful and easy to use</p>
                  <p class="pContent">In this moduel,you will start by setting up your comapany details,general business,payroll,and imports assumptions,click on the 'Next' button to continue,please note that all values must be filled in for you to move to the next view</p>
              </span>
            </div>
        </div>
        <div class="box-footer">

        </div>
      </div>
    </div>
  </div>

</section><!-- /.content -->
</div><!-- /.container -->
<script type="text/javascript">
$(document).ready(function() {
  $("#fp_link").click(function(event) {
    $("#login_cont").hide('slow', function() {

    });
    $("#fp_cont").show('slow');
  });

  $("#lg_link").click(function(event) {
    $("#fp_cont").hide('slow', function() {

    });
     $("#login_cont").show('slow');
  });
});
</script>