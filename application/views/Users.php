<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/datatables/dataTables.bootstrap.css">
<link rel="stylesheet" href="<?php echo css_url(); ?>bootstrap-switch.min.css">
<section class="content-header">
  <h1>
    Users
    <!-- <small>advanced tables</small> -->
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Users</a></li>
    <!-- <li class="active">Data tables</li> -->
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <!-- <div class="box-header">
          <h3 class="box-title">Data Table With Full Features</h3>
        </div>/.box-header -->
        <div class="box-body">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Email</th>
                <th>Status</th>
                <th>Plan Subscribed</th>
                <th>Plan Start Date</th>
                <th>Plan End Date</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
            <?php if(!empty($users)) { 

              foreach ($users as $user) { ?>
               <tr>
                <td><?php print $user['id']; ?></td>
                <td><?php print $user['name']; ?></td>
                <td><?php print $user['email']; ?></td>
                <td><?php ($user['status'] == 'a') ? print "Active" : print "Inactive" ?></td>
                <td><?php ($user['subscribed'] == 'y') ? print "Yes" : print "No" ?></td>
                <td><?php ($user['plan_start_date'] != NULL || $user['plan_start_date']!='') ? print $user['plan_start_date'] : print "N/A" ?></td>
                <td><?php ($user['plan_end_date'] != NULL || $user['plan_end_date']!='') ? print $user['plan_end_date'] : print "N/A" ?></td>
                <td><input type="checkbox" class="make-switch" <?php ($user['isActive']=='y') ? print 'checked' : '' ?> data-uid="<?php print $user['id']; ?>"></td>
              </tr>
             <?php } 
                }
             ?>
              </tbody>
              <tfoot>
                <th>ID</th>
                <th>Name</th>
                <th>Email</th>
                <th>Status</th>
                <th>Plan Subscribed</th>
                <th>Plan Start Date</th>
                <th>Plan End Date</th>
                <th>Action</th>
              </tfoot>
            </table>
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </section><!-- /.content -->
  <script src="<?php echo  asset_url(); ?>plugins/datatables/jquery.dataTables.min.js"></script>
  <script src="<?php echo  asset_url(); ?>plugins/datatables/dataTables.bootstrap.min.js"></script>
  <script src="<?php echo  js_url(); ?>bootstrap-switch.min.js"></script>
  <script>
    $(function () {
      $('#example1').DataTable({
        "fnDrawCallback": function ( oSettings ) {
            $('.make-switch').bootstrapSwitch();

            $('.make-switch').on('switchChange.bootstrapSwitch', function (e, flag) {
              
              var status='n';
              var uid = $(this).attr("data-uid");
              if(flag){status='y';}
              $.ajax({
                url:site_url+"users/chng_status",
                type:"POST",
                data:{uid:uid,status:status}
              })

            });
          }
      });
    });



  </script>