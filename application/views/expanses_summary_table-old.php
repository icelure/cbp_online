<div class="col-md-6 col-sm-12" id="chart-container">
  <canvas id="chartContainer123"  width="300" height="200" ></canvas>
</div>
<div class="col-md-6 col-sm-12 RangeSelector">
  <div>
    <h4><span class="ServiceIncomePers" id="m_perc"><?php echo $cost_increase_percentage[0]['marketing'] ?></span>%<span style="float:right; font-size:16px;">Marketing</span></h4>
    <input id="ex8" type="range" min="0" max="100" value="<?php echo $cost_increase_percentage[0]['marketing'] ?>" class="RangeSelectorInput" style="margin-top: 0px;"/>
  </div>

  <div >
    <h4  ><span class="ServiceIncomePers" id="p_perc"><?php echo $cost_increase_percentage[0]['pr'] ?></span>%<span style="float:right; font-size:16px;">Public Relations</span></h4>
    <input id="ex9" type="range" min="0" max="100" value="<?php echo $cost_increase_percentage[0]['pr'] ?>" class="RangeSelectorInput" style="margin-top: 0px;"/>
  </div>

  <div >
    <h4 ><span class="ServiceIncomePers" id="a_perc"><?php echo $cost_increase_percentage[0]['ac'] ?></span>%<span style="float:right; font-size:16px;">Administration</span></h4>
    <input id="ex10" type="range" min="0" max="100" value="<?php echo $cost_increase_percentage[0]['ac'] ?>" class="RangeSelectorInput" style="margin-top: 0px;"/>
  </div>

  <div >
    <h4  ><span class="ServiceIncomePers" id="o_perc"><?php echo $cost_increase_percentage[0]['other'] ?></span>%<span style="float:right; font-size:16px;">Other</span></h4>
    <input id="ex11" type="range" min="0" max="100" value="<?php echo $cost_increase_percentage[0]['other'] ?>" class="RangeSelectorInput" style="margin-top: 0px; margin-bottom: 10px;"/>
  </div>

  <div class="pull-right">
    <input type="button" value="Save" class="btn btn-info" id="save"/>
  </div>
</div><!-- end RangeSelector -->
<table id="table2" class="table table-bordered table-striped">
  <thead>
    <tr>
      <th>Account</th>
      <th>Weekly</th>
      <th>Monthly</th>
      <th>Quarterly</th>
      <th>Year 1</th>
      <th>Year 2</th>
      <th>Year 3</th>

    </tr>
  </thead>
  <tbody>
  <?php
  $total_year1= 0;
  $total_year2= 0;
  $total_year3= 0;

  if(!empty($list)) {
      foreach ($expense_summary as $summary) { ?>
        <tr id="<?php echo $summary['purpose'];?>" onclick="update_graph('<?php echo $summary['purpose'];?>')">
          <td><?php echo 'Total '. $summary['purpose']; ?></td>
          <td>$<?php echo $summary['weekly_cost']; ?></td>
          <td>$<?php echo $summary['monthly_cost']; ?></td>
          <td>$<?php echo $summary['quarterly_cost']; ?></td>

          <?php
          if($summary['purpose']=='Marketing')
              $cost_increase = number_format((float) $cost_increase_percentage[0]['marketing']/100, 2, '.', '');
          elseif($summary['purpose']=='Public-Relations')
              $cost_increase =number_format((float) $cost_increase_percentage[0]['pr']/100, 2, '.', '');
          elseif($summary['purpose']=='Other')
                $cost_increase = number_format((float) $cost_increase_percentage[0]['other']/100, 2, '.', '');
          elseif($summary['purpose']=='Administration')
            $cost_increase = number_format((float) $cost_increase_percentage[0]['ac']/100, 2, '.', '');
          else
            $cost_increase = 1;
            $year1= $summary['yearly_cost'];
            $year2 = intval($year1)*$cost_increase + intval($year1);
            $year3 = intval($year2)*$cost_increase + intval($year2);

            $total_year1 = $total_year1 + $year1;
            $total_year2 = $total_year2 + $year2;
            $total_year3 = $total_year3 + $year3;

          ?>
          <td id="<?php echo $summary['purpose'];?>_year1" data-val="<?php echo $year1; ?>">$<?php echo $year1 ?></td>
          <td id="<?php echo $summary['purpose'];?>_year2" data-val="<?php echo $year2; ?>">$<?php echo $year2 ?></td>
          <td id="<?php echo $summary['purpose'];?>_year3" data-val="<?php echo $year3; ?>">$<?php echo $year3 ?></td>
        </tr>
<?php }
  }?>

    <tr>
      <td></td>
      <td></td>
      <td></td>
      <td><b>Total:</b></td>
      <td id="total_y1" data-val="<?php echo $total_year1; ?>"><b>$<?php echo $total_year1; ?></b></td>
      <td id="total_y2" data-val="<?php echo $total_year2; ?>"><b>$<?php echo $total_year2; ?></b></td>
      <td id="total_y3" data-val="<?php echo $total_year3; ?>"><b>$<?php echo $total_year3; ?></b></td>
    </tr>
  </tbody>
</table>
<script type="text/javascript">
window.onload = function(){



  update_graph("loadchart");

}

var myBarChart = null;

function update_graph(account){

  if(myBarChart!=null){
    $("#chart-container").empty();
    $("#chart-container").append('<canvas id="chartContainer123"  width="300" height="200" ></canvas>');
  }

  var ctx = $("#chartContainer123");

  if(account=="loadchart"){
    var year1 = $("#total_y1").attr('data-val');
    var year2 = $("#total_y2").attr('data-val');
    var year3 = $("#total_y3").attr('data-val');
  }
  else{
    var year1 = $("#"+account+"_year1").attr('data-val');
    var year2 = $("#"+account+"_year2").attr('data-val');
    var year3 = $("#"+account+"_year3").attr('data-val');
  }

  var data = {
        labels: ["Year One","Year Two","Year Three"],
        datasets: [{
          label: "Expense Summary",
          backgroundColor: [
              'rgba(255, 99, 132, 0.2)',
              'rgba(54, 162, 235, 0.2)',
              'rgba(255, 206, 86, 0.2)'
          ],
          borderColor: [
              'rgba(255,99,132,1)',
              'rgba(54, 162, 235, 1)',
              'rgba(255, 206, 86, 1)'
          ],
          borderWidth: 1,
          data: [
            year1,
            year2,
            year3
          ],
        }]
  };

  myBarChart = new Chart(ctx, {
    type: 'bar',
    data: data,
    options: {
      scales: {
        xAxes: [{
          stacked: true
        }],
        yAxes: [{
          stacked: true
        }]
      }
    }
  });
}

</script>