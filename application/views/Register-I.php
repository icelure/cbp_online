
<link rel="stylesheet" href="<?php echo asset_url(); ?>css/model.css">
<script src="<?php echo asset_url(); ?>js/model.js"></script>

<div class="container">
  <!-- Content Header (Page header) -->
  <section class="content-header">
  <h1>
      Sign UP
      <small>@ CBPOnline</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Sign UP</li>
  </ol>
  </section>
<style type="text/css">

#error {
    display: inline-block;
    width: 30em;
    margin-right: .5em;
    padding-top: 1px;
    color: red;
}
</style>

  <!-- Modal

  <div class="modal fade in" id="TermModal" tabindex="-1" role="dialog" aria-labelledby="TermModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button><h4 class="modal-title">Company Setup</h4></div>
        <div class="modal-body">

            <iframe src="http://completebusinessplans.com/cbp_online/assets/HTML/ExpensesHelp.html"></iframe>

        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div> -->

  <!-- Main content -->
  <section class="content">
    <div class="table-responsive">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
              <?php
                $flashdata= $this->session->flashdata('response');
                if(!empty($flashdata)){
                  if($flashdata['status'] == 'success'){
              ?>
                    <div class="callout callout-success">
                      <?php echo $flashdata['message']; ?>
                    </div>
              <?php
                  }
                  if($flashdata['status'] == 'failed'){
              ?>
                  <div class="callout callout-danger">
                    <?php echo $flashdata['message']; ?>
                  </div>
              <?php
                  }
                }
              ?>

              <p id="gStartTd">Let's 'get started by regestering for an account</p>
              <p class="tdDetail">
                 <span class="chkstyle" style="color:#505050;"><a id="Body_chkExistUser" href="<?php echo base_url().'login/';?>"> <label for="Body_chkExistUser">I already have an account</label></a></span>
              </p>
          </div>
          <?php echo form_open(base_url().'register', 'method="post"');?>
          <div class="box-body no-padding">
              <div class="col-md-6">
                <div class="form-group">
                <label for="detail">User Details </label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-user"></i>
                      </div>
                    <input type="text" name="f_name" class="form-control" placeholder="First Name" value="<?php echo (isset($_POST['f_name']))? $_POST['f_name']:''?>">
                    </div>
                    <label id="error"><?php echo $this->session->flashdata('f_name');?></label>
                    <br/>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-user"></i>
                      </div>
                    <input type="text" name="l_name" class="form-control" placeholder="Last Name" value="<?php echo (isset($_POST['l_name']))? $_POST['l_name']:''?>">
                    </div>
                    <label id="error"><?php echo $this->session->flashdata('l_name');?></label>
                </div>
                <div class="form-group">
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-envelope"></i>
                      </div>

                    <input type="email" name="email" class="form-control" placeholder="Email Address" value="<?php echo (isset($_POST['email']))? $_POST['email']:''?>">
                    </div>
                    <label id="error"><?php echo $this->session->flashdata('email');?></label>
                </div>
                <div class="form-group">
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-phone"></i>
                      </div>
                      <input name="phone" class="form-control" placeholder="Business Phone" value="<?php echo (isset($_POST['phone']))? $_POST['phone']:''?>">
                    </div>
                    <label id="error"><?php echo $this->session->flashdata('phone');?></label>
                </div>
                <div class="form-group has-feedback">
                      <div class="input-group">
                        <div class="input-group-addon">
                          <i class="fa fa-flag"></i>
                        </div>
                      <select name="country" class="form-control" id="country">
                        <option value="<?php echo (isset($_POST['country']))? $_POST['country']:''?>" selected><?php echo (isset($_POST['country']))? $_POST['country']:''?></option>
                      </select>
                      </div>
                      <label id="error"><?php echo $this->session->flashdata('country');?></label>
                </div>
                <div class="form-group">
                    <label for="username">Username</label>
                    <input name="username" class="form-control" placeholder="Username" value="<?php echo (isset($_POST['username']))? $_POST['username']:''?>">
                    <label id="error"><?php echo $this->session->flashdata('username');?></label>
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" name="password" class="form-control" placeholder="Password">
                    <label id="error"><?php echo $this->session->flashdata('password');?></label>
                    <br/>
                    <input type="password" name="confirm_password" class="form-control" placeholder="Confirm Password">
                    <label id="error"><?php echo $this->session->flashdata('confirm_password');?></label>
                </div>
                <div class="form-group" id="terms">
                  <input type="checkbox" id="agree" name="agree"  data-mini="true"> <label style="color: #505050; font-size: 13px; font-weight: normal;font-family: arial; padding-top: 15px">
                  I have read and agree to the <a href="<?php echo asset_url(); ?>HTML/ExpensesHelp.html" id="Body_hlTerm"  data-toggle="modal" data-target="#TermModal" class="view-pdf" style="color:#1395d1; text-decoration: none">terms of use</a>.</label>
                </div>
                <div class="form-group">
                  <input type="hidden" name="registerForm" value="postForm">
                  <input type="submit" name="btnSignup" value="Start >" id="btnSignup" class="btnStyle btn btn-success" style="width:200px;" disabled="true"/>
                </div>
                <?php echo form_close(); ?>
              </div>
              <div class="col-md-6">
                <span class="information">
                    <p class="pHeader"><img src="<?=asset_url()?>/img/key.png" alt=""> Planning is the key to you success !</p>
                    <p class="pContent">Companies plan every day. From how many employees a retail store might need on a given day to how a Fortune 500 company will invest its profits, Planning is one of the fundamental functions of business. </p>

                    <p class="pHeader"><img src="<?=asset_url()?>/img/import.png" alt=""> Responsive desigen for any device</p>
                    <p class="pContent">Companies plan every day. From how many employees a retail store might need on a given day to how a Fortune 500 company will invest its profits, Planning is one of the fundamental functions of business.</p>

                    <p class="pHeader"><img src="<?=asset_url()?>/img/area_chart.png" alt=""> Powerful and easy to use</p>
                    <p class="pContent">In this moduel,you will start by setting up your comapany details,general business,payroll,and imports assumptions,click on the 'Next' button to continue,please note that all values must be filled in for you to move to the next view</p>

                    <p class="pHeader"><img src="<?=asset_url()?>/img/approval.png" alt=""> Printing and email ready</p>
                    <p class="pContent">In this moduel,you will start by setting up your comapany details,general business,payroll,and imports assumptions,click on the 'Next' button to continue,please note that all values must be filled in for you to move to the next view</p>
                </span>
              </div>
          </div>
          <div class="box-footer">

          </div>
        </div>
      </div>
    </div>
  </section><!-- /.content -->
</div><!-- /.container -->

<script>
$(document).ready(function () {

     $('#agree').on('ifChecked', function () {
          $('#btnSignup').removeAttr('disabled');
     });

     $('#agree').on('ifUnchecked', function () {
        $('#btnSignup').attr('disabled', 'disabled');
      });

    populateCountries("country");


})
</script>