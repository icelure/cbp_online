<div class="well well-sm col-sm-12 col-md-12 col-lg-12">
    <strong>Display</strong>
    <div class="btn-group">
        <a href="#" id="list" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th-list"></span>List</a> 
        <a href="#" id="grid" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th"></span>Grid</a>
    </div>
    <button style="float: right;"  class="btn btn-success" onclick="add_importedproduct()"><i class="glyphicon glyphicon-plus"></i>Product</button>
</div>   
<div id="importedproducts" class="row-height list-group col-sm-12 col-md-12 col-lg-12"></div>

<script src="<?php echo base_url('assets/jquery/jquery-2.1.4.min.js') ?>"></script>
<script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo js_url(); ?>jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo js_url(); ?>additional-methods.min.js"></script>
<script src="<?php echo base_url('assets/plugins/easypaginate/easyPaginate.js') ?>"></script>
<!-- style for pagination -->
<style> 
    /*pagination style*/
    .easyPaginateNav a {    font-size: 18px;
                            padding: 5px 10px;
                            font-weight: bold;
                            color: white;
                            margin-left: 5px;
                            background-color: #3c8dbc;}
    .easyPaginateNav{
        width: auto!important;
        text-align: left;
        clear: both;
    }
    .easyPaginateNav a.current {
        font-weight:bold;
        text-decoration:underline;}
    /*item design*/
/*    .item{
        background-color: white;
        padding: 0px;
    }*/
    .itemThumbnail{
        height:165px;
        margin-bottom:0px;
    }
    .itemFooter{
        padding: 5px;
        border: solid 1px #e6e6e6;
        border-top: none;
        height: 40px;
    }
    .itemInfo{
        border: solid 1px #e6e6e6;
        border-top: none;
        padding-left: 10%;
        height: 278px;
    }

    /*Style for collection view*/
    /*.glyphicon { margin-right:5px; }*/
    .thumbnail
    {
        /*margin-bottom: 20px;*/
        padding: 0px;
        -webkit-border-radius: 0px;
        -moz-border-radius: 0px;
        border-radius: 0px;
    }
    .item.list-group-item
    {
        float: none;
        width: 100%;
        background-color: #fff;
        margin-bottom: 10px;
    }
    .item.list-group-item:nth-of-type(odd):hover,.item.list-group-item:hover
    {
        background: #428bca;
    }
    .item.list-group-item .list-group-image
    {
        
    }
    .item.list-group-item .thumbnail
    {
        margin-bottom: 0px;
    }
    .item.list-group-item .caption
    {
        padding: 9px 9px 0px 9px;
    }
    .item.list-group-item:nth-of-type(odd)
    {
        background: #eeeeee;
    }

    .item.list-group-item:before, .item.list-group-item:after
    {
        display: table;
        content: " ";
    }

    .item.list-group-item img
    {
        float: left;
    }
    .item.list-group-item:after
    {
        clear: both;
    }
    .list-group-item-text
    {
        margin: 0 0 11px;
    }
/*    @media only screen 
    and (min-device-width : 768px) 
    and (max-device-width : 1024px) 
    and (orientation : landscape)
    and (-webkit-min-device-pixel-ratio: 1){
         div.well.well-sm.col-sm-11{
            width:86.666667%
        }*/
/*        item.list-group-item {
            width: 93%;
        }*/
    /*}*/
/*    @media only screen and (min-width:768px) and (max-width:1024px) and (orientation:lanscape)
    {*/
/*        .well-sm{
            width:83%;
        }*/
/*        div.well.well-sm.col-sm-11{
            width:86.666667%
        }*/
    /*}*/
/*    @media only screen and (min-width:768px) and (max-width:1024px){
        .tab-content {
            width: 88%;
            margin: 0px 10% 0 5%;
        }
        .box-body{
            width: 104%;
            background: white;
        }
    }*/

    .easyPaginateNav {
        max-width:100%;
        text-align:left !important;
    }

</style>

<script type="text/javascript">

            // Validations
            $(function () {
                $("#importform").validate({
                    rules: {
                        importedproduct_description: {
                            required: true
                        },
                        imported_unit_cost: {
                            required: true,
                            number: true
                        },
                        imported_markup_on_cost: {
                            required: true,
                            number: true
                        },
                        ImportQuantity: {
                            required: true,
                            number: true
                        },
                        importedexchange_range: {
                            required: true,
                            number: true
                        }
                    }
                });
            });

            var save_method; //for save method string
            var table;

            $(document).ready(function () {
                drawCollectionView();

                $('#list').click(function (event) {
                    event.preventDefault();
                    $('#importedproducts .item').removeClass('grid-group-item');
                    $('#importedproducts .item').addClass('list-group-item');
                });
                $('#grid').click(function (event) {
                    event.preventDefault();
                    $('#importedproducts .item').removeClass('list-group-item');
                    $('#importedproducts .item').addClass('grid-group-item');
                });
                //datatables

                $("#btnSaveImport").click(function (e) {
                    e.preventDefault();
                    if ($("#importform").valid()) {
                        saveimported();
                    }
                });

            });

            function drawCollectionView()
            {
                $.ajax({
                    url: "<?php echo site_url('ImportedProducts/ajax_list') ?>/",
                    type: "GET",
                    dataType: "JSON",
                    success: function (data)
                    {
                        loadcollectionview(data);
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert(errorThrown);
                    }
                });
            }

            function loadcollectionview(importedproducts) {

                var html = "";
                $(importedproducts.data).each(function (key, val) {
                    html += '<div class="item  col-sm-12 col-md-6 col-lg-4">'
                            + '<div class="thumbnail itemThumbnail">'
                            + '<img style="height:95%" class="group list-group-image img-responsive" src="' + this["importedproductthumbnail"] + '" alt="" />'
                            + '</div>'
                            + '<div class="itemInfo caption">'
                            + '<div class="row">'
                            + '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 txt-bold">'
                            + '<label style="font-weight:600;">UnitCost:</label>'
                            + '</div>'
                            + '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="font-weight: normal;">' + this["unitcost"] + '</div>'
                            + '</div>'
                            + '<div class="row">'
                            + '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">'
                            + '<label style="font-weight:600;">Qty:</label>'
                            + '</div>'
                            + '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="font-weight: normal;">' + this["qty"] + '</div>'
                            + '</div>'
                            + '<div class="row">'
                            + '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">'
                            + '<label style="font-weight:600;">TotalCost:</label>'
                            + '</div>'
                            + '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="font-weight: normal;">' + this["totalcost"] + '</div>'
                            + '</div>'
                            + '<div class="row">'
                            + '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">'
                            + '<label style="font-weight:600;">Exchange Rate:</label>'
                            + '</div>'
                            + '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="font-weight: normal;">' + this["exchangerate"] + '</div>'
                            + '</div>'
                            + '<div class="row">'
                            + '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">'
                            + '<label style="font-weight:600;">Total Landed Cost:</label>'
                            + '</div>'
                            + '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="font-weight: normal;">' + this["totallandedcost"] + '</div>'
                            + '</div>'
                            + '<div>'
                            + '<h4 class="group inner list-group-item-heading"><strong>Description:-</strong></h4>'
                            + '<p style="color: #333" class="group inner list-group-item-text item-desc">' + this["importedproductdescription"] + '</p>'
                            + '</div>'
                            + '</div>'
                            + '<div class="itemFooter" style="margin-bottom:10px;" >'
                            + '<a  style="margin-left: 1%;" class="btn btn-sm btn-primary pull-right" href="javascript:void(0)" title="Edit" onclick="edit_importedproduct(' + this["importedproductid"] + ')">'
                            + '<i class="glyphicon glyphicon-pencil"></i>'
                            + '</a>'
                            + '<a  class="btn btn-sm btn-danger pull-right" href="javascript:void(0)" title="Delete" onclick="delete_importedproduct(' + this["importedproductid"] + ')">'
                            + '<i class="glyphicon glyphicon-trash"></i>'
                            + '</a>'
                            + '<a  style="margin-right: 1%;" class="btn btn-sm btn-primary pull-right" href="javascript:void(0)" title="Information" onclick="loaddetail(' + this["importedproductid"] + ')">'
                            + '<i class="glyphicon glyphicon-exclamation-sign"></i>'
                            + '</a>'
                            + '</div>'
                            + '</div>';
                });
                $('#importedproducts').html(html);
                // code for pagination
                $('#importedproducts').easyPaginate({
                    paginateElement: '.item',
                    elementsPerPage: 9,
                    firstButton: true,
                    lastButton: true
                            // effect: 'climb'
                });
                $('.item-desc').each(function () {
                    if ($(this).text().length > 130) {
                        $(this).text($(this).text().substring(0, 130) + "...");
                    }
                });
            }



            function add_importedproduct()
            {
                $("label.error").remove();
                save_method = 'add';
                $('#importform')[0].reset(); // reset form on modals
                $('.form-group').removeClass('has-error'); // clear error class
                $('.help-block').empty(); // clear error string
                $('#impmodal_form').modal('show'); // show bootstrap modal
                $('.modal-title').text('Add Imported Product'); // Set Title to Bootstrap modal title
            }

            function edit_importedproduct(id)
            {
                $("label.error").remove();
                save_method = 'update';
                $('#importform')[0].reset(); // reset form on modals
                $('.form-group').removeClass('has-error'); // clear error class
                $('.help-block').empty(); // clear error string

                //Ajax Load data from ajax
                $.ajax({
                    url: "<?php echo site_url('ImportedProducts/ajax_editimported') ?>/" + id,
                    type: "GET",
                    dataType: "JSON",
                    success: function (data)
                    {
                        //  alert(data.Description);

                        $('[name="importedid"]').val(data.id);
                        $('[name="importedproduct_description"]').val(data.Description);
                        $('[name="ImportQuantity"]').val(data.Qty);
                        $('[name="imported_unit_cost"]').val(data.UnitCost);
                        $('[name="importedexchange_range"]').val(data.ExchangeRate);

                        $('[name="imported_markup_on_cost"]').val(data.MarkUpOnCost);

                        $('#impmodal_form').modal('show'); // show bootstrap modal when complete loaded

                        $('.modal-title').text('Edit Imported Product'); // Set title to Bootstrap modal title

                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert(errorThrown);
                    }
                });
            }

            function saveimported()
            {
                $('#btnSave').text('saving...'); //change button text
                $('#btnSave').attr('disabled', true); //set button disable 
                var url;

                if (save_method == 'add') {
                    url = "<?php echo site_url('ImportedProducts/ajax_addimported') ?>";
                } else {
                    url = "<?php echo site_url('ImportedProducts/ajax_updateimported') ?>";
                }

                // ajax adding data to database
                $.ajax({
                    url: url,
                    type: "POST",
                    data: new FormData($('#importform')[0]),
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (data)
                    {
                        var obj = jQuery.parseJSON(data);

                        if (obj['status']) //if success close modal and reload ajax table
                        {
                            $('#impmodal_form').modal('hide');
                            drawCollectionView()
                        }

                        $('#btnSave').text('save'); //change button text
                        $('#btnSave').attr('disabled', false); //set button enable 
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('Error adding / update data');
                        $('#btnSave').text('save'); //change button text
                        $('#btnSave').attr('disabled', false); //set button enable 

                    }
                });
            }

            function loaddetail(id)
            {
                //Ajax Load data from ajax
                $.ajax({
                    url: "<?php echo site_url('ImportedProducts/ajax_get_imported_product') ?>/" + id,
                    type: "GET",
                    dataType: "JSON",
                    success: function (data)
                    {

                        var Total = data.Qty * data.UnitCost;
                        var TotalLandedCost = Total * data.ExchangeRate;
                        //alert(data.Qty);
                        $("#imgthumbnail").attr("src", data.ThumbNail);
                        $("#impdesc").html(data.Description);
                        $("#impSolQnt").html(data.Qty);
                        $("#impUC").html(data.UnitCost);
                        //$("#imptotal").html(data.Qty * data.UnitCost);
                        $("#imptotal").html(Total.toFixed(2));
                        $("#impExchangeRange").html(data.ExchangeRate);
                        $TotalLandedCost = data.Qty * data.UnitCost * data.ExchangeRate
                        $("#impTotalLandedCost").html(TotalLandedCost.toFixed(2));
                        //var ImportDuty=(TotalLandedCost*1)/100;
                        // ImportDuty=round(ImportDuty,0);
                        $("#impID").html((data.import_duty * TotalLandedCost).toFixed(2));
                        //var DeliveryOrder=(TotalLandedCost*1)/100;
                        $("#impDo").html((data.delivery_order * $TotalLandedCost).toFixed(2));
                        //var impCmF=(TotalLandedCost*1)/100;
                        $("#impCmF").html((data.cmr_comp_fee * $TotalLandedCost).toFixed(2));
                        //var impLTF=(TotalLandedCost*1)/100;
                        $("#impLTF").html((data.lcl_transport_fee * $TotalLandedCost).toFixed(2));
                        //var impca=(TotalLandedCost*1)/100;
                        $("#impca").html((data.cargo_auto_fee * $TotalLandedCost).toFixed(2));
                        //var impps=(TotalLandedCost*1)/100;
                        $("#impps").html((data.port_service_fee * $TotalLandedCost).toFixed(2));
                        //var impcc=(TotalLandedCost*1)/100;
                        $("#impcc").html((data.custom_clearance_fee * $TotalLandedCost).toFixed(2));
                        //var imptp=(TotalLandedCost*1)/100;
                        $("#imptp").html((data.transport_fues_fee * $TotalLandedCost).toFixed(2));
                        //var impAFt=(TotalLandedCost*1)/100;
                        $("#impAFt").html((data.aqis_fee * $TotalLandedCost).toFixed(2));
                        //var impIf=(TotalLandedCost*1)/100;
                        $("#impIf").html((data.insurance_fee * $TotalLandedCost).toFixed(2));
                        //var impDp=(TotalLandedCost*1)/100;            
                        $("#impDp").html((data.dec_processing_fee * $TotalLandedCost).toFixed(2));
                        //var impMf=(TotalLandedCost*1)/100;
                        $("#impMf").html((data.misc_fee * $TotalLandedCost).toFixed(2));
                        //var impo1=(TotalLandedCost*1)/100;
                        $("#impo1").html((data.other_fee_1 * $TotalLandedCost).toFixed(2));
                        //var impo2=(TotalLandedCost*1)/100;
                        $("#impo2").html((data.other_fee_2 * $TotalLandedCost).toFixed(2));
                        //var impo3=(TotalLandedCost*1)/100;
                        $("#impo3").html((data.other_fee_3 * $TotalLandedCost).toFixed(2));
//            var impTPc=parseFloat(TotalLandedCost)+parseFloat(ImportDuty)+parseFloat(DeliveryOrder)+parseFloat(impCmF)+parseFloat(impLTF)+parseFloat(impDp)+parseFloat(impMf)+parseFloat(impo1)+parseFloat(impo2)+parseFloat(impo3)+parseFloat(impca)+parseFloat(impps)+parseFloat(imptp)+parseFloat(impIf)+parseFloat(impAFt)+parseFloat(impcc);
//           impTPc=Math.round(impTPc * 100) / 100;
                        $ToalProductCost = ($TotalLandedCost) + (data.import_duty * $TotalLandedCost) + (data.delivery_order * $TotalLandedCost) +
                                (data.cmr_comp_fee * $TotalLandedCost) + (data.lcl_transport_fee * $TotalLandedCost) + (data.cargo_auto_fee * $TotalLandedCost)
                                + (data.port_service_fee * $TotalLandedCost) + (data.custom_clearance_fee * $TotalLandedCost) + (data.transport_fues_fee * $TotalLandedCost)
                                + (data.aqis_fee * $TotalLandedCost) + (data.insurance_fee * $TotalLandedCost) + (data.dec_processing_fee * $TotalLandedCost)
                                + (data.misc_fee * $TotalLandedCost) + (data.other_fee_1 * $TotalLandedCost) + (data.other_fee_2 * $TotalLandedCost) + (data.other_fee_3 * $TotalLandedCost)
                        //$("#impTPc").html(data.TotalProductCost);
                        $("#impTPc").html($ToalProductCost.toFixed(2));


//            var impunitcost=parseFloat(impTPc)/parseFloat(data.Qty);            
//              impunitcost=Math.round(impunitcost * 100) / 100;
                        var UnitCost = $ToalProductCost / data.Qty;
                        $("#impunitcost").html(UnitCost.toFixed(2));

//            var impMarkup=data.MarkUpOnCost;
//            impMarkup=Math.round(impMarkup * 100) / 100;
                        $("#impMarkup").html((data.MarkUpOnCost * UnitCost).toFixed(2));

                        var impWp = (data.MarkUpOnCost * UnitCost) + UnitCost;
//            impWp=impWp+impunitcost;
//             impWp=Math.round(impWp * 100) / 100;
                        $("#impWp").html(impWp.toFixed(2));
                        var impGP = ((data.MarkUpOnCost * ($ToalProductCost / data.Qty)) + ($ToalProductCost / data.Qty)) - ($ToalProductCost / data.Qty);
//            impGP=Math.round(impGP * 100) / 100;
                        $("#impGP").html(impGP.toFixed(2));

                        var impTotalRevenue = ((data.MarkUpOnCost * UnitCost) + UnitCost) * data.Qty;
//            impTotalRevenue=Math.round(impTotalRevenue * 100) / 100;
                        $("#impTotalRevenue").html(impTotalRevenue.toFixed(2));

//            var impTC=impunitcost*data.Qty;
//            impTC=Math.round(impTC * 100) / 100;
                        $("#impTC").html(($ToalProductCost * data.Qty).toFixed(2));

//            var impTPc=impTR-impTC;
//            impTPc=Math.round(impTPc * 100) / 100;
                        var GrossTotal = (((data.MarkUpOnCost * ($ToalProductCost / data.Qty)) + ($ToalProductCost / data.Qty)) * data.Qty) - ($ToalProductCost * data.Qty);
                        $("#impgross").html(GrossTotal.toFixed(2));
                        //  alert(data.Description);

                        $('#impmodal_form_Detail').modal('show'); // show bootstrap modal when complete loaded

                        $('.modal-title').text('Imported Product Detail'); // Set title to Bootstrap modal title

                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert(errorThrown);
                    }
                });
                // alert(id);
            }

            function delete_importedproduct(id)
            {
                if (confirm('Are you sure delete this data?'))
                {

                    // ajax delete data to database
                    $.ajax({
                        url: "<?php echo site_url('ImportedProducts/ajax_deleteimported') ?>/" + id,
                        type: "POST",
                        dataType: "JSON",
                        success: function (data)
                        {
                            //if success reload ajax table
                            $('#impmodal_form').modal('hide');
                            drawCollectionView()
                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                            alert(errorThrown);
                        }
                    });
                }
            }

</script>
<div class="modal fade" id="impmodal_form_Detail" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="padding: 15px 15px 0 15px;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="panel-white">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-4 thumbnail itemThumbnail">
                                    <img style="height:95%" class="group list-group-image img-responsive" id="imgthumbnail" src="" alt="">
                                </div>
                                <!--<label class="control-label col-md-4">Description</label>-->
                                <div class="control-label col-md-8" id="impdesc"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <ul class="nav nav-tabs nav-pills" role="tablist">
                    <li role="presentation" class="active"><a href="#tab1" data-toggle="tab"><i class="fa fa-user m-r-xs"></i>Basic Information</a></li>
                    <li role="presentation"><a href="#tab2" data-toggle="tab"><i class="fa fa-user m-r-xs"></i>Import Expenses</a></li>
                    <li role="presentation"><a href="#tab3" data-toggle="tab"><i class="fa fa-user m-r-xs"></i>Others</a></li>
                </ul>
            </div>

            <div class="modal-body form">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-white">
                            <div class="panel-body">
                                <div id="rootwizard">
                                    <form id="wizardForm" novalidate="novalidate">
                                        <div class="tab-content" style="margin:0">
                                            <div class="tab-pane active fade in" id="tab1">
                                                <div class="row m-b-lg">
                                                    <div class="col-xs-6">
                                                        <label class="control-label col-xs-8">Unit Cost</label>
                                                        <div class="control-label col-xs-4" id="impUC"></div>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <label class="control-label col-xs-8">Weekly Sold Qty</label>
                                                        <div class="control-label col-xs-4" id="impSolQnt"></div>
                                                    </div>
                                                </div>
                                                <div class="row m-b-lg">
                                                    <div class="col-xs-6">
                                                        <label class="control-label col-xs-8">Total</label>
                                                        <div class="control-label col-xs-4" id="imptotal"></div>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <label class="control-label col-xs-8">Exchange Rate</label>
                                                        <div class="control-label col-xs-4" id="impExchangeRange"></div>
                                                    </div>
                                                </div>
                                                <div class="row m-b-lg">
                                                    <div class="col-xs-6">
                                                        <label class="control-label col-xs-8">Total Landed Cost</label>
                                                        <div class="control-label col-xs-4" id="impTotalLandedCost"></div>
                                                    </div>
                                                     <div class="col-xs-6">
                                                        <label class="control-label col-xs-8">Total Product Cost</label>
                                                        <div class="control-label col-xs-4" id="impTPc"></div>
                                                    </div>
                                                </div>
                                                <div class="row m-b-lg">
                                                    <div class="col-xs-6">
                                                        <label class="control-label col-xs-8">Avg. Unit Cost</label>
                                                        <div class="control-label col-xs-4" id="impunitcost"></div>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <label class="control-label col-xs-8">Mark Up On Cost</label>
                                                        <div class="control-label col-xs-4" id="impMarkup"></div>
                                                    </div>
                                                </div>
                                                <div class="row m-b-lg">
                                                    <div class="col-xs-6">
                                                        <label class="control-label col-xs-8">WholeSale Price</label>
                                                        <div class="control-label col-xs-4" id="impWp"></div>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <label class="control-label col-xs-8">Gross Profit</label>
                                                        <div class="control-label col-xs-4" id="impGP"></div>
                                                    </div>
                                                </div>
                                                <div class="row m-b-lg">
                                                    <div class="col-xs-6">
                                                        <label class="control-label col-xs-8">Total Revenue</label>
                                                        <div class="control-label col-xs-4" id="impTotalRevenue"></div>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <label class="control-label col-xs-8">Total Cost</label>
                                                        <div class="control-label col-xs-4" id="impTC"></div>
                                                    </div>
                                                </div>
                                                <div class="row m-b-lg">
                                                    <div class="col-xs-6">
                                                        <label class="control-label col-xs-8">Gross Total</label>
                                                        <div class="control-label col-xs-4" id="impgross"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="tab2">
                                                <div class="row m-b-lg">
                                                    <div class="col-xs-6">
                                                        <label class="control-label col-xs-8">Import Duty</label>
                                                        <div class="control-label col-xs-4" id="impID"></div>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <label class="control-label col-xs-8">CMR Comp Fee</label>
                                                        <div class="control-label col-xs-4" id="impCmF"></div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <label class="control-label col-xs-8">LCL Transport Fee</label>
                                                        <div class="control-label col-xs-4" id="impLTF"></div>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <label class="control-label col-xs-8">Cargo Auto Fee</label>
                                                        <div class="control-label col-xs-4" id="impca"></div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <label class="control-label col-xs-8">Port Service Fee</label>
                                                        <div class="control-label col-xs-4" id="impps"></div>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <label class="control-label col-xs-8">Customs Clearance</label>
                                                        <div class="control-label col-xs-4" id="impcc"></div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <label class="control-label col-xs-8">Transport Fee</label>
                                                        <div class="control-label col-xs-4" id="imptp"></div>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <label class="control-label col-xs-8">AQIS Fee</label>
                                                        <div class="control-label col-xs-4" id="impAFt"></div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <label class="control-label col-xs-8">Insurance Fee</label>
                                                        <div class="control-label col-xs-4" id="impIf"></div>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <label class="control-label col-xs-8">Dec Processing Fee</label>
                                                        <div class="control-label col-xs-4" id="impDp"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="tab3">
                                                <div class="row m-b-lg">
                                                    <div class="col-xs-6">
                                                        <label class="control-label col-xs-8">Delivery Order</label>
                                                        <div class="control-label col-xs-4" id="impDo"></div>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <label class="control-label col-xs-8">Misc. Fee</label>
                                                        <div class="control-label col-xs-4" id="impMf"></div>
                                                    </div>
                                                </div>
                                                <div class="row m-b-lg">
                                                    <div class="col-xs-6">
                                                        <label class="control-label col-xs-8">Other 1</label>
                                                        <div class="control-label col-xs-4" id="impo1"></div>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <label class="control-label col-xs-8">Other 2</label>
                                                        <div class="control-label col-xs-4" id="impo2"></div>
                                                    </div>
                                                </div>
                                                <div class="row m-b-lg">
                                                    <div class="col-xs-6">
                                                        <label class="control-label col-xs-8">Other 3</label>
                                                        <div class="control-label col-xs-4" id="impo3"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--                <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h3 class="modal-title">Imported Product Detail</h3>
                            </div>
                            <div class="modal-body form">
                                <div class="container">
                                    <div class="row">
                                        <label class="control-label col-md-1" >Description</label>
            
                                        <div class="col-md-6" style="width: 33em; word-wrap: break-word"><div id="impdesc"></div></div>
                                    </div>
                                    <div class="row">
                                        <label class="control-label col-md-2">Weekly Sold Qty</label>
                                        <div class="col-md-1"><div id="impSolQnt"></div></div>
                                        <label class="control-label col-md-2">Unit Cost</label>
                                        <div class="col-md-1"><div id="impUC"></div></div>
                                    </div>
                                    <div class="row">
            
                                        <label class="control-label col-md-2">Total</label>
                                        <div class="col-md-1"><div id="imptotal"></div></div>
                                        <label class="control-label col-md-2">Exchange Rate</label>
                                        <div class="col-md-1"><div id="impExchangeRange"></div></div>
                                    </div>
            
                                    <div class="row">
            
                                        <label class="control-label col-md-2">Total Landed Cost</label>
                                        <div class="col-md-1"><div id="impTotalLandedCost"></div></div>
                                        <label class="control-label col-md-2">Import Duty</label>
                                        <div class="col-md-1"><div id="impID"></div></div>
                                    </div>
                                    <div class="row">
            
                                        <label class="control-label col-md-2">Delivery Order</label>
                                        <div class="col-md-1"><div id="impDo"></div></div>
                                        <label class="control-label col-md-2">CMR comp Fee</label>
                                        <div class="col-md-1"><div id="impCmF"></div></div>
                                    </div>
                                    <div class="row">
            
                                        <label class="control-label col-md-2">LCL transport fee</label>
                                        <div class="col-md-1"><div id="impLTF"></div></div>
            
                                        <label class="control-label col-md-2">Cargo Auto Fee</label>
                                        <div class="col-md-1"><div id="impca"></div></div>
                                    </div>
                                    <div class="row">
            
                                        <label class="control-label col-md-2">Port Service Fee</label>
                                        <div class="col-md-1"><div id="impps"></div></div>
            
                                        <label class="control-label col-md-2">Customs Clearance</label>
                                        <div class="col-md-1"><div id="impcc"></div></div>
                                    </div>
                                    <div class="row">
            
                                        <label class="control-label col-md-2">Transport Fee</label>
                                        <div class="col-md-1"><div id="imptp"></div></div>
            
                                        <label class="control-label col-md-2">AQIS Fee</label>
                                        <div class="col-md-1"><div id="impAFt"></div></div>
                                    </div>
                                    <div class="row">
            
                                        <label class="control-label col-md-2">Insurance Fee</label>
                                        <div class="col-md-1"><div id="impIf"></div></div>
            
                                        <label class="control-label col-md-2">Dec Processing Fee</label>
                                        <div class="col-md-1"><div id="impDp"></div></div>
                                    </div>
                                    <div class="row">
            
                                        <label class="control-label col-md-2">Misc. Fee</label>
                                        <div class="col-md-1"><div id="impMf"></div></div>
                                        <label class="control-label col-md-2">Other 1 </label>
                                        <div class="col-md-1"><div id="impo1"></div></div>
                                    </div>
                                    <div class="row">
            
            
                                        <label class="control-label col-md-2">Other 2</label>
                                        <div class="col-md-1"><div id="impo2"></div></div>
                                        <label class="control-label col-md-2">Other 3</label>
                                        <div class="col-md-1"><div id="impo3"></div></div>
            
                                    </div>
                                    <div class="row">
            
                                        <label class="control-label col-md-2">Total Product Cost</label>
                                        <div class="col-md-1"><div id="impTPc"></div></div>
                                        <label class="control-label col-md-2">Avg. Unit Cost </label>
                                        <div class="col-md-1"><div id="impunitcost"></div></div>
            
                                    </div>
                                    <div class="row">
            
                                        <label class="control-label col-md-2">Mark Up On Cost</label>
                                        <div class="col-md-1"><div id="impMarkup"></div></div>
                                        <label class="control-label col-md-2">WholeSale Price </label>
                                        <div class="col-md-1"><div id="impWp"></div></div>
            
                                    </div>
                                    <div class="row">
            
                                        <label class="control-label col-md-2">Gross Profit</label>
                                        <div class="col-md-1"><div id="impGP"></div></div>
                                        <label class="control-label col-md-2">Total Revenue </label>
                                        <div class="col-md-1"><div id="impTotalRevenue"></div></div>
            
                                    </div>
                                    <div class="row">
            
                                        <label class="control-label col-md-2">Total Cost</label>
                                        <div class="col-md-1"><div id="impTC"></div></div>
                                        <label class="control-label col-md-2">Gross Total</label>
                                        <div class="col-md-1"><div id="impgross"></div></div>
            
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            </div>-->
        </div>
        <!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- Bootstrap modal -->
<div class="modal fade" id="impmodal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Imported Product Form</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="importform" class="form-horizontal">
                    <input type="hidden" value="" name="importedid"/> 
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Description</label>
                            <div class="col-md-9">
                                <input name="importedproduct_description" placeholder="Product Description" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Quantity</label>
                            <div class="col-md-9">
                                <input name="ImportQuantity" placeholder="Quantity" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Unit Cost</label>
                            <div class="col-md-9">                                
                                <input name="imported_unit_cost" placeholder="Unit Cost" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>   
                        <div class="form-group">
                            <label class="control-label col-md-3">Exchange Rate</label>
                            <div class="col-md-9">                                
                                <input name="importedexchange_range" placeholder="Exchange Rate" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>   

                        <div class="form-group">
                            <label class="control-label col-md-3">Markup on Cost</label>
                            <div class="col-md-9">                                
                                <input name="imported_markup_on_cost" placeholder="Markup on Cost" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>   
                        <div class="form-group">
                            <label class="control-label col-md-3">Picture</label>
                            <div class="col-md-9">                                
                                <input name="importedflnFile" placeholder="Markup on Cost" class="form-control" type="file">
                                <span class="help-block"></span>
                            </div>
                        </div>   
                    </div>
                    <div class="modal-footer">
                        <input type="submit" id="btnSaveImport" value="Save" class="btn btn-primary">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->