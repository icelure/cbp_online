<style type="text/css">
.process-step .btn:focus{outline:none}
.process{display:table;width:100%;position:relative}
.process-row{padding-top: 5px}
.process-step button[disabled]{opacity:1 !important;filter: alpha(opacity=100) !important}
.process-row:before{top:40px;bottom:0;position:absolute;content:" ";width:100%;height:1px;background-color:#ccc;z-order:0}
.process-step{display:table-cell;text-align:center;position:relative}
.process-step p{margin-top:4px}
.btn-circle{width:65px;height:65px;text-align:center;font-size:12px;border-radius:50%}
/*.tab-content{margin: 0 10% 0 10%;}*/
.error{color:rgba(255, 0, 0, 0.62);}
.input-group{width: 100%}
.img-circle {
    border-radius: 50%;

}

}
tr.selected {
    background-color: #B0BED9 !important;
}
}
h1,p{
color:white;
}
.bordered th, .bordered td{
padding:10px;
}
.bordered tbody tr:nth-child(odd){
background:#eee;
color:#000;
}
.bordered tbody tr:nth-child(even){
color:#fff;

}
.nav>li>a {
    padding: 7px 3px;
}


.tab-header{
  background-color:#ecf0f5; border: 0px solid blue; padding: 1px;
}


.nav-pills>li{
    margin-right: 5px;
    margin-bottom: -1px;

}
.nav-pills>li.active>a, .nav-pills>li.active>a:focus, .nav-pills>li.active>a:hover {
    color: #fff;
    background-color: #fafafa;
}
.nav-pills>li.active>a, .nav-pills>li.active>a:hover, .nav-pills>li.active>a:focus {
    border-top-color: #60979c;
    border-bottom-color: transparent;
}

.nav-pills>li>a, .nav-pills>li>a:hover {
    background-color: #d4d7dc;
}

.nav-pills>li>a {
    border-radius: 0;
}

@media (max-width: 920px){
  #tabs {
    display: none;
  }
  .nav-pills>li {
    width: 75px;
    text-align: center;
  }
}

@media (max-width: 525px){
  #tabs {
    display: none;
  }
  .nav-pills>li {
    width: 40px;
    text-align: center;
  }
}

 .input-group .input-group-addon {
       
        border: 1px solid #ccc;
        width: 48px !important;

</style>
<section class="content-header">

  <h1>Peoples</h1>

  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">People</a></li>
  </ol>
</section>

<section class="content">
  <div class="tab-header">
    <ul class="process-row nav nav-pills">
      <li class="nav-item active">
        <a href="#menu0" data-toggle="tab"><p style="color:rgba(0, 0, 0, 1.0); border: 0px solid blue; margin:0 5px 10px;"><i class="glyphicon glyphicon-info-sign"></i> <strong id="tabs">About</strong></p>
        </a>
      </li>
      <li class="nav-item">
        <a href="#menu1" data-toggle="tab"><p style="color:rgba(0, 0, 0, 1.0); border: 0px solid blue; margin:0 5px 10px;"><i class="glyphicon glyphicon-user"></i> <strong id="tabs">Peoples</strong></p></a>
      </li>
      <li class="nav-item">
        <a href="#menu3" data-toggle="tab"><p  style="color:rgba(0, 0, 0, 1.0); border: 0px solid blue; margin:0 5px 10px;"><i class="glyphicon glyphicon-thumbs-up"></i> <strong id="tabs">Interaction</strong></p></a>
      </li>
      <li class="nav-item">
        <a href="#menu4" data-toggle="tab"><p  style="color:rgba(0, 0, 0, 1.0); border: 0px solid blue; margin:0 5px 10px;"><i class="glyphicon glyphicon-save-file"></i> <strong id="tabs">Summary</strong></p></a>
      </li>
    </ul>
  </div><!-- End Tab Header -->

  <div class="tab-content clearfix">
    <div id="menu0" class="tab-pane fade active in"> <!-- Start Menu 0 -->
      <div style="background-color: rgba(250, 250, 250, 1.00); border: 0px solid blue; padding: 0px;">
        <div class="box-header with-border">
            <h3 class="box-title" style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-size:22px;font-weight:thin;"><strong>What is people Income ?</strong></h3>
            <ul class="list-unstyled list-inline pull-right">
                <li><a class="btn btn-primary view-pdf pull-right" href="<?php echo asset_url(); ?>HTML/ExpensesHelp.html">Help</a></li>
            </ul>
        </div><!-- /.box-header -->
        <div class="box-body">
          <p style="color:rgba(0, 0, 0, 1.0);font-size:14px;font-weight:light;">If you are establsihing a company that will be selling products,Revenue By products is  the total amount of revenue received by the company for the sale of products over a period of time,usualy calculated on a weekly bases.</p>

          <h3 class="box-title"style="color:#3c8dbc;font-size:22px;font-weight:bold;">Creating Products</strong></h3>
          <p style="color:rgba(0, 0, 0, 1.0);font-size:14px;font-weight:light;">In this module,you you can add a product,add an image for the product, view a slide show of all your product, and calculate cost and projected revenue based oon a percntage markup on cost.Values are automaticaly calculated and posted to the relevant documents for easy printing and reporting.</p>

          <h3 class="box-title"style="color:#3c8dbc;font-size:22px;font-weight:bold;">What's next </strong></h3>
          <p style="color:rgba(0, 0, 0, 1.0);font-size:14px;font-weight:light;">Click on the Next button to start creating your products for evaluation.</p>
        </div><!-- /.box-body -->
        <div class="box-footer">
          <ul class="list-unstyled list-inline pull-right">
            <li>
              <button type="button" class="btn btn-info next-step">Next <i class="fa fa-chevron-right"></i></button>
            </li>
          </ul>
        </div>
      </div>
    </div> <!-- End Menu 0 -->

    <div id="menu1" class="tab-pane fade in"> <!-- Start Menu 1 -->
      <div style="background-color: rgba(250, 250, 250, 1.00); border: 0px solid blue; padding: 0px;">
          <div class="box-header with-border">
              <h2 style="color:#3c8dbc;font-size:22px;font-weight:bold;">People</h2>
          </div><!-- /.box-header -->

          <div class="box-body">
              <!-- text input -->
              <div id="show_people">
                <?php include 'people_view.php'; ?>
              </div>
          </div>
          <div class="box-footer">
              <ul class="list-unstyled list-inline pull-right">
                <li>
                  <button type="button" class="btn btn-default prev-step"><i class="fa fa-chevron-left"></i> Back</button>
                </li>
                <li>
                  <button type="button" class="btn btn-info next-step" id="report_section">Next <i class="fa fa-chevron-right"></i></button>
                </li>
              </ul>
          </div>
      </div>
    </div> <!-- End Menu 1 -->
    <div id="menu3" class="tab-pane fade in">
      <!--<div class="container">-->
      <div class="box box-warning"style="background-color: rgba(250, 250, 250, 1.00); border: 0px solid blue; padding: 0px;">
          <div class="box-header with-border">
              <ul class="list-unstyled list-inline pull-right">
                  <li>
                      <a class="btn btn-primary view-pdf pull-right" href="<?php echo asset_url(); ?>HTML/ExpensesHelp.html">Help</a>
                  </li>
              </ul>
              <h3 class="box-title" style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-size:22px;font-weight:thin;"><strong>Online Business Planning</strong></h3>
              <ul class="list-unstyled list-inline pull-right"></ul>
              <h2 style="color:#3c8dbc;font-size:22px;font-weight:bold;">Planning is the key to your success !</h2>
          </div><!-- /.box-header -->
          <div class="box-body">
              <div class="row" id="people_summary">
                  <?php include 'people_summary_view.php'; ?>
              </div>
          </div>
          <div class="box-footer">
            <ul class="list-unstyled list-inline pull-right">
                <li><button type="button" class="btn btn-default prev-step"><i class="fa fa-chevron-left"></i> Back</button></li>
                <li><button type="button" class="btn btn-info next-step" id="summary_section">Next <i class="fa fa-chevron-right"></i></button></li>
            </ul>
          </div>
        </div>
    </div> <!-- End Menu 3 -->
    <div id="menu4" class="tab-pane fade in">
        <div style="background-color: rgba(250, 250, 250, 1.00);; border: 0px solid blue; padding: 0px;">
            <div class="box-header with-border box-header with-border col-sm-12 col-md-12 col-lg-12">
                <h3 class="box-title"style="font-size:17px;font-weight:light;"><strong>You have completed setting up your products</strong></h3>
                <ul class="list-unstyled list-inline pull-right">
                    <li><a class="btn btn-primary view-pdf pull-right" href="<?php echo asset_url(); ?>HTML/ExpensesHelp.html">Help</a></li>
                </ul>
                <h2 style="font-size:25px;font-weight:light;"></h2>
                <p style="font-size:17px;font-weight:light;"><strong>Note:</strong>click on each option and enter your text </p>
            </div><!-- /.box-header -->
            <div class="box-body">
                <p style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-weight:light;">For a company, this is the total amount of revenue received by the company for services rendered fo s specific period of time </p>
                <p style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-weight:light;">Personal Services Income (PSI) is income mainly derived from an individual’s personal efforts and skill.Generally, consultants and contractors operate as a sole trader or work through a company,that charges fees on an hourly basis or a fixes job rate. </p>
                <p style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-weight:light;">In this moduel , you can enter a service, based on a number of hour your worded during w weekly cycle, and wnter the rate per hour that you may chager ad you can include a call out fee fro this service .  </p>
            </div><!-- /.box-body -->
            <div class="box-footer">
                <ul class="list-unstyled list-inline pull-right">
                <li><button type="button" class="btn btn-default prev-step"><i class="fa fa-chevron-left"></i> Back</button></li>
                <li><button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Done!</button></li>
            </ul>
            </div>
        </div>
    </div><!-- End Menu 4 -->
  </div> <!-- End Tab Content -->
</Section> <!-- End Section Body -->
<div class="modal fade" id="personmodal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Add People Form</h3>
            </div>
            <div class="modal-body form">
                <div class="tab-header">
                    <ul class="nav nav-pills" style="top:40px;">
                      <li class="nav-item active">
                        <a href="#form1" data-toggle="tab"><p style="color:rgba(0, 0, 0, 1.0); border: 0px solid blue; margin:0 5px 10px;"><i class="glyphicon glyphicon-info-sign"></i> <strong id="tabs">About</strong></p>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a href="#form2" data-toggle="tab"><p style="color:rgba(0, 0, 0, 1.0); border: 0px solid blue; margin:0 5px 10px;"><i class="glyphicon glyphicon-user"></i> <strong id="tabs">Liability</strong></p></a>
                      </li>
                      <li class="nav-item">
                        <a href="#form3" data-toggle="tab"><p style="color:rgba(0, 0, 0, 1.0); border: 0px solid blue; margin:0 5px 10px;"><i class="glyphicon glyphicon-user"></i> <strong id="tabs">Annuation</strong></p></a>
                      </li>
                    </ul>
                </div><!--end tab header-->
                <form action="#" id="personform" class="form-horizontal" enctype="multipart/form-data">
                <div class="tab-content clearfix">
                    <div id="form1" class="tab-pane fade active in">
                        <div style="border: 0px solid blue; margin: 10px;">
                        <input type="hidden" value="" name="personid"/>
                        <div class="form-body">
                            <div class="form-group">
                                <label class="control-label col-md-4">First Name</label>
                                <div class="col-md-8">
                                  
                                   <div class="input-group">
                                      <div class="input-group-addon">
                                          <i class="fa fa-pencil"></i>
                                      </div>
                                    <input name="person_fname" placeholder="First Name" class="form-control" type="text" required>
                                  <span class="help-block"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">Last Name</label>
                                <div class="col-md-8">
                                   

                                    <div class="input-group">
                                      <div class="input-group-addon">
                                          <i class="fa fa-pencil"></i>
                                      </div>
                                    <input name="person_lname" placeholder="Last Name" class="form-control" type="text" required>
                                    <span class="help-block"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">Hour Work</label>
                                <div class="col-md-8">
                                  
                                  <div class="input-group">
                                      <div class="input-group-addon">
                                          <i class="fa fa-calendar"></i>
                                      </div>
                                    <input name="person_hour_work" placeholder="Hour Work" class="form-control" type="number" required>
                                  <span class="help-block"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">Rates / Hour</label>
                                <div class="col-md-8">
                                    <div class="input-group">
                                      <div class="input-group-addon">
                                          <i class="fa fa-dollar"></i>
                                      </div>
                                      <input name="person_rates" placeholder="Rates/Hour" class="form-control" type="number" required>
                                      <span class="help-block"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">Subsidies Received</label>
                                <div class="col-md-8">
                                  <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-percent"></i>
                                    </div>
                                    <input name="person_subsidi" placeholder="Subsidies Received" class="form-control" type="number">
                                    <span class="help-block"></span>
                                  </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">Commission Received</label>
                                <div class="col-md-8">
                                  <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-percent"></i>
                                    </div>
                                    <input name="person_commission" placeholder="Commission Received" class="form-control" type="number">
                                    <span class="help-block"></span>
                                  </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">Other</label>
                                <div class="col-md-8">
                                  <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-percent"></i>
                                    </div>
                                    <input name="person_other" placeholder="Other" class="form-control" type="number">
                                    <span class="help-block"></span>
                                  </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">Picture</label>
                                <div class="col-md-8">
                                    <input name="personflnFile" placeholder="Picture" class="form-control" type="file">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            </div>
                        </div>
                        </div><!--end form1-->
                        <div id="form2" class="tab-pane fade in">
                            <div style="border: 0px solid blue; margin: 10px;">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-4">Pension</label>
                                    <div class="col-md-8">
                                      <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-percent"></i>
                                        </div>
                                        <input name="person_pension" placeholder="Pension" class="form-control" type="number">
                                        <span class="help-block"></span>
                                      </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">Medicare Levi</label>
                                    <div class="col-md-8">
                                      <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-percent"></i>
                                        </div>
                                        <input name="person_medicare" placeholder="Medicare Levi" class="form-control" type="number">
                                        <span class="help-block"></span>
                                      </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">Retirement Annuity</label>
                                    <div class="col-md-8">
                                      <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-percent"></i>
                                        </div>
                                        <input name="person_retire" placeholder="Retirement Annuity" class="form-control" type="number">
                                        <span class="help-block"></span>
                                      </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">Income Tax (PAYG)</label>
                                    <div class="col-md-8">
                                      <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-percent"></i>
                                        </div>
                                        <input name="person_tax" placeholder="Income Tax (PAYG)" class="form-control" type="number">
                                        <span class="help-block"></span>
                                      </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">Union Fee</label>
                                    <div class="col-md-8">
                                      <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-percent"></i>
                                        </div>
                                        <input name="person_union" placeholder="Union Fee" class="form-control" type="number">
                                        <span class="help-block"></span>
                                      </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">Sick leave</label>
                                    <div class="col-md-8">
                                      <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-percent"></i>
                                        </div>
                                        <input name="person_sick" placeholder="Sick leave" class="form-control" type="number">
                                        <span class="help-block"></span>
                                      </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">Fringe Benefit Tax</label>
                                    <div class="col-md-8">
                                      <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-percent"></i>
                                        </div>
                                        <input name="person_fringe" placeholder="Fringe Benefit Tax" class="form-control" type="number">
                                        <span class="help-block"></span>
                                      </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">Other Deductions</label>
                                    <div class="col-md-8">
                                      <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-percent"></i>
                                        </div>
                                        <input name="person_deductions" placeholder="Other Deductions" class="form-control" type="number">
                                        <span class="help-block"></span>
                                      </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div><!--end form2-->
                        <div id="form3" class="tab-pane fade in">
                            <div style="border: 0px solid blue; margin: 10px;">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-4">Super Annuation</label>
                                    <div class="col-md-8">
                                      <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-percent"></i>
                                        </div>
                                        <input name="person_superannuation" placeholder="Super Annuation" class="form-control" type="number">
                                        <span class="help-block"></span>
                                      </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">Work Cover</label>
                                    <div class="col-md-8">
                                      <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-percent"></i>
                                        </div>
                                        <input name="person_workcover" placeholder="Work Cover" class="form-control" type="number">
                                        <span class="help-block"></span>
                                      </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div><!--end form3-->
                    </div><!--end tab content-->
                    <div class="modal-footer">
                        <input type="submit" id="btnSavePerson" value="Save" class="btn btn-primary">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!--Modal-->

<script type="text/javascript">
    $(function () {

        //$('#start_date').daterangepicker({singleDatePicker: true});
        $("[data-mask]").inputmask();
        $('.btn-circle').on('click', function () {
            $('.btn-circle.btn-info').removeClass('btn-info').addClass('btn-default');
            $(this).addClass('btn-info').removeClass('btn-default').blur();
        });
        $('.next-step, .prev-step').on('click', function (e) {
            var $activeTab = $('.tab-pane.active');
            $('.btn-circle.btn-info').removeClass('btn-info').addClass('btn-default');
            if ($(e.target).hasClass('next-step'))
            {
                var nextTab = $activeTab.next('.tab-pane').attr('id');
                $('[href="#' + nextTab + '"]').removeClass('btn-default');
                $('[href="#' + nextTab + '"]').tab('show');
                $("body").scrollTop(0);
            } else
            {
                var prevTab = $activeTab.prev('.tab-pane').attr('id');
                $('[href="#' + prevTab + '"]').removeClass('btn-default');
                $('[href="#' + prevTab + '"]').tab('show');
                $("body").scrollTop(0);
            }
        });
        //redraw the datatables
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
          var target = $(e.target).attr("href") // activated tab
          if ((target == '#menu3')) {

              table1.ajax.reload(null, false);

          }
        });
    });
function add_person()
{
    $("label.error").remove();
    save_method = 'add';
    $('#personform')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#personmodal_form').modal('show'); // show bootstrap modal
    $('#personmodal_form').on('shown.bs.modal', function() {
        $('.nav a[href="#form1"]').tab('show');
    })
    $('.modal-title').text('Add People'); // Set Title to Bootstrap modal title
}

function saveperson()
{
    $('#btnSavePerson').text('saving...'); //change button text
    $('#btnSavePerson').attr('disabled', true); //set button disable
    var url;

    if (save_method == 'add') {
        url = "<?php echo site_url('people/ajax_addperson');?>";
    } else {
        url = "<?php echo site_url('people/ajax_updateperson') ?>";
    }


    // ajax adding data to database
    $.ajax({
        url: url,
        type: "POST",
        data: new FormData($('#personform')[0]),
        contentType: false,
        cache: false,
        processData: false,
        success: function (data)
        {

            var obj = jQuery.parseJSON(data);

            if (obj['status']) //if success close modal and reload ajax table
            {
                $('#personmodal_form').modal('hide');
                drawCollectionView()
                table1.ajax.reload(null, false);
            }

            $('#btnSavePerson').text('save'); //change button text
            $('#btnSavePerson').attr('disabled', false); //set button enable
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            $('#btnSavePerson').text('save'); //change button text
            $('#btnSavePerson').attr('disabled', false); //set button enable

        }
    });
}

function delete_people(id){

    if (confirm('Are you sure delete this data?')){
            // ajax delete data to database
        $.ajax({
            url: "<?php echo site_url('people/ajax_delete') ?>/" + id,
            type: "POST",
            dataType: "JSON",
            success: function (data)
            {
                //if success reload ajax table
                $('#modal_form').modal('hide');
                drawCollectionView();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert(errorThrown);

            }
        });

    }
}

function edit_people(id){

    $("label.error").remove();
    save_method = 'update';
    $('#personform')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string

    //Ajax Load data from ajax
    $.ajax({
        url: "<?php echo site_url('people/ajax_edit/') ?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function (data)
        {
            $('[name="personid"]').val(data.id);
            $('[name="person_fname"]').val(data.f_name);
            $('[name="person_lname"]').val(data.l_name);
            $('[name="person_hour_work"]').val(data.hour_worked);
            $('[name="person_rates"]').val(data.rates);
            $('[name="person_subsidi"]').val(data.subsidies);
            $('[name="person_commission"]').val(data.commission);
            $('[name="person_other"]').val(data.other);
            $('[name="person_pension"]').val(data.pension);
            $('[name="person_medicare"]').val(data.medicare_levi);
            $('[name="person_retire"]').val(data.retirement_annuity);
            $('[name="person_tax"]').val(data.income_tax);
            $('[name="person_union"]').val(data.union_fee);
            $('[name="person_sick"]').val(data.sick_leave);
            $('[name="person_fringe"]').val(data.fringe_benefit);
            $('[name="person_deductions"]').val(data.other_deduction);
            $('[name="person_superannuation"]').val(data.superannuation);
            $('[name="person_workcover"]').val(data.work_cover);
            $('#personmodal_form').modal('show'); // show bootstrap modal when complete loaded
            $('#personmodal_form').on('shown.bs.modal', function() {
                $('.nav a[href="#form1"]').tab('show');
            })
            $('.modal-title').text('Edit People'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
           alert(errorThrown);
       }
    });
}

function loaddetail(id){

    //Ajax Load data from ajax
    $.ajax({
        url: "<?php echo site_url('people/ajax_get_personal_detail') ?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function (data)
        {

            $("#imgthumbnail").attr("src", data.thumbnail);
            $("#person_fname").html(data.f_name);
            $("#person_lname").html(data.l_name);
            $("#personid").html(data.id);
            $("#person_hour_work").html(data.hour_worked);
            $("#person_rates").html(data.rates);
            $("#weekly_salary").html(data.weekly_salary);
            $("#commission").html(data.commission);
            $("#commission_recieved").html(data.commission_recieved);
            $("#subsidies").html(data.subsidies);
            $("#subsidies_recieved").html(data.subsidies_recieved);
            $("#other").html(data.other);
            $("#other_recieved").html(data.other_recieved);
            $("#total_gross_salary").html(data.total_gross_salary);

            //tab3
            $("#gross_salary").html(data.total_gross_salary);
            $("#pension").html(data.pension);
            $("#pension_d").html(data.pension_d);
            $("#medicare").html(data.medicare);
            $("#medicare_d").html(data.medicare_d);
            $("#retirement").html(data.retirement_annuity);
            $("#retirement_d").html(data.retirement_annuity_d);
            $("#income_tax").html(data.income_tax);
            $("#income_tax_d").html(data.income_tax_d);
            $("#union_fee").html(data.union_fee);
            $("#union_fee_d").html(data.union_fee_d);
            $("#sick_leave").html(data.sick_leave);
            $("#sick_leave_d").html(data.sick_leave_d);
            $("#fringe_benefit").html(data.fringe_benefit);
            $("#fringe_benefit_d").html(data.fringe_benefit_d);
            $("#other_deduction").html(data.other_deduction);
            $("#other_deduction_d").html(data.other_deduction_d);

            $("#total_deductions_d").html(data.total_deductions_d);
            $("#net_salary_d").html(data.net_salary_d);

            //tab4
            $("#gross_salary_4").html(data.total_gross_salary);
            $("#superannuation").html(data.superannuation);
            $("#superannuation_d").html(data.superannuation_d);
            $("#work_cover").html(data.work_cover);
            $("#work_cover_d").html(data.work_cover_d);
            $("#total_annuation_d").html(data.total_annuation_d);
            $("#total_payroll_d").html(data.total_payroll_d);

            $('#personmodal_form_Detail').modal('show');

            $('#personmodal_form_Detail').on('shown.bs.modal', function() {
                $('.nav a[href="#tab1"]').tab('show');
            })
            //}); // show bootstrap modal when complete loaded

            $('.modal-title').text('Personal Detail'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert(errorThrown);

        }
    });
    // alert(id);
}
$('#personmodal_form_Detail').on('show', function(e){
    alert(e);
});
</script>