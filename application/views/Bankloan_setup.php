
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/daterangepicker/daterangepicker-bs3.css">
<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/iCheck/all.css">
<link rel="stylesheet" href="<?php echo asset_url(); ?>css/model.css">
<script src="<?php echo asset_url(); ?>js/model.js"></script>



<style type="text/css">
  .process-step .btn:focus{outline:none}
  .process{display:table;width:100%;position:relative}
  .process-row{display:table-row}
  .process-step button[disabled]{opacity:1 !important;filter: alpha(opacity=100) !important}
  .process-row:before{top:40px;bottom:0;position:absolute;content:" ";width:100%;height:1px;background-color:#ccc;z-order:0}
  .process-step{display:table-cell;text-align:center;position:relative}
  .process-step p{margin-top:4px}
  .btn-circle{width:65px;height:65px;text-align:center;font-size:12px;border-radius:50%}
  .tab-content{margin: 0 10% 0 10%;}
  .error{color:rgba(255, 0, 0, 0.62);}
  .input-group{width: 100%}
  .img-circle {
    border-radius: 50%;
}
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even) {
    background-color: #fff;
}
      
  }

h1,p{
color:white;
    
}

</style>

      
      
      <section class="content-header">
  <!--<h1>
    Loan Calculator
   
  </h1>-->
  <!--<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Loan Calculator settings</a></li>
    <!-- <li class="active">Data tables</li> 
  </ol>-->
</section>

<!-- Main content -->

  <div class="process">
   <div class="process-row nav nav-tabs">
    <div class="process-step">
     <button type="button" class="btn btn-info btn-circle" data-toggle="tab" href="#menu0"><i class="fa fa-rocket fa-3x"></i></button>
     <p style="color:#FFFFFF;font-size:35px;font-weight:light;"><strong>Calculate Your<br />Business Loan</strong></p>
   </div>
   
 
  
   
 </div>
</div>
      
      
<div class="tab-content">
    <div id="menu0" class="tab-pane fade active in">
      <div class="box box-solid"style="background-color: rgba(0, 0, 0, 0.10); border: 0px solid blue; padding: 0px;">
        <div class="box-header with-border">
        <h3 class="box-title" style="color:#FFFFFF;font-size:35px;font-weight:light;"><strong>Need a loan ? <br>Loan Calculator</strong></h3>
      <ul class="list-unstyled list-inline pull-right">
      <li><a class="btn btn-primary view-pdf pull-right" href="<?php echo asset_url(); ?>HTML/ExpensesHelp.html">Help</a></li>
      </ul>
      
        <h2>Enter Loan Data !</h2>
        <p style="color:#FFFFFF;font-size:17px;font-weight:light;"><strong>Note:</strong>  <strong></strong> Loan Balance, Cumulative Equity, and Interest Payments </p>
      </div><!-- /.box-header -->
    <div class="box-body">
      
      <p style="color:#FFFFFF;font-size:22px;font-weight:light;">
	  <form role="form" method="post" action=""  name="bank" enctype="multipart/form-data">
	  <div class="form-group col-md-6">
	  
            <label>Amount of the loan ($):</label>
			<div class="input-group">
                    <div class="input-group-addon">
                      <i data-rd-id="r1_pgt" class="fa fa-dollar"></i>
                    </div>
			<input id="amount" name="amount" class="form-control" value="<?php echo $loan_amount;?>">
           </div>
          </div>
		  <div class="form-group col-md-6">
		   <label>Repayment period (years):</label>
                    <div class="input-group">
                    <div class="input-group-addon">
                      <i data-rd-id="r1_pgt" class=""></i>
                    </div>
                  <input id="apr" name="apr" class="form-control" value="<?php echo $loan_length;?>">
                    </div>
            
           
			
           
          </div>
		   <div class="form-group col-md-6">
            <label>Annual interest (%):</label>
			<div class="input-group">
                    <div class="input-group-addon">
                      <i data-rd-id="r1_pgt" class="fa fa-percent"></i>
                    </div>
			<input id="years" name="years" class="form-control" value="<?php echo $annual_interest; //$annual_interest;?>">
            </div>
          </div>
		     <div class="form-group col-md-6">
            <label>Perodicity value:</label>
			
			<select name="pay_periodicity" class="form-control"> 
			<option value="52" <?php if($pay_periodicity1==52) echo 'selected';?>>Weekly</option> 
			<option value="26"<?php if($pay_periodicity1==26) echo 'selected';?>>Bi-weekly</option> 
			<option value="12" <?php if($pay_periodicity1==12) echo 'selected';?>>Monthly</option> 
			<option value="6" <?php if($pay_periodicity1==6) echo 'selected';?>>Bi-monthly</option> 
			<option value="4" <?php if($pay_periodicity1==4) echo 'selected';?>>Quarterly</option> 
			<option value="2" <?php if($pay_periodicity1==2) echo 'selected';?>>Semi-annually</option> 
			<option value="1" <?php if($pay_periodicity1==1) echo 'selected';?>>Annually</option> </select>
              </div>
         
			
          
		  <div class="form-group col-md-6">
            <label>Approximate Payments:</label>
			
			<button class="form-control" style="background-color: rgba(53, 138, 187, 0.31);
    border-color: #00acd6;text-color:white;    font-size: 17px;
    font-weight: bold;" type="submit" id="btn" name="btn"> Calculate</button>
	
           
          </div></form>
		  
		  <div class="form-group col-md-12"></div>
<?php if($is_post==true) { ?>		 
		 <div class="form-group col-lg-6 col-md-12 col-xs-12" style="margin: 0 auto; float: none;">
		  
            <table cellpadding=5 width=100%  align="center" class=bordered style="margin-bottom: 10px;">
	<tr>
		<th colspan=4 style="background: rgba(0, 0, 0, 0.36);color:#fff;">Loan Summary</th>
	</tr>
	<tr>
		<td>Loan amount:</td>
		<td><b>$<?php echo $loan_amount;?></b></td>
	</tr>
	<tr>
		<td>Loan length:</td>
		<td><b><?php echo $loan_length;?>&nbsp;years</b></td>
	</tr>
	<tr>
		<td>Annual interest:</td>
		<td><b><?php echo $annual_interest;?>%</b></td>
	</tr>
	
	
	<tr>
		<td>Total paid:</td>
		<td><b>$<?php echo $total_paid;?></b></td>
	</tr>
	<tr>
		<td>Total interest:</td>
		<td><b>$<?php echo $total_interest;?></b></td>
	</tr>
	<tr>
		<td>Total periods:</td>
		<td><b><?php echo $total_periods;?></b></td>
	</tr>
</table>
          
		  <!-- BEGIN amortization_table -->
<div style="width:100%; overflow:auto; ">
<table id="fullsummary" class=bordered cellpadding=5 align="center">
<thead style="background: rgba(0, 0, 0, 0.36);color:#fff;">
	<tr>
		<th>Period</th><th>Interest Paid</th><th>Principal Paid</th><th>Remaining Balance</th>
	</tr>
	</thead>
	<tbody>
	<?php echo $amortization_table_rows;?>
	</tbody>
	<tfoot>
	<tr>
		<th>Totals:</th><th>$<?php echo $total_interest;?></th><th>$<?php echo $total_principal; ?></th><th>&nbsp;</th>
	</tr></tfoot>
</table>
</div>
<!-- END amortization_table -->
		  
		  
		  
</div><?php } ?>
		  
</p>
</div><!-- /.box-body -->
      </div>
     
    </div>
      

      
      
      
      
      

 <div id="menu1" class="tab-pane fade in">
  <div class="box box-warning">
    <div class="box-header with-border">
      <h3 class="box-title">Start Calculation</h3>
    </div><!-- /.box-header -->
   
     
       
        
            <label>Company Name</label>
            <input type="text" class="form-control" name="company_name" id="company_name" value="<?php print $company_name; ?>" placeholder="Enter your company name">
          </div>
         
          
        </div>
      </div>
      
    <!-- /.content -->
     <script src="<?php echo asset_url(); ?>plugins/daterangepicker/daterangepicker.js"></script>
     <script src="<?php echo asset_url(); ?>plugins/input-mask/jquery.inputmask.js"></script>
     <script src="<?php echo asset_url(); ?>plugins/iCheck/icheck.min.js"></script>
     <script type="text/javascript" src="<?php echo js_url(); ?>jquery.validate.min.js"></script>
     <script type="text/javascript" src="<?php echo js_url(); ?>additional-methods.min.js"></script>

<script>

 //$(document).ready(function(){   
//$("#summery").css("display","none");
 //$("#fullsummary").css("display","none");
   //  $("#btn").click(function()
     //{ 
 
     // $.ajax({
         // type: "POST",
         // url: "<?php echo $baseurl;?>Bankloan_setup/checkreq", 
         // data: {amount: $("#amount").val(),years:$("#years").val(),apr:$("#apr").val(),pay_periodicity:$('select[name=pay_periodicity]').val()},
         // dataType: "text",  
         // cache:false,
         // success: 
              // function(data){
                //alert(data);  //as a debugging message.
				// $("#summery").css("display","block");
				// $("#fullsummary").css("display","block");
				// var ob=data.split("|");
				// $("#loan").append(ob[0]);
				// $("#loanlnth").append(ob[1]);
				// $("#anualintr").append(ob[2]);
				// $("#totalpaid").append(ob[3]);
				// $("#totalinterst").append(ob[4]);
				// $("#totalperid").append(ob[5]);
				// $("#fullsummary tbody").html(ob[6]);
				// $("#foot1").append(ob[7]);
				// $("#foot2").append(ob[8]);
              // }
          // });// you have missed this bracket
     // return false;
  //});
  //});


</script>
        
