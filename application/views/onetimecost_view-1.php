
    <style type="text/css">
        .input-group .input-group-addon {

        border: 1px solid #ccc;
        width: 48px !important;
    }
    </style>
	<div class="col-md-12" id="one_time_cost">
        <button class="btn btn-success" onclick="add_onetimecost()"><i class="glyphicon glyphicon-plus"></i> <strong id="tabs"> Add One Time Cost</strong></button>
        <button class="btn btn-default" onclick="reload_table2()"><i class="glyphicon glyphicon-refresh"></i> <strong id="tabs"> Reload</strong></button>
        <br />
        <br />
        <table id="table_one" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>One Time Cost</th>
                    <th>Description</th>
                    <th>Amount Paid</th>
                    <th>VAT/GST</th>
					<th>Total VAT/GST</th>
					<th>Total Cost</th>
                    <th style="width:50px;">Action</th>
                </tr>
            </thead>
            <tbody>
            </tbody>

            <tfoot>
            <tr>
					<th>Total One Time Cost</th>
                    <th></th>
                    <th></th>
                    <th></th>
					<th></th>
					<th></th>
                <th></th>
            </tr>
            </tfoot>
        </table>
	</div>



<script type="text/javascript">

var save_method_one; //for save method string
var table_one;

$(document).ready(function() {

    //datatables
    table_one = $('#table_one').DataTable({
		responsive: true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('onetimecost/ajax_list')?>",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        {
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable
        },
        ],
		"footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
			var numFormat = $.fn.dataTable.render.number( '\,', '.' ).display;
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            total = api
                .column( 2 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            // Total over this page
            pageTotal = api
                .column( 2, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            // Update footer
            $( api.column( 2 ).footer() ).html(
                numFormat(pageTotal)
            );

			total1 = api
                .column( 4 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
			pageTotal = api
                .column( 4, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            // Update footer
            $( api.column( 4 ).footer() ).html(
                numFormat(pageTotal)
            );

			total2 = api
                .column( 5 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
			pageTotal = api
                .column( 5, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            // Update footer
            $( api.column( 5 ).footer() ).html(
                numFormat(pageTotal)
            );
        }
    });

    //datepicker
    $('.datepicker').datepicker({
        autoclose: true,
        format: "yyyy-mm-dd",
        todayHighlight: true,
        orientation: "top auto",
        todayBtn: true,
        todayHighlight: true,
    });

});



function add_onetimecost()
{
    save_method_one = 'add';
    $('#form_onetime')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block-one').empty(); // clear error string
    $('#modal_form_one').modal('show'); // show bootstrap modal
    $('.modal-title').text('Add One Time Cost'); // Set Title to Bootstrap modal title
}

function edit_onetimecost(id)
{
    save_method_one = 'update';
    $('#form_onetime')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block-one').empty(); // clear error string

    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo site_url('onetimecost/ajax_edit/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {

            $('[name="id"]').val(data.id);
            $('[name="one_time_cost_type"]').val(data.one_time_cost);
            $('[name="one_time_cost_description"]').val(data.description);
            $('[name="amount_paid"]').val(data.amount_paid);
            $('#modal_form_one').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit One Time Cost'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function reload_table2()
{
	reload_summary();
    table_one.ajax.reload(null,false); //reload datatable ajax
}

function save_onetimecost()
{
    $('#btnSave_one').text('saving...'); //change button text
    $('#btnSave_one').attr('disabled',true); //set button disable
    var url;

    if(save_method_one == 'add') {
        url = "<?php echo site_url('onetimecost/ajax_add')?>";
    } else {
        url = "<?php echo site_url('onetimecost/ajax_update')?>";
    }

    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form_onetime').serialize(),
        dataType: "JSON",
        success: function(data)
        {

            if(data.status) //if success close modal and reload ajax table
            {
                $('#modal_form_one').modal('hide');
                reload_table2();
            }

            $('#btnSave_one').text('save'); //change button text
            $('#btnSave_one').attr('disabled',false); //set button enable


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            $('#btnSave_one').text('save'); //change button text
            $('#btnSave_one').attr('disabled',false); //set button enable

        }
    });
}

function delete_onetimecost(id)
{
    if(confirm('Are you sure delete this data?'))
    {
        // ajax delete data to database
        $.ajax({
            url : "<?php echo site_url('onetimecost/ajax_delete')?>/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                //if success reload ajax table
                $('#modal_form_one').modal('hide');
                reload_table2();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });

    }
}

</script>

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form_one" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">onetimecost Form</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form_onetime" class="form-horizontal">
                    <input type="hidden" value="" name="id"/>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">One Time Cost</label>
                            <div class="col-md-9">
                                <select class="form-control" name="one_time_cost_type" id="one_time_cost_type">
						<option value="">Select</option>
						<option value="Land_Buildings">Land & Buildings</option>
						<option value="Plant_Equipment">Plant & Equipment</option>
						<option value="Security_Deposit">Security Deposit(s)</option>
						<option value="ONE_TIME_COSTS">ONE-TIME COSTS</option>
					 </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Description</label>
                            <div class="col-md-9">


                                <div class="input-group">
                                      <div class="input-group-addon">
                                          <i class="fa fa-pencil"></i>
                                      </div>
                                    <input name="one_time_cost_description" placeholder="Description" class="form-control" type="text">
                                <span class="help-block-one"></span>

                                    </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Amount Paid</label>
                            <div class="col-md-9">

                                <div class="input-group">
                                      <div class="input-group-addon">
                                          <i class="fa <?php if ($currency=="AUD" || $currency=="USD"){echo "fa-dollar";}elseif($currency=="INR"){echo "fa-inr";}elseif($currency=="EUR"){echo "fa-eur";}else{echo "fa-dollar";} ?>"></i>
                                      </div>
                                       <input name="amount_paid" placeholder="Monthly Cost" class="form-control" type="text">
                                <span class="help-block-one"></span>
                                    </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave_one" onclick="save_onetimecost()" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->