
<style type="text/css">
    
    .input-group .input-group-addon {
       
        border: 1px solid #ccc;
        width: 48px !important;
    }
</style>
<div class="col-md-12" id="one_time_cost">
    <button type="button" class="btn btn-success" onclick="add_importedproduct()"><i class="glyphicon glyphicon-plus"></i> <strong id="tabs"> Add Service</strong></button>
    <button type="button" class="btn btn-default" onclick="reload_tableimported()"><i class="glyphicon glyphicon-refresh"></i> <strong id="tabs"> Reload</strong></button>
    <br />
    <br />
<div class="table-responsive">
    <table id="tableimported" class="table table-striped table-bordered">
<colgroup><col width="5%"><col width="25%"><col width="25%"><col width="5%"><col width="10%"><col width="10%"><col width="10%"><col width="10%"></colgroup>
        <thead>
            <tr>

                <th>Service ID</th>
                <th>Contracters</th>
                <th>Description</th>
                <th>Hours Worked</th>
                <th>Rate per Hour</th>
                <th>Call out Fee</th>
                <th>Total</th>
                <th style="width:125px;">Action</th>
            </tr>
        </thead>
        <tbody>
    </tbody>
        
        <tfoot>
        <tr>
            <th colspan="6" style="text-align: right;">Total</th><th class="SumTotal">0</th><th></th>
        </tr>
        </tfoot>
		
    </table>
</div>
</div>

<script type="text/javascript">
$(function () {
    
 $("#importform").validate({
                rules: {
                     impser_serid: {
                         required:true
                    },
                    impser_contractors: {
                        required: true
                        
                    },
                    impser_desc: {
                        required: true
                        
                    },
                    impser_hrswork: {
                        required: true,
                        number: true
                    },
                   impser_rateperhr: {
                        required: true,
                        number: true
                    },
                   impser_callourfee: {
                        required: true,
                        number: true
                    }
                }
            });
});
        var save_method; //for save method string
        var table;

        $(document).ready(function () {


            //datatables

            table = $('#tableimported').DataTable({
                "processing": true, //Feature control the processing indicator.
                "serverSide": true, //Feature control DataTables' server-side processing mode.
                "order": [], //Initial no order.

                // Load data for the table's content from an Ajax source
                "ajax": {
                    "url": "<?php echo site_url('Importedservice/ajax_list') ?>",
                    "type": "POST"
                },
"drawCallback": function(d) {
$('.SumTotal').text(d.json.sumTotal);
    },
                //Set column definition initialisation properties.
                "columnDefs": [
                    {
                        "targets": [-1], //last column
                        "orderable": false, //set not orderable
                    },
                ],
            });

    $("#btnSave").click(function (e) {
                e.preventDefault();
                if ($("#importform").valid()) {
                    saveimported();
                }
      }); 

        });



        function add_importedproduct()
        {
            $("label.error").remove();
            save_method = 'add';
            $('#importform')[0].reset(); // reset form on modals
            $('.form-group').removeClass('has-error'); // clear error class
            $('.help-block').empty(); // clear error string
            $('#impmodal_form').modal('show'); // show bootstrap modal
            $('.modal-title').text('Add Services'); // Set Title to Bootstrap modal title
        }

        function edit_importedproduct(id)
        {

            save_method = 'update';
            $('#importform')[0].reset(); // reset form on modals
            $('.form-group').removeClass('has-error'); // clear error class
            $('.help-block').empty(); // clear error string

            //Ajax Load data from ajax
            $.ajax({
                url: "<?php echo site_url('Importedservice/ajax_editimported') ?>/" + id,
                type: "GET",
                dataType: "JSON",
                success: function (data)
                {
                    //  alert(data.Description);

                    $('[name="impser_serid"]').val(data.ServiceID);
                    $('[name="impser_contractors"]').val(data.Contractors);
                    $('[name="impser_desc"]').val(data.Description);
                    $('[name="impser_hrswork"]').val(data.HoursWorked);
                    $('[name="impser_rateperhr"]').val(data.RatePerHour);

                    $('[name="impser_callourfee"]').val(data.CallOutFee);
                   // $('[name="impser_total"]').val(data.Total);

                    $('#impmodal_form').modal('show'); // show bootstrap modal when complete loaded

                    $('.modal-title').text('Edit Services'); // Set title to Bootstrap modal title
                   
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert(errorThrown);
                }
            });
        }

        function reload_tableimported()
        {
            table.ajax.reload(null, false); //reload datatable ajax 
            reload_tableimported1();
        }
        
        function saveimported()
        { 


          
            $('#btnSave').text('saving...'); //change button text
            $('#btnSave').attr('disabled', true); //set button disable 
            var url;

            if (save_method == 'add') {
                url = "<?php echo site_url('Importedservice/ajax_addimported') ?>";
            } else {
                url = "<?php echo site_url('Importedservice/ajax_updateimported') ?>";
            }
            //loading_show();
            // ajax adding data to database
            $.ajax({
                url: url,
                type: "POST",
                data: new FormData($('#importform')[0]),
                contentType: false,
                cache: false,
                processData: false,
                success: function (data)
                {
                     var obj = jQuery.parseJSON(data);
            		
                    if(obj['status']) //if success close modal and reload ajax table
                    {
                        $('#impmodal_form').modal('hide');
                     /* $.ajax({
                           url: "<?php echo site_url('Upsincome/affect');?>",                           
                           success: function (data)
                           {
                               $("#menu1").find("#monthly_expense_row_add_imported").html(data);
                               loading_hide();
                            
                           }
                         });*/
                        reload_tableimported();
                        
                    }

                    $('#btnSave').text('save'); //change button text
                    $('#btnSave').attr('disabled', false); //set button enable 



                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Error adding / update data');
                    $('#btnSave').text('save'); //change button text
                    $('#btnSave').attr('disabled', false); //set button enable 

                }
            });
        }

        
        function delete_importedproduct(id)
        {
            if (confirm('Are you sure you want to delete this data?'))
            {
               // loading_show();
                // ajax delete data to database
                $.ajax({
                    url: "<?php echo site_url('Importedservice/ajax_deleteimported') ?>/" + id,
                    type: "POST",
                    dataType: "JSON",
                    success: function (data)
                    {
                         
                        //if success reload ajax table
                        $('#impmodal_form').modal('hide');
                        reload_tableimported();
                        //location.reload();
                        /* $.ajax({
                           url: "<?php echo site_url('Upsincome/affect');?>",                           
                           success: function (data)
                           {
                               $("#menu1").find("#monthly_expense_row_add_imported").html(data);
                               loading_hide();
                           }
                         }); */
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert(errorThrown);
                    }
                });

            }
        }

</script>
<style>
#tableimported_filter{
float:right;
}
#tableimported_paginate{
float:right;
}
.table>tbody>tr>td{
padding:0px !important;
}
.btn-group-xs>.btn, .btn-xs{
padding:4px 12px;
}
#modal{
position: fixed;
z-index: 1000;
top: 0;
left: 0;
height: 100%;
width: 100%;
background: rgba( 255, 255, 255, 0.7 ) url('<?php echo base_url(); ?>assets/images/loading.gif') 50% 50% no-repeat;
}
.error{
color:rgba(255, 0, 0, 0.62);
}
</style>

<!-- Bootstrap modal -->
<div class="modal fade" id="impmodal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Imported Product Form</h3>
            </div>
            <div class="modal-body form">
                <form name="importForm" action="#" id="importform" class="form-horizontal">
                    <input type="hidden" value="" name="importedid"/> 
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Service ID</label>
                            <div class="col-md-9">
                                   
                                 

                                      
                                     
                                    <div class="input-group">
                                      <div class="input-group-addon">
                                          <i class="fa fa-tag"></i>
                                      </div>
                                      <input name="impser_serid" placeholder="Service ID" class="form-control" type="number">
                                      <span class="help-block"></span>
                                    </div>
                                                                     
                            </div>
                        </div>
                
                        <div class="form-group">
                            <label class="control-label col-md-3">Contractors</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                      <div class="input-group-addon">
                                          <i class="fa fa-pencil"></i>
                                      </div>
                                     <input name="impser_contractors" placeholder="Contractors" class="form-control" type="text">
                                      <span class="help-block"></span>
                                    </div>
                            </div>
                        </div>



                        <div class="form-group">
                            <label class="control-label col-md-3">Description</label>
                            <div class="col-md-9">                                
                               <div class="input-group">
                                      <div class="input-group-addon">
                                          <i class="fa fa-pencil"></i>
                                      </div>
                                      <input name="impser_desc" placeholder="Description" class="form-control" type="text">
                                <span class="help-block"></span>
                                    </div> 
                            </div>
                        </div> 

                        <div class="form-group">
                            <label class="control-label col-md-3">Hours Worked</label>
                            <div class="col-md-9">                                
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                   <input name="impser_hrswork" placeholder="Hours Worked" class="form-control" type="number">
                                <span class="help-block"></span>
                                  </div>      
                            </div>
                        </div>   

                        <div class="form-group">
                            <label class="control-label col-md-3">Rate Per Hour</label>
                            <div class="col-md-9">                                
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-dollar"></i>
                                    </div>
                                   <input name="impser_rateperhr" placeholder="Rate Per Hour" class="form-control" type="text">
                                <span class="help-block"></span>
                                  </div>      

                            </div>
                        </div>   
                        <div class="form-group">
                            <label class="control-label col-md-3">Call Our Fee</label>
                            <div class="col-md-9">                                                                
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-dollar"></i>
                                    </div>
                                   <input name="impser_callourfee" placeholder="Call Our Fee" class="form-control" type="text">
                                <span class="help-block"></span>

                                  </div>   
                            </div>
                        </div>
						<!-- <div class="form-group">
                            <label class="control-label col-md-3">Total</label>
                            <div class="col-md-9">                                
                                <input name="impser_total" placeholder="Total" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>	-->					
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <!-- <button type="button" id="btnSave" onclick="saveimported()" class="btn btn-primary">Save</button> -->
                <input type="submit" id="btnSave" value="Save" class="btn btn-primary"></button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

