<div class="well well-sm col-sm-12 col-md-12 col-lg-12">
    <strong>Display</strong>
        <div class="btn-group">
            <a href="#" id="list" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th-list"></span>List</a>
            <a href="#" id="grid" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th"></span>Grid</a>
        </div>
    <button style="float: right;"  class="btn btn-success" onclick="add_person()"><i class="glyphicon glyphicon-plus"></i>Personal</button>
</div>
<div id="personal" class="row-height list-group col-sm-12 col-md-12 col-lg-12">

</div>
<script src="<?php echo base_url('assets/plugins/easypaginate/easyPaginate.js') ?>"></script>

<style>

.easyPaginateNav a {
    font-size: 18px;
    padding: 5px 10px;
    font-weight: bold;
    color: white;
    margin-left: 5px;
    background-color: #3c8dbc;
}
.easyPaginateNav{
    width: auto!important;
    text-align: left;
    clear: both;
}
.easyPaginateNav a.current {
    font-weight:bold;
    text-decoration:underline;}

.itemThumbnail{
    height:165px;
    margin-bottom:0px;
}
.itemFooter{
    padding: 5px;
    border: solid 1px #e6e6e6;
    border-top: none;
    height: 40px;
}
.itemInfo{
    border: solid 1px #e6e6e6;
    border-top: none;
    padding-left: 10%;
    height: 278px;
}


.thumbnail
{
    /*margin-bottom: 20px;*/
    padding: 0px;
    -webkit-border-radius: 0px;
    -moz-border-radius: 0px;
    border-radius: 0px;
}
.item.list-group-item
{
    float: none;
    width: 100%;
    background-color: #fff;
    margin-bottom: 10px;
}
.item.list-group-item:nth-of-type(odd):hover,.item.list-group-item:hover
{
    /*background: #428bca;*/
    background: #e2effb;
}
.item.list-group-item .list-group-image
{

}
.item.list-group-item .thumbnail
{
    margin-bottom: 0px;
}
.item.list-group-item .caption
{
    padding: 9px 9px 0px 9px;
}
.item.list-group-item:nth-of-type(odd)
{
    background: #eeeeee;
}

.item.list-group-item:before, .item.list-group-item:after
{
    display: table;
    content: " ";
}

.item.list-group-item img
{
    float: left;
}
.item.list-group-item:after
{
    clear: both;
}
.list-group-item-text
{
    margin: 0 0 11px;
}
.easyPaginateNav {
    max-width:100%;
    text-align:left !important;
}

.nav-pills>li.active>a, .nav-pills>li.active>a:hover, .nav-pills>li.active>a:focus {
    color: black;
}

</style>

<script type="text/javascript">

// Validations
$(function () {
    $("#personform").validate({
        ignore: "",
        rules: {
            person_fname: {
                required: true
            },
            person_lname: {
                required: true,
            },
            person_work_hour: {
                required: true,
                number: true
            },
            person_rate: {
                required: true,
                number: true
            },
            person_subsidi: {
                number: true
            },
            person_commission: {
                number: true
            },
            person_other: {
                number: true
            },
            person_pension: {
               number: true
            },
            person_medicare: {
               number: true
            },
            person_retire: {
               number: true
            },
            person_tax: {
               number: true
            },
            person_union: {
               number: true
            },
            person_sick: {
               number: true
            },
            person_fringe: {
               number: true
            },
            person_deductions: {
               number: true
            },
            person_superannuation: {
               number: true
            },
            person_workcover: {
               number: true
            }


        }
    });
});

var save_method; //for save method string
var table;

$(document).ready(function () {
    drawCollectionView();

    $('#list').click(function (event) {
        event.preventDefault();
        $('#personal .item').removeClass('grid-group-item');
        $('#personal .item').addClass('list-group-item');
    });
    $('#grid').click(function (event) {
        event.preventDefault();
        $('#personal .item').removeClass('list-group-item');
        $('#personal .item').addClass('grid-group-item');
    });
    //datatables

    $("#btnSavePerson").click(function (e) {
        e.preventDefault();
        if ($("#personform").valid()) {
            saveperson();
        }
    });

});

function drawCollectionView()
{

    $.ajax({
        url: "<?php echo site_url('people/ajax_list') ?>/",
        type: "GET",
        dataType: "JSON",
        success: function (data)
        {
            loadcollectionview(data);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert(errorThrown);
        }
    });
}


function loadcollectionview(person_data) {

var html = "";
$(person_data.data).each(function (key, val) {
    html += '<div class="item  col-sm-12 col-md-6 col-lg-4">'
            + '<div class="thumbnail itemThumbnail">'
                + '<img style="height:95%" class="group list-group-image img-responsive" src="' + this["thumbnail"] + '" alt="" />'
            + '</div>'
            + '<div class="itemInfo caption">'
                + '<div class="row">'
                    + '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 txt-bold">'
                        + '<label style="font-weight:600;">First Name:</label>'
                    + '</div>'
                    + '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="font-weight: normal;">' + this["f_name"] + '</div>'
                + '</div>'
                + '<div class="row">'
                    + '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">'
                        + '<label style="font-weight:600;">Last Name:</label>'
                    + '</div>'
                    + '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="font-weight: normal;">' + this["l_name"] + '</div>'
                + '</div>'
                + '<div class="row">'
                    + '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">'
                        + '<label style="font-weight:600;">Hour Worked:</label>'
                    + '</div>'
                    + '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="font-weight: normal;">' + this["hour_worked"] + '</div>'
                + '</div>'
                + '<div class="row">'
                    + '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">'
                        + '<label style="font-weight:600;">Rates / Hour :</label>'
                    + '</div>'
                    + '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="font-weight: normal;">' + this["rates"] + '</div>'
                + '</div>'
                + '<div class="row">'
                    + '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">'
                        + '<label style="font-weight:600;">Subsidies :</label>'
                    + '</div>'
                    + '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="font-weight: normal;">' + this["subsidies"] + '</div>'
                + '</div>'
                + '<div class="row">'
                    + '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">'
                        + '<label style="font-weight:600;">Commission :</label>'
                    + '</div>'
                    + '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="font-weight: normal;">' + this["commission"] + '</div>'
                + '</div>'
                + '<div class="row">'
                    + '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">'
                        + '<label style="font-weight:600;">Other :</label>'
                    + '</div>'
                    + '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="font-weight: normal;">' + this["other"] + '</div>'
                + '</div>'
            + '</div>'
            + '<div class="itemFooter" style="margin-bottom:10px;" >'
                + '<a  style="margin-left: 1%;" class="btn btn-sm btn-primary pull-right" href="javascript:void(0)" title="Edit" onclick="edit_people(' + this["id"] + ')">'
                    + '<i class="glyphicon glyphicon-pencil"></i>'
                + '</a>'
                + '<a  class="btn btn-sm btn-danger pull-right" href="javascript:void(0)" title="Delete" onclick="delete_people(' + this["id"] + ')">'
                    + '<i class="glyphicon glyphicon-trash"></i>'
                + '</a>'
                + '<a style="margin-right: 1%;" class="btn btn-sm btn-primary pull-right" href="javascript:void(0)" title="Information" onclick="loaddetail(' + this["id"] + ')">'
                    + '<i class="glyphicon glyphicon-exclamation-sign"></i>'
                + '</a>'
            + '</div>'
         + '</div>';
});
$('#personal').html(html);
// code for pagination
$('#personal').easyPaginate({
    paginateElement: '.item',
    elementsPerPage: 9,
    firstButton: true,
    lastButton: true
            // effect: 'climb'
});
$('.item-desc').each(function () {
    if ($(this).text().length > 130) {
        $(this).text($(this).text().substring(0, 130) + "...");
    }
});
}


</script>
<div class="modal fade" id="personmodal_form_Detail" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="padding: 15px 15px 0 15px;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="panel-white">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-4 thumbnail itemThumbnail">
                                    <img style="height:95%" class="group list-group-image img-responsive" id="imgthumbnail" src="" alt="">
                                </div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <label class="control-label col-md-4">First Name :</label>
                                        <div class="control-label col-md-8" id="person_fname"></div>
                                    </div>
                                    <div class="row">
                                        <label class="control-label col-md-4">Last Name :</label>
                                        <div class="control-label col-md-8" id="person_lname"></div>
                                    </div>
                                    <div class="row">
                                        <label class="control-label col-md-4">Person ID :</label>
                                        <div class="control-label col-md-8" id="personid"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!--panel body -->
                </div> <!--panel white -->
                <ul class="nav nav-tabs nav-pills" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#tab1" data-toggle="tab"><i class="fa fa-user m-r-xs"></i>Hours/Pay Rate</a>
                    </li>
                    <li role="presentation">
                        <a href="#tab2" data-toggle="tab"><i class="fa fa-user m-r-xs"></i>Remuneration</a>
                    </li>
                    <li role="presentation">
                        <a href="#tab3" data-toggle="tab"><i class="fa fa-user m-r-xs"></i>Net Salary</a>
                    </li>
                    <li role="presentation">
                        <a href="#tab4" data-toggle="tab"><i class="fa fa-user m-r-xs"></i>Payroll Liability</a>
                    </li>
                </ul>
            </div> <!-- modal header -->
            <div class="modal-body form">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-white">
                            <div class="panel-body">
                                <div class="tab-content" style="margin:0">
                                    <div class="tab-pane active fade in" id="tab1">
                                        <div class="row m-b-lg">
                                            <div class="col-xs-12">
                                                <label class="control-label col-xs-8">Hour Worked</label>
                                                <div class="control-label col-xs-4" id="person_hour_work"></div>
                                                <label class="control-label col-xs-8">Rater Per Hour</label>
                                                <div class="control-label col-xs-4" id="person_rates"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade in" id="tab2">
                                        <div class="row m-b-lg">
                                            <div class="col-xs-8">
                                                <label class="control-label col-xs-8">Gross Weekly Salary</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="control-label col-xs-12" id="weekly_salary"></div>
                                            </div>
                                        </div>
                                        <div class="row m-b-lg">
                                            <div class="col-xs-8">
                                                <label class="control-label col-xs-8">Subsidies Received</label>
                                                <div class="control-label col-xs-4" id="subsidies"></div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="control-label col-xs-12" id="subsidies_recieved"></div>
                                            </div>
                                        </div>
                                        <div class="row m-b-lg">
                                            <div class="col-xs-8">
                                                <label class="control-label col-xs-8">Commission Received</label>
                                                <div class="control-label col-xs-4" id="commission"></div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="control-label col-xs-12" id="commission_recieved"></div>
                                            </div>
                                        </div>
                                        <div class="row m-b-lg">
                                            <div class="col-xs-8">
                                                <label class="control-label col-xs-8">Other Received</label>
                                                <div class="control-label col-xs-4" id="other"></div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="control-label col-xs-12" id="other_recieved"></div>
                                            </div>
                                        </div>
                                        <div class="row m-b-lg">
                                            <div class="col-xs-8">
                                                <label class="control-label col-xs-8">Total Gross Salary</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="control-label col-xs-12" id="total_gross_salary"></div>
                                            </div>
                                        </div>
                                    </div><!--end tab2 -->
                                    <div class="tab-pane fade in" id="tab3">
                                        <div class="row m-b-lg">
                                            <div class="col-xs-8">
                                                <label class="control-label col-xs-8">Total Gross Salary</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="control-label col-xs-12"><strong><p id="gross_salary"></p></strong></div>
                                            </div>
                                        </div>
                                        <div class="clearfix"><hr></div>
                                        <div class="row m-b-lg">
                                            <div class="col-xs-8">
                                                <label class="control-label pull-right">Average $</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <label class="control-label col-xs-8 ">Total</label>
                                            </div>
                                        </div>
                                        <div class="row m-b-lg">
                                            <div class="col-xs-4">
                                                <label class="control-label col-xs-12">Pension</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="control-label col-xs-12"><p id="pension" class="text-right"></p></div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="control-label col-xs-12" id="pension_d"></div>
                                            </div>
                                        </div>
                                        <div class="row m-b-lg">
                                            <div class="col-xs-4">
                                                <label class="control-label col-xs-12">Medicare Levi</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="control-label col-xs-12"><p id="medicare" class="text-right"></p></div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="control-label col-xs-12" id="medicare_d"></div>
                                            </div>
                                        </div>
                                        <div class="row m-b-lg">
                                            <div class="col-xs-5">
                                                <label class="control-label col-xs-12">Retirement Annuity</label>
                                            </div>
                                            <div class="col-xs-3">
                                                <div class="control-label col-xs-12"><p id="retirement" class="text-right"></p></div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="control-label col-xs-12" id="retirement_d"></div>
                                            </div>
                                        </div>
                                        <div class="row m-b-lg">
                                            <div class="col-xs-5">
                                                <label class="control-label col-xs-12">Income Tax (PAYG)</label>
                                            </div>
                                            <div class="col-xs-3">
                                                <div class="control-label col-xs-12"><p id="income_tax" class="text-right"></p></div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="control-label col-xs-12" id="income_tax_d"></div>
                                            </div>
                                        </div>
                                        <div class="row m-b-lg">
                                            <div class="col-xs-4">
                                                <label class="control-label col-xs-12">Union Fee</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="control-label col-xs-12"><p id="union_fee" class="text-right"></p></div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="control-label col-xs-12" id="union_fee_d"></div>
                                            </div>
                                        </div>
                                        <div class="row m-b-lg">
                                            <div class="col-xs-4">
                                                <label class="control-label col-xs-12">Sick Leave</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="control-label col-xs-12"><p id="sick_leave" class="text-right"></p></div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="control-label col-xs-12" id="sick_leave_d"></div>
                                            </div>
                                        </div>
                                        <div class="row m-b-lg">
                                            <div class="col-xs-4">
                                                <label class="control-label col-xs-12">Fringe Benefit</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="control-label col-xs-12"><p id="fringe_benefit" class="text-right"></p></div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="control-label col-xs-12" id="fringe_benefit_d"></div>
                                            </div>
                                        </div>
                                        <div class="row m-b-lg">
                                            <div class="col-xs-4">
                                                <label class="control-label col-xs-12">Other Deductions</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="control-label col-xs-12"><p id="other_deduction" class="text-right"></p></div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="control-label col-xs-12" id="other_deduction_d"></div>
                                            </div>
                                        </div>
                                        <div class="row m-b-lg">
                                            <div class="col-xs-8">
                                                <label class="control-label col-xs-8">Total Deductions</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="control-label col-xs-12"><strong><p id="total_deductions_d"></p></strong></div>
                                            </div>
                                        </div>
                                        <div class="clearfix"><hr></div>
                                        <div class="row m-b-lg">
                                            <div class="col-xs-8">
                                                <label class="control-label col-xs-8">Net Salary</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="control-label col-xs-12">
                                                <strong><p id="net_salary_d"></p></strong></div>
                                            </div>
                                        </div>
                                    </div><!--end tab3 -->
                                    <div class="tab-pane fade in" id="tab4">
                                        <div class="row m-b-lg">
                                            <div class="col-xs-8">
                                                <label class="control-label col-xs-8">Total Gross Salary</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="control-label col-xs-12"><strong><p id="gross_salary_4"></p></strong></div>
                                            </div>
                                        </div>
                                        <div class="clearfix"><hr></div>
                                        <div class="row m-b-lg">
                                            <div class="col-xs-8">
                                                <label class="control-label pull-right">Average $</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <label class="control-label col-xs-8 ">Total</label>
                                            </div>
                                        </div>
                                        <div class="row m-b-lg">
                                            <div class="col-xs-4">
                                                <label class="control-label col-xs-12">Superannuation</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="control-label col-xs-12"><p id="superannuation" class="text-right"></p></div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="control-label col-xs-12" id="superannuation_d"></div>
                                            </div>
                                        </div>
                                        <div class="row m-b-lg">
                                            <div class="col-xs-4">
                                                <label class="control-label col-xs-12">Work Cover</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="control-label col-xs-12"><p id="work_cover" class="text-right"></p></div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="control-label col-xs-12" id="work_cover_d"></div>
                                            </div>
                                        </div>
                                        <div class="row m-b-lg">
                                            <div class="col-xs-8">
                                                <label class="control-label col-xs-8">Total </label>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="control-label col-xs-12"><strong><p id="total_annuation_d"></p></strong></div>
                                            </div>
                                        </div>
                                        <div class="clearfix"><hr></div>
                                        <div class="row m-b-lg">
                                            <div class="col-xs-8">
                                                <label class="control-label col-xs-12">Total Payroll Liabilities</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="control-label col-xs-12">
                                                <strong><p id="total_payroll_d"></p></strong></div>
                                            </div>
                                        </div>
                                    </div><!--end tab4 -->
                                </div><!-- tab content -->
                            </div><!--panel body -->
                        </div><!--panel-white-->
                    </div><!--col-md-12-->
                </div><!--row -->
            </div><!-- modal body -->
        </div><!-- modal content -->
    </div><!-- modal dialog -->
</div><!-- modal form -->