        <div class="container">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <h1>
              Login
              <small>@ CBPOnline</small>
            </h1>
            <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
              <li class="active">Log in</li>
            </ol>
          </section>

          <!-- Main content -->
          <section class="content">

            <?php  
            $errors = validation_errors();
            if($errors !== ""){ ?>
            <div class="callout callout-danger">
              <?php echo $errors; ?>
            </div>

            <?php } ?>

            <?php
            $flashdata= $this->session->flashdata('response');
            if(!empty($flashdata)){
              if($flashdata['status'] == 'success'){
                ?>
                <div class="callout callout-success">
                  <?php echo $flashdata['message']; ?>
                </div>
                <?php
              }
              if($flashdata['status'] == 'failed'){
                ?>
                <div class="callout callout-danger">
                  <?php echo $flashdata['message']; ?>
                </div>
                <?php
              }
            }
            ?>

            <div class="login-box">
              <div class="login-logo">
              <a href="javascript:void(0);"><b>Login to your account</a>
              </div><!-- /.login-logo -->
              <div class="login-box-body">
                <p class="login-box-msg">Sign in to start your session</p>
                <form action="<?php echo base_url() ?>login" method="post">
                  <div class="form-group has-feedback">
                    <input type="email" name="email" id="email" class="form-control" placeholder="Email">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                  </div>
                  <div class="form-group has-feedback">
                    <input type="password" name="password" id="password" class="form-control" placeholder="Password">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                  </div>
                  <div class="row">
                    <div class="col-xs-4">
                      <div class="checkbox icheck">
                        <label>
                          <input type="checkbox" name="remember_me"> Remember Me
                        </label>
                      </div>
                    </div><!-- /.col -->
                    <div class="col-xs-4">
                      <input type="hidden" name="loginForm" value="postForm">
                      <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                    </div><!-- /.col -->
                  </div>
                </form>

                <a href="#">I forgot my password</a><br>
                <a href="<?php echo  base_url(); ?>register" class="text-center">Register a new membership</a>

              </div><!-- /.login-box-body -->
            </div><!-- /.login-box -->
          </section><!-- /.content -->
        </div><!-- /.container -->