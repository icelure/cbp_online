<?php
extract($company_detail);
extract($general_assumptions);
extract($import_duty_assumptions);
extract($australian_payroll);
if($financial_year == ""){
  $financial_year="July-Jun";
}
if($currency == "USD"){
  $currency_class = "fa-dollar";
}elseif ($currency == "EUR") {
  $currency_class = "fa-eur";
}elseif ($currency == "INR") {
  $currency_class = "fa-inr";
}else{
  $currency_class = "fa-dollar";
}
?>


<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/daterangepicker/daterangepicker-bs3.css">
<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/iCheck/all.css">
<link href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css')?>" rel="stylesheet">
<link href="<?php echo base_url('assets/datatables/css/dataTables.bootstrap.css')?>" rel="stylesheet">
<link href="<?php echo base_url('assets/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')?>" rel="stylesheet">
<link rel="stylesheet" href="<?php echo asset_url(); ?>css/model.css">
<script src="<?php echo asset_url(); ?>js/model.js"></script>
<style type="text/css">


.process-step .btn:focus{outline:none}
.process{display:table;width:100%;position:relative}
.process-row{padding-top: 5px}
.process-step button[disabled]{opacity:1 !important;filter: alpha(opacity=100) !important}
.process-row:before{top:40px;bottom:0;position:absolute;content:" ";width:100%;height:1px;background-color:#ccc;z-order:0}
.process-step{display:table-cell;text-align:center;position:relative}
.process-step p{margin-top:4px}
.btn-circle{width:65px;height:65px;text-align:center;font-size:12px;border-radius:50%}
/*.tab-content{margin: 0 10% 0 10%;}*/
.error{color:rgba(255, 0, 0, 0.62);}
.input-group{width: 100%}
    .img-circle {
        border-radius: 50%;
    }
    .input-group-addon{
        border:none;
    }
tr.selected {
    background-color: #B0BED9 !important;
}
h1,p{
color:white;
}
.bordered th, .bordered td{
padding:10px;
}
.bordered tbody tr:nth-child(odd){
background:#eee;
color:#000;
}
.bordered tbody tr:nth-child(even){
color:#fff;

}


.nav>li>a {
    padding: 7px 3px;
}


.tab-header{
  background-color:#3c8dbc;
}



</style>
<section class="content-header">

<h1>
Welcome to Complete Buisness Plans
<!-- <small>advanced tables</small> -->

</h1>
<ol class="breadcrumb">
<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
<li><a href="#">Welcome to Complete Business Plans</a></li>
<!-- <li class="active">Data tables</li> -->
</ol>
</section>



<!-- Main content -->
<section class="content">
 <div class="tab-header">
    <ul class="process-row nav nav-tabs">
      <li class="nav-item active">
        <a href="#menu0" data-toggle="tab"><p style="color:rgba(0, 0, 0, 1.0); border: 0px solid blue; padding: 1px;"><strong>Introduction</strong></p>
    </a>
      </li>
      <li class="nav-item">
        <a href="#menu1" data-toggle="tab"><p style="color:rgba(0, 0, 0, 1.0); border: 0px solid blue; padding: 1px;"><strong>Company Setup</strong></p></a>
      </li>
      <li class="nav-item">
        <a href="#menu2" data-toggle="tab"><p style="color:rgba(0,0, 0, 1.0); border: 0px solid blue; padding: 1px;"><strong>General Business Assumptions</strong></p></a>
      </li>
      <li class="nav-item">
        <a href="#menu3" data-toggle="tab"><p style="color:rgba(0, 0, 0, 1.0); border: 0px solid blue; padding: 1px;"><strong>General Import Cost Assumptions</strong></p></a>
      </li>
      <li class="nav-item">
        <a href="#menu4" data-toggle="tab"><p style="color:rgba(0, 0, 0, 1.0); border: 0px solid blue; padding: 1px;"><strong>General Payroll Assumptions</strong></p></a>
      </li>
      <li class="nav-item">
        <a href="#menu5" data-toggle="tab"><p style="color:rgba(0, 0, 0, 1.0); border: 0px solid blue;"><strong>Save Result</strong></p></a>
      </li>
    </ul>
  </div>

  <div class="tab-content clearfix">

  <div id="menu0" class="tab-pane fade active in">
    <div class="box box-warning"style="background-color: rgba(250, 250, 250, 1.00); border: 0px solid blue; padding: 0px;">


    <div class="box-header with-border">
      <ul class="list-unstyled list-inline pull-right">
        <li>
          <a class="btn btn-primary view-pdf pull-right" href="<?php echo asset_url(); ?>HTML/ExpensesHelp.html">Help</a>
        </li>
      </ul>

      <h3 class="box-title"style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-size:22px;font-weight:thin;"><strong>Online Business Planning</strong></h3>
      <ul class="list-unstyled list-inline pull-right">

      </ul>

      <h2 style="color:#3c8dbc;font-size:22px;font-weight:bold;">Planning is the key to your success !</h2>
    </div><!-- /.box-header -->

    <div class="box-body">

      <h2 style="color:#3c8dbc;font-size:22px;font-weight:bold;">About CBP</h2>
      <p style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-weight:light;">CBP is dynamic application that will help you create a simple but effective Business Plan.</p>
      <h2 style="color:#3c8dbc;font-size:22px;font-weight:bold;">What is a Business Plan ?</h2>
      <p style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-weight:light;">A Business Plan is a set of strategic documents that you will design to outline your plans for a start-up or current business.These documents are used to describe your business objectives,goals and strategies,as well as to provide a blueprint of your financing and marketing plans. Essentially,it provides information about what steps your will be undertaking to achieve your goals and objectives.</p>
      <h2 style="color:#3c8dbc;font-size:22px;font-weight:bold;">Why create a Business Plan ?</h2>
      <p style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-weight:light;">It has been said by many experts and financial advisors that a Business Plan is essential to the success of a business.Your business plan should provide a detailed description of your business,including the products or services you plan to provide or sell,as well as expected monthly expenditures and profits..</p>
      <P style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-weight:light;">If you are selling products,then you'll need to include information about the manufacture of those products.A detailed description of the market for your products or services, along with competition comparisons,should be provided as well.You'll also need to include your plans for development,distribution,staffing,funding,and more.</p>
      <h2 style="color:#3c8dbc;font-size:22px;font-weight:bold;">Essential points to include in your Business Plan</h2>
      <p style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-weight:light;">When preparing your Business Plan,the key points is to focus on researching every aspect of your business.You'll need to include detailed information about everything from the basics of your product or service to how you intend to manage daily operations.If you need financing, you'll need to make your plan both informative and captivating.By doing so, you'll be able to keep loan officers or investors interested long enough to read the bulk of your plan and hopefully they will decide to provide the financing you need based on your detailed research and assumptions.</p>
      <p style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-weight:light;">A sound a realistic Business Plan is also your company narrative,and it represents the time and effort you have spent learning and understanding the nature of your business, and how you intend to operate and grow your business to success..</p>
      <h3 class="box-title"style="color:#3c8dbc;font-size:22px;font-weight:bold;">What's next </strong></h3>
      <p style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-weight:light;">In this moduel,you will start by setting up your comapany details,general business,payroll,and imports  assumptions,click on the 'Next' button to continue,please note that all values must be filled in for you to move to the next view</p>

    </div><!-- /.box-body -->

    <div class="box-footer">
      <ul class="list-unstyled list-inline pull-right">
        <li>
          <button type="button" class="btn btn-info next-step">Next <i class="fa fa-chevron-right"></i></button>
      </ul>
    </div>
  </div>
  </div><!-- End #menu0 -->

  <div id="menu1" class="tab-pane fade in">
    <div class="box box-warning"style="background-color: rgba(250, 250, 250, 1.00); border: 0px solid blue; padding: 0px;">

    <div class="box-header with-border">
      <ul class="list-unstyled list-inline pull-right">
          <li>
            <a class="btn btn-primary view-pdf pull-right" href="<?php echo asset_url(); ?>HTML/ExpensesHelp.html">Help</a>
          </li>
      </ul>
      <h3 class="box-title"style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-size:22px;font-weight:thin;"><strong>Online Business Planning</strong></h3>
      <ul class="list-unstyled list-inline pull-right">

      </ul>
      <h2 style="color:#3c8dbc;font-size:22px;font-weight:bold;">Planning is the key to your success !</h2>
    </div><!-- /.box-header -->

    <div class="box-body">
      <form role="form" method="post" action="<?php echo base_url() ?>company_setup" id="company_setting_form" enctype="multipart/form-data">
      <!-- text input -->
      <div class="row">

        <div class="form-group col-md-6">
          <label>Start Date</label>
          <input type="text" class="form-control" id="start_date" name="start_date" value="<?php print date("m/d/Y", strtotime($start_date)) ?>" placeholder="Enter start date">
        </div>
        <div class="form-group col-md-12">
          <label>Company Name</label>
          <input type="text" class="form-control" name="company_name" id="company_name" value="<?php print $company_name; ?>" placeholder="Enter your company name">
        </div>
        <div class="form-group col-md-6">
          <label>ABN No</label>
          <input type="text" class="form-control" name="abn_no" id="abn_no" value="<?php print $abn_no; ?>" placeholder="Enter ABN no">
        </div>
      </div>
    </div>

    <div class="box-header with-border">
      <h3 class="box-title"style="color:#3c8dbc;font-size:25px;font-weight:bold;">Currency Structure/Financial system </h3>
    </div><!-- /.box-header -->

    <div class="box-body">
      <div class="row">
        <div class="form-group col-md-6">
          <label>Currency</label>
          <select class="form-control" name="currency" id="currency">
            <option <?php ($currency == 'AUD') ? print "selected" : print "" ?>>AUD</option>
            <option <?php ($currency == 'USD') ? print "selected" : print "" ?>>USD</option>
            <option <?php ($currency == 'INR') ? print "selected" : print ""; ?>>INR</option>
            <option <?php ($currency == 'EUR') ? print "selected" : print "" ?>>EUR</option>
          </select>
        </div>
        <div class="form-group col-md-6">
          <label>Financial Year</label>
          <input type="text" name="financial_year" id="financial_year" value="<?php print $financial_year; ?>" class="form-control" placeholder="Enter director name" readonly>
        </div>
      </div>
    </div><!-- /.box-body -->

    <div class="box-header with-border">
      <h3 class="box-title"style="color:#3c8dbc;font-size:22px;font-weight:bold;">Company Info</h3>
    </div><!-- /.box-header -->

    <div class="box-body">
      <div class="row">
        <div class="form-group col-md-8">
        <label>Company Logo</label>
        <input type="file" name="company_logo" id="company_logo" class="form-control">
      </div>
      <?php
        if($company_logo !== ""){
          $src = base_url().'assets/uploads/company_logo/'.$company_logo;
        }else{
          $src = base_url().'assets/img/cbpLogoSmall_3x.png';
        }
      ?>
      <div class="form-group col-md-4">
        <img class="img-circle" src="<?php print $src;  ?>" width="90" height="90">
      </div>
      <div class="form-group col-md-4">
        <label>Street No</label>
        <input type="text" name="street_no" id="street_no" class="form-control" value="<?php print $street_no; ?>"  placeholder="Enter street no">
      </div>
      <div class="form-group col-md-4">
        <label>Street Name</label>
        <input type="text" name="street_name" id="street_name" class="form-control" value="<?php print $street_name; ?>" placeholder="Enter street name">
      </div>
      <div class="form-group col-md-4">
        <label>Suburb</label>
        <input type="text" name="suburb" id="suburb" class="form-control" value="<?php print $suburb; ?>" placeholder="Enter suburb">
      </div>
      <div class="form-group col-md-4">
        <label>State/Province</label>
        <input type="text" name="state" id="state" class="form-control" value="<?php print $state; ?>" placeholder="Enter state/province">
      </div>
      <div class="form-group col-md-4">
        <label>Post Code / Zip Code</label>
        <input type="text" name="zipcode" id="zipcode" class="form-control" value="<?php print $zipcode; ?>" placeholder="Enter post code/zip code">
      </div>
      <div class="form-group col-md-4">
        <label>Country</label>
        <input type="text" name="country" id="country" class="form-control" value="<?php print $country; ?>" placeholder="Enter country">
      </div>
      <!-- <div class="form-group col-md-6">
        <label>Telephone</label>
        <input type="text" name="telephone" class="form-control" data-inputmask='"mask": "(999) 999-9999"' data-mask>
      </div> -->
      <div class="form-group col-md-6">
        <label>Telephone</label>
        <div class="input-group">
          <div class="input-group-addon">
            <i class="fa fa-phone"></i>
          </div>
          <input type="text" name="telephone" id="telephone" class="form-control" value="<?php print $telephone; ?>"  data-inputmask='"mask": "(999) 999-9999"' data-mask>
        </div><!-- /.input group -->
      </div>
      <div class="form-group col-md-6">
        <label>Fax</label>
        <div class="input-group">
          <div class="input-group-addon">
              <i class="fa fa-fax"></i>
          </div>
          <input type="text" name="fax" id="fax" class="form-control" value="<?php print $fax; ?>" placeholder="Enter Fax">
        </div>
      </div>
      <div class="form-group col-md-6">
        <label>Email</label>
        <input type="text" name="company_email" id="company_email" class="form-control" value="<?php print $company_email; ?>" placeholder="Enter company email">
      </div>
      <div class="form-group col-md-6">
        <label>Website</label>
        <input type="text" name="website" id="website" class="form-control" value="<?php print $website; ?>" placeholder="Enter company website">
      </div>
    </div>
  </div>

  <div class="box-header with-border">
    <h3 class="box-title"style="color:#3c8dbc;font-size:25px;font-weight:bold;">Directors</h3>
  </div><!-- /.box-header -->

  <div class="box-body">
  <?php if(!empty($directors)) {
    foreach ($directors as $key => $value) { ?>
      <div class="form-group col-md-6">
        <label>Director Name</label>
        <input type="text" name="director_name[]" value="<?php print $value['name'] ?>" class="form-control" placeholder="Enter director name">
      </div>

      <div class="form-group col-md-4">
        <label>Director Logo</label>
        <input type="file" name="director_logo[]" class="form-control">
      </div>
      <?php
      if($value['image'] !== ""){
            $src = base_url().'assets/uploads/company_logo/'.$value['image'];
          }else{
            $src = base_url().'assets/img/cbpLogoSmall_3x.png';
          }
       ?>
      <div class="form-group col-md-2">
        <img class="img-circle" src="<?php print $src ?>" width="80" height="80">
      </div>
   <?php } ?>


  <?php } else { ?>
    <div class="row">
      <div class="form-group col-md-6">
        <label>Director Name</label>
        <input type="text" name="director_name[]" class="form-control" placeholder="Enter director name">
      </div>

      <div class="form-group col-md-6">
        <label>Director Logo</label>
        <input type="file" name="director_logo[]" class="form-control">
      </div>

      <div class="form-group col-md-6">
        <label>Director Name</label>
        <input type="text" name="director_name[]" class="form-control" placeholder="Enter director name">
      </div>

      <div class="form-group col-md-6">
        <label>Director Logo</label>
        <input type="file" name="director_logo[]" class="form-control">
      </div>

      <div class="form-group col-md-6">
        <label>Director Name</label>
        <input type="text" name="director_name[]" class="form-control" placeholder="Enter director name">
      </div>

      <div class="form-group col-md-6">
        <label>Director Logo</label>
        <input type="file" name="director_logo[]" class="form-control">
      </div>

      <div class="form-group col-md-6">
        <label>Director Name</label>
        <input type="text" name="director_name[]" class="form-control" placeholder="Enter director name">
      </div>

      <div class="form-group col-md-6">
        <label>Director Logo</label>
        <input type="file" name="director_logo[]" class="form-control">
      </div>
    </div>
    <?php } ?>
  </div><!-- /.box-body -->

  <div class="box-body">
    <div class="row">
      <div class="form-group col-md-6">
        </select>
      </div>
    </div>
  </div><!-- /.box-body -->

  <div class="box-footer">
    <ul class="list-unstyled list-inline pull-right">
      <li>
        <button type="button" class="btn btn-default prev-step"><i class="fa fa-chevron-left"></i> Back</button>
      </li>
      <li>
        <button type="button" class="btn btn-info next-step" id="comapny_detail_section">Next <i class="fa fa-chevron-right"></i></button
      </li>
    </ul>
  </div>
  </div>
  </div> <!-- End #menu1 -->

  <div id="menu2" class="tab-pane fade in">
    <div class="box box-warning"style="background-color: rgba(250, 250, 250, 1.0); border: 0px solid blue; padding: 0px;">
      <div class="box-header with-border">
        <ul class="list-unstyled list-inline pull-right">
          <li>
            <a class="btn btn-primary view-pdf pull-right" href="<?php echo asset_url(); ?>HTML/ExpensesHelp.html">Help</a>
          </li>
        </ul>
        <h3 class="box-title"style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-size:22px;font-weight:thin;"><strong>Online Business Planning</strong></h3>
        <ul class="list-unstyled list-inline pull-right">

        </ul>
        <h2 style="color:#3c8dbc;font-size:22px;font-weight:bold;">Planning is the key to your success !</h2>
      </div><!-- /.box-header -->
      <div class="box-body">
        <!-- text input -->
        <div class="row">
          <div class="form-group col-md-6">
            <label>Company Tax</label>
            <input type="text" class="form-control" name="company_tax" id="company_tax" value="<?php print $company_tax; ?>" placeholder="Enter Tax">
          </div>
          <div class="form-group col-md-6">
            <label>GST/VAT</label>
            <input type="text" class="form-control" name="company_vat" id="company_vat" value="<?php print $company_vat; ?>" placeholder="GST/VAT">
          </div>
        </div>
      </div>

      <div class="box-header with-border">
        <h3 class="box-title"style="color:#3c8dbc;font-size:25px;font-weight:bold;">General Trading & Terms</h3>
      </div><!-- /.box-header -->

      <div class="box-body">
        <!-- textarea -->
        <div class="row">
          <div class="form-group col-md-6">
            <label>Receivables In Days</label>
            <select class="form-control" name="rsvbl_in_days">
              <option value="0" <?php ($rsvbl_in_days == "0") ? print "selected"  : print "" ?>>0</option>
              <option value="30" <?php ($rsvbl_in_days == "30") ? print "selected" :  print "" ?>>30</option>
              <option value="60" <?php ($rsvbl_in_days == "60") ? print "selected" :  print "" ?>>60</option>
              <option value="90" <?php ($rsvbl_in_days == "90") ? print "selected" :  print "" ?>>90</option>
              <option value="120" <?php ($rsvbl_in_days == "120") ? print "selected" :  print "" ?>>120</option>
            </select>
          </div>

          <div class="form-group col-md-6">
            <label>Payables In Days</label>
            <select class="form-control" name="pybl_in_days">
             <option value="0" <?php ($pybl_in_days == "0") ? print "selected" :  print "" ?>>0</option>
              <option value="30" <?php ($pybl_in_days == "30") ? print "selected" :  print "" ?>>30</option>
              <option value="60" <?php ($pybl_in_days == "60") ? print "selected" :  print "" ?>>60</option>
              <option value="90" <?php ($pybl_in_days == "90") ? print "selected" :  print "" ?>>90</option>
              <option value="120" <?php ($pybl_in_days == "120") ? print "selected" :  print "" ?>>120</option>
            </select>
          </div>

          <div class="form-group col-md-6">
            <label>GST/VAT Payable Paid in Days</label>
            <select class="form-control" name="vat_paid_in_days">
              <option value="0" <?php ($vat_paid_in_days == "0") ? print "selected" :  print "" ?>>0</option>
              <option value="30" <?php ($vat_paid_in_days == "30") ? print "selected" :  print "" ?>>30</option>
              <option value="60" <?php ($vat_paid_in_days == "60") ? print "selected" :  print "" ?>>60</option>
              <option value="90" <?php ($vat_paid_in_days == "90") ? print "selected" :  print "" ?>>90</option>
              <option value="120" <?php ($vat_paid_in_days == "120") ? print "selected" :  print "" ?>>120</option>
            </select>
          </div>

          <div class="form-group col-md-6">
            <label>Company Tax Paid In Days</label>
            <select class="form-control" name="cmpnytx_paid_in_days">
              <option value="0" <?php ($cmpnytx_paid_in_days == "0") ? print "selected" :  print "" ?>>0</option>
              <option value="30" <?php ($cmpnytx_paid_in_days == "30") ? print "selected" :  print "" ?>>30</option>
              <option value="60" <?php ($cmpnytx_paid_in_days == "60") ? print "selected" :  print "" ?>>60</option>
              <option value="90" <?php ($cmpnytx_paid_in_days == "90") ? print "selected" :  print "" ?>>90</option>
              <option value="120" <?php ($cmpnytx_paid_in_days == "120") ? print "selected" :  print "" ?>>120</option>
            </select>
          </div>
        </div>
      </div><!-- /.box-body -->

      <div class="box-header with-border">
        <h3 class="box-title"style="color:#3c8dbc;font-size:25px;font-weight:bold;">Sales/Services Income Increase Each Year </h3>
      </div><!-- /.box-header -->

      <div class="box-body">
        <!-- textarea -->
        <div class="row">
          <div class="form-group col-md-6">
            <label>Sales Income Increase Each Year By</label>
            <input type="text" class="form-control" name="sales_income_increase" id="sales_income_increase" value="<?php print $sales_income_increase; ?>"  placeholder="Enter Sales Income Increase">
          </div>
          <div class="form-group col-md-6">
            <label>Services Income Each year By</label>
            <input type="text" class="form-control" name="services_income" id="services_income" value="<?php print $services_income; ?>" placeholder="Enter Services Income">
          </div>
        </div>
      </div><!-- /.box-body -->

      <div class="box-header with-border">
        <h3 class="box-title"style="color:#3c8dbc;font-size:25px;font-weight:bold;">Sales/Services Cost Increase Each Year </h3>
      </div><!-- /.box-header -->

      <div class="box-body">
        <!-- textarea -->
        <div class="row">
          <div class="form-group col-md-6">
            <label>Sales Cost Increase Each Year By</label>
            <input type="text" class="form-control" name="sales_cost_increase" id="sales_cost_increase" value="<?php print $sales_cost_increase; ?>" placeholder="Enter Sales Cost Increase">
          </div>
          <div class="form-group col-md-6">
            <label>Service Cost  Increase Each year By</label>
            <input type="text" class="form-control" name="service_cost_increase" id="service_cost_increase" value="<?php print $service_cost_increase; ?>" placeholder="Enter Service Cost Increase">
          </div>
        </div>
      </div><!-- /.box-body -->

      <div class="box-header with-border">
        <h3 class="box-title"style="color:#3c8dbc;font-size:25px;font-weight:bold;">General Operating Expenses Increase Each Year</h3>
      </div><!-- /.box-header -->

      <div class="box-body">
        <!-- textarea -->
        <div class="row">
          <div class="form-group col-md-6">
            <label>Marketing Increase Each Year By</label>
            <input type="text" class="form-control" name="marketing_increase" id="marketing_increase" value="<?php print $marketing_increase; ?>"  placeholder="Enter Marketing Increase">
          </div>
          <div class="form-group col-md-6">
            <label>Public Relations Increase Each Year By</label>
            <input type="text" class="form-control" name="public_reactions" id="public_reactions" value="<?php print $public_reactions; ?>"  placeholder="Enter Public Relations Increase">
          </div>
          <div class="form-group col-md-6">
            <label>Administrations Cost Increase Each Year By</label>
            <input type="text" class="form-control" name="administration_cost" id="administration_cost" value="<?php print $administration_cost; ?>" placeholder="Enter Administrations Cost Increase">
          </div>
          <div class="form-group col-md-6">
            <label>Other Increases Each Year By</label>
            <input type="text" class="form-control" name="other_increse" id="other_increse" value="<?php print $other_increse; ?>" placeholder="Enter Other Increases Each Year By">
          </div>
          <div class="form-group col-md-6">
            <label>Depreciation On Equipment</label>
            <input type="text" class="form-control" name="depreciation_on_equipment" id="depreciation_on_equipment" value="<?php print $depreciation_on_equipment; ?>" placeholder="Enter Depreciation On Equipment">
          </div>
        </div>
      </div><!-- /.box-body -->

      <div class="box-footer">
        <ul class="list-unstyled list-inline pull-right">
         <li>
          <button type="button" class="btn btn-default prev-step"><i class="fa fa-chevron-left"></i> Back</button>
         </li>
         <li>
          <button type="button" class="btn btn-info next-step" id="general_assumption_section">Next <i class="fa fa-chevron-right"></i></button>
         </li>
       </ul>
      </div>
    </div>
  </div> <!-- End #menu2 -->

  <div id="menu3" class="tab-pane fade in">
    <div class="box box-warning"style="background-color: rgba(250, 250, 250, 1.0); border: 0px solid blue; padding: 0px;">

      <div class="box-header with-border">
        <ul class="list-unstyled list-inline pull-right">
          <li>
            <a class="btn btn-primary view-pdf pull-right" href="<?php echo asset_url(); ?>HTML/ExpensesHelp.html">Help</a></li>
        </ul>
        <h3 class="box-title"style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-size:22px;font-weight:thin;"><strong>Online Business Planning</strong></h3>
        <ul class="list-unstyled list-inline pull-right">

        </ul>
        <h2 style="color:#3c8dbc;font-size:22px;font-weight:bold;">Planning is the key to your success !</h2>
        </div><!-- /.box-header -->

        <div class="box-body">
          <div class="row">
            <div class="form-group col-md-6">
              <div class="pull-right">
                <label>
                  <input type="radio" name="r1_imd" value="f" class="minimal" <?php ($r1_imd=='f') ? print "checked" : print "" ?>>
                  Fixed Price
                </label>
                <label>
                  <input type="radio" name="r1_imd" value="p" class="minimal" <?php ($r1_imd=='p' || $r1_imd==NULL) ? print "checked" : print "" ?>>
                  Percentage
                </label>
              </div>
              <label>Import Duty</label>
              <div class="input-group">
                <input type="text" class="form-control" name="import_duty" id="import_duty" value="<?php print $import_duty; ?>" placeholder="Enter Import Duty">
                <div class="input-group-addon">
                  <i data-rd-id="r1_imd" class="fa curr <?php ($r1_imd=='f') ? print $currency_class : print "fa-percent" ?>"></i>
              </div>
            </div>
          </div>

          <div class="form-group col-md-6">
            <div class="pull-right">
              <label>
                <input type="radio" name="r1_do" value="f" class="minimal" <?php ($r1_do=='f') ? print "checked" : print "" ?>>
                Fixed Price
              </label>
              <label>
                <input type="radio" name="r1_do" value="p" class="minimal" <?php ($r1_do=='p' || $r1_do==NULL) ? print "checked" : print "" ?> >
                Percentage
              </label>
            </div>

            <label>Delivery Order </label>
            <div class="input-group">
              <input type="text" class="form-control" name="delivery_order" id="delivery_order" value="<?php print $delivery_order; ?>"  placeholder="Enter Delivery Order">
              <div class="input-group-addon">
                <i data-rd-id="r1_do" class="fa curr <?php ($r1_do=='f') ? print $currency_class : print "fa-percent" ?>"></i>
              </div>
            </div>
          </div>

          <div class="form-group col-md-6">
            <div class="pull-right">
              <label>
                <input type="radio" name="r1_cmcf" value="f" class="minimal" <?php ($r1_cmcf=='f') ? print "checked" : print "" ?>>
                Fixed Price
              </label>
              <label>
                <input type="radio" name="r1_cmcf" value="p" class="minimal" <?php ($r1_cmcf=='p' || $r1_cmcf==NULL) ? print "checked" : print "" ?>>
                Percentage
              </label>
            </div>
            <label>CMR Comp Fee</label>
            <div class="input-group">
              <input type="text" class="form-control" name="cmr_comp_fee" id="cmr_comp_fee" value="<?php print $cmr_comp_fee; ?>" placeholder="Enter CMR Comp Fee">
              <div class="input-group-addon">
                <i data-rd-id="r1_cmcf" class="fa curr <?php ($r1_cmcf=='f') ? print $currency_class : print "fa-percent" ?>"></i>
              </div>
            </div>
          </div>

          <div class="form-group col-md-6">
            <div class="pull-right">
              <label>
                <input type="radio" name="r1_ltf" value="f" class="minimal" <?php ($r1_ltf=='f') ? print "checked" : print "" ?>>
                Fixed Price
              </label>
              <label>
                <input type="radio" name="r1_ltf" value="p" class="minimal" <?php ($r1_ltf=='p'  || $r1_ltf==NULL) ? print "checked" : print "" ?>>
                Percentage
              </label>
            </div>
            <label>LCL Transport Fee</label>
            <div class="input-group">
              <input type="text" class="form-control" name="lcl_transport_fee" id="lcl_transport_fee" value="<?php print $lcl_transport_fee; ?>" placeholder="Enter LCL Transport Fee">
              <div class="input-group-addon">
                <i data-rd-id="r1_ltf" class="fa curr <?php ($r1_ltf=='f') ? print $currency_class : print "fa-percent" ?>"></i>
              </div>
            </div>
          </div>

          <div class="form-group col-md-6">
            <div class="pull-right">
              <label>
                <input type="radio" name="r1_caf" value="f" class="minimal" <?php ($r1_caf=='f') ? print "checked" : print "" ?>>
                Fixed Price
              </label>
              <label>
                <input type="radio" name="r1_caf" value="p" class="minimal" <?php ($r1_caf=='p' || $r1_caf==NULL) ? print "checked" : print "" ?>>
                Percentage
              </label>
            </div>
            <label>Cargo Auto Fee</label>
            <div class="input-group">
              <input type="text" class="form-control" name="cargo_auto_fee" id="cargo_auto_fee" value="<?php print $cargo_auto_fee; ?>"  placeholder="Enter Cargo Auto Fee">
              <div class="input-group-addon">
                <i data-rd-id="r1_caf" class="fa curr <?php ($r1_caf=='f') ? print $currency_class : print "fa-percent" ?>"></i>
              </div>
            </div>
          </div>

          <div class="form-group col-md-6">
            <div class="pull-right">
              <label>
                <input type="radio" name="r1_psf" value="f"  class="minimal" <?php ($r1_psf=='f') ? print "checked" : print "" ?>>
                Fixed Price
              </label>
              <label>
                <input type="radio" name="r1_psf" value="p" class="minimal" <?php ($r1_psf=='p' || $r1_psf==NULL) ? print "checked" : print "" ?>>
                Percentage
              </label>
            </div>
            <label>Port Service Fee</label>
            <div class="input-group">
              <input type="text" class="form-control" name="port_service_fee" id="port_service_fee" value="<?php print $port_service_fee; ?>" placeholder="Enter Port Service Fee">
              <div class="input-group-addon">
                <i data-rd-id="r1_psf" class="fa curr <?php ($r1_psf=='f') ? print $currency_class : print "fa-percent" ?>"></i>
              </div>
            </div>
          </div>

          <div class="form-group col-md-6">
            <div class="pull-right">
              <label>
                <input type="radio" name="r1_ccf" value="f" class="minimal"  <?php ($r1_ccf=='f') ? print "checked" : print "" ?>>
                Fixed Price
              </label>
              <label>
                <input type="radio" name="r1_ccf" value="p" class="minimal" <?php ($r1_ccf=='p' || $r1_ccf==NULL) ? print "checked" : print "" ?>>
                Percentage
              </label>
            </div>
            <label>Customs Clearance Fee</label>
            <div class="input-group">
              <input type="text" class="form-control" name="custom_clearance_fee" value="<?php print $custom_clearance_fee; ?>"  id="custom_clearance_fee" placeholder="Enter Customs Clearance Fee">
              <div class="input-group-addon">
                <i data-rd-id="r1_ccf" class="fa curr <?php ($r1_ccf=='f') ? print $currency_class : print "fa-percent" ?>"></i>
              </div>
            </div>
          </div>

          <div class="form-group col-md-6">
            <div class="pull-right">
              <label>
                <input type="radio" name="r1_tff" value="f" class="minimal" <?php ($r1_tff=='f') ? print "checked" : print "" ?>>
                Fixed Price
              </label>
              <label>
                <input type="radio" name="r1_tff" value="p" class="minimal" <?php ($r1_tff=='p' || $r1_tff==NULL) ? print "checked" : print "" ?>>
                Percentage
              </label>
            </div>
            <label>Transport Fues Fee</label>
            <div class="input-group">
              <input type="text" class="form-control" name="transport_fues_fee" value="<?php print $transport_fues_fee; ?>"  id="transport_fues_fee" placeholder="Enter Transport Fues Fee">
              <div class="input-group-addon">
                <i data-rd-id="r1_tff" class="fa curr <?php ($r1_tff=='f') ? print $currency_class : print "fa-percent" ?>"></i>
              </div>
            </div>
          </div>

          <div class="form-group col-md-6">
            <div class="pull-right">
              <label>
                <input type="radio" name="r1_aqis" value="f" class="minimal" <?php ($r1_aqis=='f') ? print "checked" : print "" ?>>
                Fixed Price
              </label>
              <label>
                <input type="radio" name="r1_aqis" value="p" class="minimal" <?php ($r1_aqis=='p' || $r1_aqis==NULL) ? print "checked" : print "" ?>>
                Percentage
              </label>
            </div>
            <label>AQIS Fee</label>
            <div class="input-group">
              <input type="text" class="form-control" name="aqis_fee" id="aqis_fee" value="<?php print $aqis_fee; ?>" placeholder="Enter AQIS Fee">
              <div class="input-group-addon">
                <i data-rd-id="r1_aqis" class="fa curr <?php ($r1_aqis=='f') ? print $currency_class : print "fa-percent" ?>"></i>
              </div>
            </div>
          </div>

          <div class="form-group col-md-6">
            <div class="pull-right">
              <label>
                <input type="radio" name="r1_if" value="f" class="minimal" <?php ($r1_if=='f') ? print "checked" : print "" ?>>
                Fixed Price
              </label>
              <label>
              <input type="radio" name="r1_if" value="p" class="minimal" <?php ($r1_if=='p' || $r1_if==NULL) ? print "checked" : print "" ?>>
                Percentage
              </label>
            </div>
            <label>Insurance Fee</label>
              <div class="input-group">
                <input type="text" class="form-control" name="insurance_fee" id="insurance_fee" value="<?php print $insurance_fee; ?>" placeholder="Enter Insurance Fee">
                <div class="input-group-addon">
                  <i data-rd-id="r1_if" class="fa curr <?php ($r1_if=='f') ? print $currency_class : print "fa-percent" ?>"></i>
                </div>
              </div>
          </div>

          <div class="form-group col-md-6">
            <div class="pull-right">
              <label>
                <input type="radio" name="r1_dpf" value="f" class="minimal" <?php ($r1_dpf=='f') ? print "checked" : print "" ?>>
                Fixed Price
              </label>
              <label>
                <input type="radio" name="r1_dpf" value="p" class="minimal" <?php ($r1_dpf=='p' || $r1_dpf==NULL) ? print "checked" : print "" ?>>
                Percentage
              </label>
            </div>
            <label>Dec Processing Fee</label>
            <div class="input-group">
              <input type="text" class="form-control" name="dec_processing_fee" id="dec_processing_fee" value="<?php print $dec_processing_fee; ?>" placeholder="Enter Dec Processing Fee">
              <div class="input-group-addon">
                <i data-rd-id="r1_dpf" class="fa curr <?php ($r1_dpf=='f') ? print $currency_class : print "fa-percent" ?>"></i>
              </div>
            </div>
          </div>

          <div class="form-group col-md-6">
            <div class="pull-right">
              <label>
                <input type="radio" name="r1_mf" value="f" class="minimal" <?php ($r1_mf=='f') ? print "checked" : print "" ?>>
                Fixed Price
              </label>
              <label>
                <input type="radio" name="r1_mf" value="p" class="minimal" <?php ($r1_mf=='p' || $r1_mf==NULL) ? print "checked" : print "" ?>>
                Percentage
              </label>
            </div>
            <label>Misc Fee</label>
            <div class="input-group">
              <input type="text" class="form-control" name="misc_fee" id="misc_fee" value="<?php print $misc_fee; ?>"  placeholder="Enter Misc Fee">
              <div class="input-group-addon">
                <i data-rd-id="r1_mf" class="fa curr <?php ($r1_mf=='f') ? print $currency_class : print "fa-percent" ?>"></i>
              </div>
            </div>
          </div>

          <div class="form-group col-md-6">
            <div class="pull-right">
              <label>
                <input type="radio" name="r1_of1" value="f" class="minimal" <?php ($r1_of1=='f') ? print "checked" : print "" ?>>
                Fixed Price
              </label>
              <label>
                <input type="radio" name="r1_of1" value="p" class="minimal" <?php ($r1_of1=='p' || $r1_of1==NULL) ? print "checked" : print "" ?>>
                Percentage
              </label>
            </div>
            <label>Other Fee 1</label>
            <div class="input-group">
              <input type="text" class="form-control" name="other_fee_1" id="other_fee_1" value="<?php print $other_fee_1; ?>" placeholder="Enter Other Fee">
              <div class="input-group-addon">
                <i data-rd-id="r1_of1" class="fa curr <?php ($r1_of1=='f') ? print $currency_class : print "fa-percent" ?>"></i>
              </div>
            </div>
          </div>

          <div class="form-group col-md-6">
            <div class="pull-right">
              <label>
                <input type="radio" name="r1_of2" value="f" class="minimal" <?php ($r1_of2=='f') ? print "checked" : print "" ?>>
                Fixed Price
              </label>
              <label>
                <input type="radio" name="r1_of2" value="p" class="minimal" <?php ($r1_of2=='p' || $r1_of2==NULL) ? print "checked" : print "" ?>>
                Percentage
              </label>
            </div>
            <label>Other Fee 2</label>
            <div class="input-group">
              <input type="text" class="form-control" name="other_fee_2" id="other_fee_2" value="<?php print $other_fee_2; ?>"  placeholder="Enter Other Fee">
              <div class="input-group-addon">
                <i data-rd-id="r1_of2"  class="fa curr <?php ($r1_of2=='f') ? print $currency_class : print "fa-percent" ?>"></i>
              </div>
            </div>
          </div>

          <div class="form-group col-md-6">
            <div class="pull-right">
              <label>
                <input type="radio" name="r1_of3" value="f" class="minimal" <?php ($r1_of3=='f') ? print "checked" : print "" ?>>
                Fixed Price
              </label>
              <label>
                <input type="radio" name="r1_of3" value="p" class="minimal" <?php ($r1_of3=='p' || $r1_of3==NULL) ? print "checked" : print "" ?>>
                Percentage
              </label>
            </div>
            <label>Other Fee 3</label>
            <div class="input-group">
              <input type="text" class="form-control" name="other_fee_3" id="other_fee_3" value="<?php print $other_fee_3; ?>"  placeholder="Enter Other Fee">
              <div class="input-group-addon">
                <i data-rd-id="r1_of3" class="fa curr <?php ($r1_of3=='f') ? print $currency_class : print "fa-percent" ?>"></i>
              </div>
            </div>
          </div>
        </div>
      </div><!-- /.box-body -->

      <div class="box-footer">
        <ul class="list-unstyled list-inline pull-right">
         <li>
          <button type="button" class="btn btn-default prev-step"><i class="fa fa-chevron-left"></i> Back</button>
         </li>
         <li>
          <button type="button" class="btn btn-info next-step" id="import_cost_duty_section">Next <i class="fa fa-chevron-right"></i></button>
         </li>
       </ul>
      </div>
    </div>
  </div><!-- End #menu3 -->

  <div id="menu4" class="tab-pane fade in" >
    <div class="box box-warning"style="background-color: rgba(250, 250, 250, 1.0); border: 0px solid blue; padding: 0px;">

            <div class="box-header with-border">
                <ul class="list-unstyled list-inline pull-right">
                    <li><a class="btn btn-primary view-pdf pull-right" href="<?php echo asset_url(); ?>HTML/ExpensesHelp.html">Help</a></li>
                    </ul>
                        <h3 class="box-title"style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-size:22px;font-weight:thin;"><strong>Online Business Planning</strong></h3>
                        <ul class="list-unstyled list-inline pull-right">
                        </ul>
                        <h2 style="color:#3c8dbc;font-size:22px;font-weight:bold;">Planning is the key to your success !</h2>
                </div><!-- /.box-header -->
            <div class="box-body">

          <div class="row">
          <div class="form-group col-md-6">

            <div class="pull-right">
              <label>
                <input type="radio" name="r1_pgt" value="f" class="minimal" <?php ($r1_pgt=='f') ? print "checked" : print "" ?>>
                Fixed Price
              </label>
              <label>
                <input type="radio" name="r1_pgt" value="p" class="minimal" <?php ($r1_pgt=='p' || $r1_pgt==NULL) ? print "checked" : print "" ?>>
                Percentage
              </label>
            </div>

            <label>PAYG Tax</label>
            <div class="input-group">

            <input type="text" class="form-control" name="payg_tax" id="payg_tax" value="<?php print $payg_tax; ?>" placeholder="Enter PAYG Tax">
            <div class="input-group-addon">
              <i data-rd-id="r1_pgt" class="fa curr <?php ($r1_pgt=='f') ? print $currency_class : print "fa-percent" ?>"></i>
            </div>
            </div>

          </div>
          <div class="form-group col-md-6">

            <div class="pull-right">
              <label>
                <input type="radio" name="r1_s" value="f" class="minimal" <?php ($r1_s=='f') ? print "checked" : print "" ?>>
                Fixed Price
              </label>
              <label>
                <input type="radio" name="r1_s" value="p" class="minimal" <?php ($r1_s=='p' || $r1_s==NULL) ? print "checked" : print "" ?>>
                Percentage
              </label>
            </div>

            <label>Superannuation</label>
            <div class="input-group">

            <input type="text" class="form-control" name="superannuation" value="<?php print $superannuation; ?>"  id="superannuation" placeholder="Enter Superannuation">
            <div class="input-group-addon">
              <i data-rd-id="r1_s" class="fa curr <?php ($r1_s=='f') ? print $currency_class : print "fa-percent" ?>"></i>
            </div>
            </div>

          </div>
          <div class="form-group col-md-6">

            <div class="pull-right">
              <label>
                <input type="radio" name="r1_wc" value="f" class="minimal" <?php ($r1_wc=='f') ? print "checked" : print "" ?>>
                Fixed Price
              </label>
              <label>
                <input type="radio" name="r1_wc" value="p" class="minimal" <?php ($r1_wc=='p' || $r1_wc==NULL) ? print "checked" : print "" ?>>
                Percentage
              </label>
            </div>

            <label>Work Cover</label>
            <div class="input-group">

            <input type="text" class="form-control" name="work_cover" id="work_cover" value="<?php print $work_cover; ?>" placeholder="Enter Work Cover">
            <div class="input-group-addon">
              <i data-rd-id="r1_wc" class="fa curr <?php ($r1_wc=='f') ? print $currency_class : print "fa-percent" ?>"></i>
            </div>
            </div>

          </div>
          <div class="form-group col-md-6">

            <div class="pull-right">
              <label>
                <input type="radio" name="r1_uf" value="f" class="minimal" <?php ($r1_uf=='f') ? print "checked" : print "" ?>>
                Fixed Price
              </label>
              <label>
                <input type="radio" name="r1_uf" value="p" class="minimal" <?php ($r1_uf=='p' || $r1_uf==NULL) ? print "checked" : print "" ?>>
                Percentage
              </label>
            </div>

            <label>Union Fee</label>
            <div class="input-group">

            <input type="text" class="form-control" name="union_fee" id="union_fee" value="<?php print $union_fee; ?>" placeholder="Enter Union Fee">
            <div class="input-group-addon">
              <i data-rd-id="r1_uf" class="fa curr <?php ($r1_uf=='f') ? print $currency_class : print "fa-percent" ?>"></i>
            </div>
            </div>

          </div>

          <div class="form-group col-md-6">

            <div class="pull-right">
              <label>
                <input type="radio" name="r1_hp" value="f" class="minimal" <?php ($r1_hp=='f') ? print "checked" : print "" ?>>
                Fixed Price
              </label>
              <label>
                <input type="radio" name="r1_hp" value="p" class="minimal" <?php ($r1_hp=='p' || $r1_hp==NULL) ? print "checked" : print "" ?>>
                Percentage
              </label>
            </div>

            <label>Holiday Pay</label>
            <div class="input-group">

            <input type="text" class="form-control" name="holiday_pay" id="holiday_pay" value="<?php print $holiday_pay; ?>" placeholder="Enter Holiday Pay">
            <div class="input-group-addon">
              <i data-rd-id="r1_hp"  class="fa curr <?php ($r1_hp=='f') ? print $currency_class : print "fa-percent" ?>"></i>
            </div>
            </div>

          </div>

          <div class="form-group col-md-6">

            <div class="pull-right">
              <label>
                <input type="radio" name="r1_sl" value="f" class="minimal" <?php ($r1_sl=='f') ? print "checked" : print "" ?>>
                Fixed Price
              </label>
              <label>
                <input type="radio" name="r1_sl" value="p" class="minimal" <?php ($r1_sl=='p' || $r1_sl==NULL) ? print "checked" : print "" ?>>
                Percentage
              </label>
            </div>

            <label>Sick Leave</label>
            <div class="input-group">

            <input type="text" class="form-control" name="sick_leave" id="sick_leave" value="<?php print $sick_leave; ?>"  placeholder="Enter Sick Leave">
            <div class="input-group-addon">
              <i data-rd-id="r1_sl" class="fa curr <?php ($r1_sl=='f') ? print $currency_class : print "fa-percent" ?>"></i>
            </div>
            </div>

          </div>
          <div class="form-group col-md-6">

            <div class="pull-right">
              <label>
                <input type="radio" name="r1_lsf" value="f" class="minimal" <?php ($r1_lsf=='f') ? print "checked" : print "" ?>>
                Fixed Price
              </label>
              <label>
                <input type="radio" name="r1_lsf" value="p" class="minimal" <?php ($r1_lsf=='p' || $r1_lsf==NULL) ? print "checked" : print "" ?>>
                Percentage
              </label>
            </div>

            <label>Long Service Fee</label>
            <div class="input-group">

            <input type="text" class="form-control" name="long_service_fee" id="long_service_fee" value="<?php print $long_service_fee; ?>" placeholder="Enter Long Service Fee">
            <div class="input-group-addon">
              <i data-rd-id="r1_lsf" class="fa curr <?php ($r1_lsf=='f') ? print $currency_class : print "fa-percent" ?>"></i>
            </div>
            </div>

          </div>
          <div class="form-group col-md-6">

            <div class="pull-right">
              <label>
                <input type="radio" name="r1_ot1" value="f" class="minimal" <?php ($r1_ot1=='f') ? print "checked" : print "" ?>>
                Fixed Price
              </label>
              <label>
                <input type="radio" name="r1_ot1" value="p" class="minimal" <?php ($r1_ot1=='p' || $r1_ot1==NULL) ? print "checked" : print "" ?>>
                Percentage
              </label>
            </div>

            <label>Other 1</label>
            <div class="input-group">

            <input type="text" class="form-control" name="other_ap1" id="other_ap1" value="<?php print $other_ap1; ?>" placeholder="Enter Other">
            <div class="input-group-addon">
              <i data-rd-id="r1_ot1"  class="fa curr <?php ($r1_ot1=='f') ? print $currency_class : print "fa-percent" ?>"></i>
            </div>
            </div>

          </div>
          <div class="form-group col-md-6">

            <div class="pull-right">
              <label>
                <input type="radio" name="r1_ot2" value="f" class="minimal" <?php ($r1_ot2=='f') ? print "checked" : print "" ?>>
                Fixed Price
              </label>
              <label>
                <input type="radio" name="r1_ot2" value="p" class="minimal" <?php ($r1_ot2=='p' || $r1_ot2==NULL) ? print "checked" : print "" ?>>
                Percentage
              </label>
            </div>

            <label>Other 2</label>
            <div class="input-group">

            <input type="text" class="form-control" name="other_ap2" id="other_ap2" value="<?php print $other_ap2; ?>"  placeholder="Enter Other">
            <div class="input-group-addon">
              <i data-rd-id="r1_ot2" class="fa curr <?php ($r1_ot2=='f') ? print $currency_class : print "fa-percent" ?>"></i>
            </div>
            </div>

          </div>
          <div class="form-group col-md-6">

            <div class="pull-right">
              <label>
                <input type="radio" name="r1_ot3" value="f" class="minimal" <?php ($r1_ot3=='f') ? print "checked" : print "" ?>>
                Fixed Price
              </label>
              <label>
                <input type="radio" name="r1_ot3" value="p" class="minimal" <?php ($r1_ot3=='p' || $r1_ot3==NULL) ? print "checked" : print "" ?>>
                Percentage
              </label>
            </div>

            <label>Other 3</label>
            <div class="input-group">

            <input type="text" class="form-control" name="other_ap3" id="other_ap3" value="<?php print $other_ap3; ?>"  placeholder="Enter Other">
            <div class="input-group-addon">
              <i data-rd-id="r1_ot3" class="fa curr <?php ($r1_ot3=='f') ? print $currency_class : print "fa-percent" ?>"></i>
            </div>
            </div>

          </div>
          <div class="form-group col-md-6">

            <div class="pull-right">
              <label>
                <input type="radio" name="r1_pri" value="f" class="minimal" <?php ($r1_pri=='f') ? print "checked" : print "" ?>>
                Fixed Price
              </label>
              <label>
                <input type="radio" name="r1_pri" value="p" class="minimal" <?php ($r1_pri=='p' || $r1_pri==NULL) ? print "checked" : print "" ?>>
                Percentage
              </label>
            </div>

            <label>Pay Rate Increase Each Year B</label>
            <div class="input-group">

            <input type="text" class="form-control" name="payrate_increase" id="payrate_increase" value="<?php print $payrate_increase; ?>"  placeholder="Enter Pay Rate Increase">
            <div class="input-group-addon">
              <i  data-rd-id="r1_pri" class="fa curr <?php ($r1_pri=='f') ? print $currency_class : print "fa-percent" ?>"></i>
            </div>
            </div>
            <input type="hidden" name="submtForm" value="cmpnySettings" >
            <input type="hidden" name="company_id" value="<?php print $id; ?>">

          </div>

          </div>

        </div><!-- /.box-body -->

        <div class="box-footer">
          <ul class="list-unstyled list-inline pull-right">
            <li>
              <button type="button" class="btn btn-default prev-step"><i class="fa fa-chevron-left"></i> Back</button>
            </li>
            <li>
              <button type="button" class="btn btn-info next-step" id="australion_payroll_section">Next <i class="fa fa-chevron-right"></i></button>
            </li>
         </ul>
       </div>
     </div>
  </div><!-- End #menu4 -->

  <div id="menu5" class="tab-pane fade in" >
    <div class="box box-solid"style="background-color: rgba(250, 250, 250, 1.0); border: 0px solid blue; padding: 0px;">
      <div class="box-header with-border">
        <ul class="list-unstyled list-inline pull-right">
          <li>
          <a class="btn btn-primary view-pdf pull-right" href="<?php echo asset_url(); ?>HTML/ExpensesHelp.html">Help</a>
          </li>
        </ul>
        <h3 class="box-title"style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-size:22px;font-weight:thin;"><strong>Online Business Planning</strong></h3>
        <ul class="list-unstyled list-inline pull-right">

        </ul>
        <h2 style="color:#3c8dbc;font-size:22px;font-weight:bold;">Planning is the key to your success !</h2>
      </div><!-- /.box-header -->

      <div class="box-body">
        <h3 class="box-title"style="color:#3c8dbc;font-size:22px;font-weight:bold;">What's next </strong></h3>
        <p style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-weight:light;">Now that you have completed setting up your comapany details , you will need go to the Startup Module to evaluate your projected start up costs,and arrange your finances to start your business venture.</p>
        <p style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-weight:light;">Click on  Done to continue.</p>
      </div><!-- /.box-body -->

      <div class="box-footer">
        <ul class="list-unstyled list-inline pull-right">

          <li>
            <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Done!</button>
          </li>
       </ul>
      </div>
    </div>
   </form>
   </div>
  </div><!-- End #menu5 -->
  </div>
</div>

</section><!-- /.content -->
<script src="<?php echo asset_url(); ?>plugins/daterangepicker/daterangepicker.js"></script>
<script src="<?php echo asset_url(); ?>plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?php echo asset_url(); ?>plugins/iCheck/icheck.min.js"></script>
<script type="text/javascript" src="<?php echo js_url(); ?>jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo js_url(); ?>additional-methods.min.js"></script>
<script type="text/javascript">

    var $tabs = $('.tabbable li');

    $('#prevtab').on('click', function() {
        $tabs.filter('.active').prev('li').find('a[data-toggle="tab"]').tab('show');
    });

    $('#nexttab').on('click', function() {
        $tabs.filter('.active').next('li').find('a[data-toggle="tab"]').tab('show');
    });

    var currency_class= "fa-dollar";
        $(function(){

        	$("#currency").change(function(event) {

              curr = $(this).val();
              if(curr == "AUD"){
                currency_class = "fa-dollar";
                $("#financial_year").val("July-Jun");

              }else if(curr == "USD"){
                currency_class = "fa-dollar";
                $("#financial_year").val("Jan-Dec");
              }else if(curr == "INR"){
                currency_class = "fa-inr";
                $("#financial_year").val("Apr-Mar");
                console.log(currency_class);
              }else if(curr == "EUR"){
                currency_class = "fa-eur";
                $("#financial_year").val("Apr-Mar");
              }
              $(".curr").removeClass("fa-dollar");
              $(".curr").removeClass("fa-inr");
              $(".curr").removeClass("fa-eur");
              $(".curr").addClass(currency_class);
            });


            $('#start_date').daterangepicker({singleDatePicker: true});
            $("[data-mask]").inputmask();
            $('.btn-circle').on('click',function(){
             $('.btn-circle.btn-info').removeClass('btn-info').addClass('btn-default');
             $(this).addClass('btn-info').removeClass('btn-default').blur();
           });



    /*        $('input[type="radio"].minimal').iCheck({
              radioClass: 'iradio_minimal-blue'
            });*/

          $('input:radio').change(function(){
            if($(this).val()=='p'){
              $('i[data-rd-id="'+$(this).attr('name')+'"]').removeClass(currency_class).addClass('fa-percent');

            }else{
                $('i[data-rd-id="'+$(this).attr('name')+'"]').removeClass('fa-percent').addClass(currency_class);
            }

          });

          $("#company_setting_form").validate({
            rules:{
              company_name : "required",
              abn_no : "required",
              start_date : "required",
              street_no : "required",
              street_name : "required",
              suburb : "required",
              state : "required",
              zipcode : "required",
              country : "required",
              fax:{
                number : true
              },
              company_email:{
                required : true,
                email : true
              },
              website:{
                required : true,
                url : true
              },
              company_tax:{
                required : true
              },
              company_vat:{
                required : true
              },
              opening_cash_balance:{
                required : true
              },
              opening_debtors_balance:{
                required : true
              },
              closing_creditors_balance:{
                required : true
              },
              sales_income_increase:{
                required: true
              },
              services_income:{
                required: true
              },
              sales_cost_increase:{
                required: true
              },
              service_cost_increase:{
                required: true
              },
              marketing_increase:{
                required:true
              },
              public_reactions:{
                required: true
              },
              administration_cost:{
                required: true
              },
              depreciation_on_equipment:{
                required: true
              },
              import_duty:{
                required: true
              },
              delivery_order:{
                required: true
              },
              cmr_comp_fee:{
                required: true
              },
              lcl_transport_fee:{
                required: true
              },
              cargo_auto_fee:{
                required: true
              },
              port_service_fee:{
                required: true
              },
              custom_clearance_fee:{
                required: true
              },
              transport_fues_fee:{
                required: true
              },
              aqis_fee:{
                required: true
              },
              insurance_fee:{
                required: true
              },
              dec_processing_fee:{
                required: true
              },
              misc_fee:{
                required: true
              },
              payg_tax:{
                required: true
              },
              superannuation:{
                required: true
              },
              work_cover:{
                required: true
              },
              union_fee:{
                required: true
              },
              holiday_pay:{
                required: true
              },
              sick_leave:{
                required: true
              },
              long_service_fee:{
                required: true
              },
              payrate_increase:{
                required: true
              }
            }
          });

          $('.next-step, .prev-step').on('click', function (e){
            section_id = $(this).attr('id');

            if(section_id=='comapny_detail_section'){
              $("label.error").remove();
              abn_valid = $("#abn_no").valid();
              cn_valid = $("#company_name").valid();
              sn_valid = $("#street_name").val();
              sno_valid = $("#street_no").valid();
              stdate_valid = $("#start_date").valid();
              sub_valid = $("#suburb").valid();
              state_valid = $("#state").valid();
              zp_valid = $("#zipcode").valid();
              cntr_valid = $("#country").valid();
              tph_valid = $("#telephone").valid();
              fx_valid = $("#fax").valid();
              cmp_valid = $("#company_email").valid();
              wb_valid = $("#website").valid();


              if(abn_valid && sn_valid && sno_valid && stdate_valid && sub_valid && state_valid && zp_valid && cntr_valid && tph_valid && fx_valid && cmp_valid && wb_valid && cn_valid){

              }else{
                return false;
              }

            }

            if(section_id=='general_assumption_section'){
              $("label.error").remove();
              ctx_valid = $("#company_tax").valid();
              cvt_valid = $("#company_vat").valid();
              ocb_valid = $("#opening_cash_balance").valid();
              odb_valid = $("#opening_debtors_balance").valid();
              ccb_valid = $("#closing_creditors_balance").valid();
              sii_valid = $("#sales_income_increase").valid();
              sri_valid = $("#services_income").valid();
              slc_valid = $("#sales_cost_increase").valid();
              sci_valid = $("#service_cost_increase").valid();
              mi_valid = $("#marketing_increase").valid();
              pr_valid = $("#public_reactions").valid();
              ac_valid = $("#administration_cost").valid();
              doe_valid = $("#depreciation_on_equipment").valid();


              if(ctx_valid && cvt_valid && ocb_valid && odb_valid && ccb_valid && sii_valid && sri_valid && slc_valid && sci_valid && mi_valid && pr_valid && ac_valid && doe_valid){

              }else{
                console.log("asd");
                return false;
              }
            }

            if(section_id=='import_cost_duty_section'){
              $("label.error").remove();
              inputboxes = $("#menu3").find('input[type="text"]');
              var flag = 0;
              $.each(inputboxes, function(index, val) {

                  if($("#"+this.id).valid()){

                  }else{
                    console.log("asd");
                    flag = flag + 1;
                  }
              });
              if(flag >= 1){
                return false;
              }
            }

            if(section_id=='australion_payroll_section'){
              $("label.error").remove();
              inputboxes = $("#menu4").find('input[type="text"]');
              var flag = 0;
              $.each(inputboxes, function(index, val) {

                  if($("#"+this.id).valid()){

                  }else{

                    flag = flag + 1;
                  }
              });
              if(flag >= 1){
                return false;
              }
            }


           var $activeTab = $('.tab-pane.active');

           $('.btn-circle.btn-info').removeClass('btn-info').addClass('btn-default');

           if ( $(e.target).hasClass('next-step') )
           {
            var nextTab = $activeTab.next('.tab-pane').attr('id');
            $('[href="#'+ nextTab +'"]').addClass('btn-info').removeClass('btn-default');
            $('[href="#'+ nextTab +'"]').tab('show');
            }
            else
            {
              var prevTab = $activeTab.prev('.tab-pane').attr('id');
              $('[href="#'+ prevTab +'"]').addClass('btn-info').removeClass('btn-default');
              $('[href="#'+ prevTab +'"]').tab('show');
            }
          });
        });
      </script>
