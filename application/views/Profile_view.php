<div class="container-responsive">
<section class="content-header">
  <h1>Profile </h1>
    <ol class="breadcrumb">

      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

      <li class="active">User Profile</li>

    </ol>

</section>
<style type="text/css">
#error {
    display: inline-block;
    width: 30em;
    margin-right: .5em;
    padding-top: 1px;
    color: red;
}
.file-preview {
	border:none;
}
</style>
<?php //print_r($user); exit;?>
<section class="content">
<div class="table-responsive">
  <div class="col-md-12">
    <div class="box box-primary">
      <div class="box-header with-border">
          <?php
            $flashdata= $this->session->flashdata('response');
            if(!empty($flashdata)){
              if($flashdata['status'] == 'success'){
          ?>
                <div class="callout callout-success">
                  <?php echo $flashdata['message']; ?>
                </div>
          <?php
              }
              if($flashdata['status'] == 'failed'){
          ?>
              <div class="callout callout-danger">
                <?php echo $flashdata['message']; ?>
              </div>
          <?php
              }
            }
          ?>

          <p id="gStartTd">Who am I ?</p>

      </div>
      <?php echo form_open_multipart('', 'method="post"');?>
      <div class="box-body no-padding">
          <div class="col-md-6">
                <div class="form-group required">
                  <div id="kv-avatar-errors-1" class="center-block" style="width:800px;display:none">
                  <?php echo $this->session->flashdata('avatar-1');?>
                  </div>
                    <div class="kv-avatar " style="width:140px">
                      <input id="avatar-1" name="avatar-1" type="file" class="file-loading">
                      <br>
                    </div>
                    <label for="avatar">Profile photo </label><br><span style="color:red;">max (120x120) jpg, jpeg, gif and png only.</span>
                </div>
                <div class="form-group">
                  <a href="<?php echo base_url()."reset_password/".urlencode($user->email)."/".$user->token;?>" class="btnStyle btn btn-success form-control">Change Password <span class="fa fa-key" style="top:2px"></a>
                </div>
                <div class="form-group">
                <label></label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-user"></i>
                  </div>
                <input type="text" name="f_name" class="form-control" placeholder="First Name" value="<?php echo (isset($user->f_name))? $user->f_name:''?>">
                </div>
                <label id="error"><?php echo $this->session->flashdata('f_name');?></label>
                <br/>
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-user"></i>
                  </div>
                <input type="text" name="l_name" class="form-control" placeholder="Last Name" value="<?php echo (isset($user->l_name))? $user->l_name:''?>">
                </div>
                <label id="error"><?php echo $this->session->flashdata('l_name');?></label>
            </div>
            <div class="form-group">
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-envelope"></i>
                  </div>

                <input type="email" name="email" class="form-control" placeholder="Email Address" value="<?php echo (isset($user->email))? $user->email:''?>" readonly>
                </div>
                <label id="error"><?php echo $this->session->flashdata('email');?></label>
            </div>
            <div class="form-group">
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-phone"></i>
                  </div>
                  <input name="phone" class="form-control" placeholder="Business Phone" value="<?php echo (isset($user->phone))? $user->phone:''?>">
                </div>
                <label id="error"><?php echo $this->session->flashdata('phone');?></label>
            </div>
            <div class="form-group has-feedback">
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-flag"></i>
                    </div>
                  <select name="country" class="form-control" id="country" onfocus="populateDropDown()">
                    <option value="<?php echo (isset($user->country))? $user->country:''?>" selected><?php echo (isset($user->country))? $user->country:''?></option>
                  </select>
                  </div>
                  <label id="error"><?php echo $this->session->flashdata('country');?></label>
                </div>
            <div class="form-group">
                <input type="hidden" name="editForm" value="postForm">
                <button type="submit" name="btnEdit" id="btnEdit" class="btnStyle btn btn-success" style="width:200px;">
                <b>Save</b> <span class="glyphicon glyphicon-play" style="top:2px"></span>
                </button>
            </div>
            <?php echo form_close(); ?>
          </div>
        <div class="box-footer">
    </div>
    </div>
  </div>
</div>
</section><!-- /.content -->
</div><!-- /.container -->
<script type="text/javascript">


<?php if (isset($user->id)) { ?>

function restoreDefault() {
    $.ajax({
        type: 'POST',
        url: 'profile/restoreDefault/<?php echo $user->id;?>',
        success: function(){
            window.location.href = '<?php echo site_url('profile')?>';
        },
        error: function (jqXHR, textStatus, errorThrown) {

            showResultFailed(jqXHR.responseText);
            hideWaitingFail();
        }
    })
}


var btnReset = '<button type="button" class="btn btn-default" title="Restore default" ' +
        'onclick="restoreDefault()">' +
        '<i class="fa fa-trash"></i>' +
        '</button>';
<?php } ?>

$("#avatar-1").fileinput({
    overwriteInitial: true,
    maxFileSize: 500,
    showClose: false,
    showCaption: false,
    browseLabel: '',
    removeLabel: '',
    browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
    removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
    removeTitle: 'Cancel or reset changes',
    elErrorContainer: '#kv-avatar-errors-1',
    msgErrorClass: 'alert alert-block alert-danger',
    defaultPreviewContent: '<img class="img-circle" src="<?php echo ($user->photo)? asset_url().'uploads/users/'.$user->photo:asset_url().'uploads/users/no-photo.jpg'; ?>" alt="Your Avatar" style="width:130px;border:1px solid black">',
    layoutTemplates: {main2: '{preview}  {remove} {browse} '<?php if ($user->photo) { ?> +  btnReset <?php } ?>} ,
    allowedFileExtensions: ["jpg", "png", "gif"]
});
	function populateDropDown(){
		populateCountries("country");
	}
</script>