<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/datatables/dataTables.bootstrap.css">
<link rel="stylesheet" href="<?php echo css_url(); ?>bootstrap-switch.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
<section class="content-header">

<h1>
      Expenses
    <!-- <small>advanced tables</small> -->
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Expenses</a></li>
    <!-- <li class="active">Data tables</li> -->
  </ol>
</section>
</section>

<!-- Actions content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
          <div class="box-header with-border">

              <h3 class="box-title">Expense</h3>

          </div>
          <!-- <button style=" margin-left: 11px; margin-top: 17px; " class="btn btn-success" onclick="add_expenses()"><i class="glyphicon glyphicon-plus"></i></button> -->
          <button style=" margin-left: 11px; margin-top: 17px; " class="btn btn-success" onclick="add_expenses()"><i class="glyphicon glyphicon-plus"></i> Add Expense</button>
          
          <!-- <div class="box-header">
            <h3 class="box-title">Data Table With Full Features</h3>
          </div>/.box-header -->
        <div class="box-body">
          <table id="table" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>Expense ID</th>
                <th>Description</th>
                <th>Weekly Cost</th>
                <th>Monthly Cost</th>
                <th>Quarterly Cost</th>
                <th>Yearly Cost</th>
                <th colspan="2"> Actions</th>
              </tr>
            </thead>
              <tbody>
              <?php if(!empty($list)) {

                  foreach ($list as $expenses) { ?>
                      <tr>
                          <td><?php echo $expenses['id']; ?></td>
                          <td><?php echo $expenses['description']; ?></td>
                          <td>$<?php echo $expenses['weekly_cost']; ?></td>
                          <td>$<?php echo $expenses['monthly_cost']; ?></td>
                          <td>$<?php echo $expenses['quarterly_cost']; ?></td>
                          <td>$<?php echo $expenses['yearly_cost']; ?></td>
                          <td colspan="2"><a id="edit" style=" margin-top: 17px; " class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_expense(<?php echo $expenses['id']; ?>)"><i class="glyphicon glyphicon-pencil"></i></a><a id="delete" style=" margin-top: 17px; " class="btn btn-sm btn-danger" href="javascript:void(0)" title="Delete" onclick="delete_expense(<?php echo $expenses['id']; ?>)"><i class="glyphicon glyphicon-trash"></i></a></td>
                      </tr>
                  <?php }
              }
              ?>
              </tbody>
            <tfoot>
                  <th>Expense ID</th>
                  <th>Description</th>
                  <th>Weekly Cost</th>
                  <th>Monthly Cost</th>
                  <th>Quarterly Cost</th>
                  <th>Yearly Cost</th>
                  <th colspan="2">Actions</th>

              </tfoot>
            </table>
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </section><!-- /.content -->

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Expenses Form</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal">
                    <input type="hidden" value="" name="id"/>
                    <div class="form-body">

                        <div class="form-group">
                            <label class="control-label col-md-3">Descriptionnnnn</label>
                            <div class="col-md-9">
                                

                                <div class="input-group">
                                      <div class="input-group-addon">
                                          <i class="fa fa-pencil"></i>
                                      </div>
                                      <input name="description" placeholder="Expenses Description" class="form-control" type="text">
                                <span class="help-block"></span>
                                    </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Weekly Cost</label>
                            <div class="col-md-9">
                               

                                <div class="input-group">
                                      <div class="input-group-addon">
                                          <i class="fa fa-dollar"></i>
                                      </div>
                                       <input name="weekly_cost" placeholder="Weekly Cost" class="form-control" type="text">
                                <span class="help-block"></span>
                                    </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Monthly Cost</label>
                            <div class="col-md-9">
                                
                                <div class="input-group">
                                      <div class="input-group-addon">
                                          <i class="fa fa-dollar"></i>
                                      </div>
                                       <input name="monthly_cost" placeholder="Monthly Cost" class="form-control" type="text">
                                <span class="help-block"></span>

                                    </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Quarterly Cost</label>
                            <div class="col-md-9">
                               

                                <div class="input-group">
                                      <div class="input-group-addon">
                                          <i class="fa fa-dollar"></i>
                                      </div>
                                        <input name="quarterly_cost" placeholder="Quarterly Cost" class="form-control" type="text">
                                <span class="help-block"></span>

                                    </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Yearly Cost</label>
                            <div class="col-md-9">
                               

                                <div class="input-group">
                                      <div class="input-group-addon">
                                          <i class="fa fa-dollar"></i>
                                      </div>
                                         <input name="yearly_cost" placeholder="Yearly Cost" class="form-control" type="text">
                                <span class="help-block"></span>

                                    </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Purpose</label>
                            <div class="col-md-9">
                                <select name="purpose" class="form-control">
                                  <option value="Marketing">Marketing</option>
                                  <option value="Public-Relations">Public Relations</option>
                                  <option value="Administration">Administration</option>
                                  <option value="Other">Other</option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script src="<?php echo  asset_url(); ?>plugins/datatables/jquery.dataTables.min.js"></script>
  <script src="<?php echo  asset_url(); ?>plugins/datatables/dataTables.bootstrap.min.js"></script>
  <script src="<?php echo  js_url(); ?>bootstrap-switch.min.js"></script>
  <script>
    var save_method; //for save method string
    $(function () {
        //datatables
        
      table = $('#table').DataTable({



                // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo site_url('expenses/ajax_list')?>",
                "type": "POST"
            },
        });
        $('#table tbody').on( 'click', 'tr', function () {
           // $(this).find('td').eq(0).text();
            //alert( 'Row index: '+table.row( this ).data());
           // var id = table.row( this ).id();
          //  alert( $(this).find('td:first').text() );
            if ( $(this).hasClass('selected') ) {
                $(this).removeClass('selected');
            }
            else {
                table.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
            $('#edit').attr('data-info', $(this).find('td:first').text());
            $('#delete').attr('data-info', $(this).find('td:first').text());
           // $('#edit').data('sample_name',$(this).find('td:first').text());
           // $('#delete').data('sample_name',$(this).find('td:first').text());


        } );

        //$('#table').DataTable();
    });
    function reload_table()
    {
        table.ajax.reload(null,false); //reload datatable ajax
    }
    //add Expenses show model

    function add_expenses()
    {
        save_method = 'add';
        $('#form')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $('#modal_form').modal('show'); // show bootstrap modal
        $('.modal-title').text('Add Expense'); // Set Title to Bootstrap modal title
    }
    // add expenses to database through ajax call

    function save()
    {
        $('#btnSave').text('saving...'); //change button text
        $('#btnSave').attr('disabled',true); //set button disable
        var url;

        if(save_method == 'add') {
            url = "<?php echo base_url(); ?>expenses/ajax_add";
        } else {
            url = "<?php echo base_url(); ?>expenses/ajax_update";
        }

        // ajax adding data to database
        $.ajax({
            url : url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data)
            {

                if(data.status) //if success close modal and reload ajax table
                {
                    $('#modal_form').modal('hide');
                    reload_table();
                }

                $('#btnSave').text('save'); //change button text
                $('#btnSave').attr('disabled',false); //set button enable


            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
                $('#btnSave').text('save'); //change button text
                $('#btnSave').attr('disabled',false); //set button enable

            }
        });
    }

    function edit_expense(e_id)
    {
        var id=e_id;
        save_method = 'update';
        $('#form')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        //Ajax Load data from ajax
        $.ajax({
            url : "<?php echo base_url(); ?>expenses/ajax_edit/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data)
            {
                $('[name="id"]').val(data.id);
                $('[name="description"]').val(data.description);
                $('[name="weekly_cost"]').val(data.weekly_cost);
                $('[name="monthly_cost"]').val(data.monthly_cost);
                $('[name="quarterly_cost"]').val(data.quarterly_cost);
                $('[name="yearly_cost"]').val(data.yearly_cost);
                $('[name="purpose"]').val(data.purpose);
                $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
                $('.modal-title').text('Edit Expense'); // Set title to Bootstrap modal title

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error get data from ajax');
            }
        });
    }
    $('.navigateTest').click(function(){
        alert("test called");
        var serviceID = this.id;
        alert("serviceID :: " + serviceID);
    });

    function delete_expense(e_id)
    {
        if(confirm('Are you sure you wan to delete this data?'))
        {
            var id=e_id;
            // ajax delete data to database
            $.ajax({
                url : "<?php echo base_url(); ?>expenses/ajax_delete/"+id,
                type: "POST",
                dataType: "JSON",
                success: function(data)
                {
                    //if success reload ajax table
                    $('#modal_form').modal('hide');
                    reload_table();
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Error deleting data');
                }
            });

        }
    }
  </script>