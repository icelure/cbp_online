<div class="well well-sm col-sm-12 col-md-12 col-lg-12">
    <strong>Display</strong>
    <div class="btn-group">
        <a href="#" id="list" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th-list"></span>List</a>
        <a href="#" id="grid" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th"></span>Grid</a>
    </div>
     <button style="float: right;"  class="btn btn-success" onclick="add_importedproduct()"><i class="glyphicon glyphicon-plus"></i> Imported Product</button>
</div>

<div id="importedproducts" class="row-height list-group col-sm-12 col-md-12 col-lg-12">

</div>

<script src="<?php echo base_url('assets/plugins/easypaginate/easyPaginate.js') ?>"></script>
<style>
/*pagination style*/
.easyPaginateNav a {
	font-size: 18px;
    padding: 5px 10px;
    font-weight: bold;
    color: white;
    margin-left: 5px;
    background-color: #3c8dbc;
}
.easyPaginateNav{
    width: auto!important;
    text-align: left;
    clear: both;
}
.easyPaginateNav a.current {
    font-weight:bold;
    text-decoration:underline;
}

.itemThumbnail{
    height:165px;
    margin-bottom:0px;
}
.itemFooter{
    padding: 5px;
    border: solid 1px #e6e6e6;
    border-top: none;
    height: 40px;
}
.itemInfo{
    border: solid 1px #e6e6e6;
    border-top: none;
    padding-left: 10%;
    height: 278px;
}

.thumbnail
{
    /*margin-bottom: 20px;*/
    padding: 0px;
    -webkit-border-radius: 0px;
    -moz-border-radius: 0px;
    border-radius: 0px;
}
.item.list-group-item
{
    float: none;
    width: 100%;
    background-color: #fff;
    margin-bottom: 10px;
}
.item.list-group-item:nth-of-type(odd):hover,.item.list-group-item:hover
{
    /*background: #428bca;*/
    background: #e2effb;
}
.item.list-group-item .list-group-image
{

}
.item.list-group-item .thumbnail
{
    margin-bottom: 0px;
}
.item.list-group-item .caption
{
    padding: 9px 9px 0px 9px;
}
.item.list-group-item:nth-of-type(odd)
{
    background: #eeeeee;
}

.item.list-group-item:before, .item.list-group-item:after
{
    display: table;
    content: " ";
}

.item.list-group-item img
{
    float: left;
}
.item.list-group-item:after
{
    clear: both;
}
.list-group-item-text
{
    margin: 0 0 11px;
}
.easyPaginateNav {
    max-width:100%;
    text-align:left !important;
}
.nav-pills>li.active>a, .nav-pills>li.active>a:hover, .nav-pills>li.active>a:focus {
    color: black;
}
</style>
<script type="text/javascript">
// Validations
$(function () {
    $("#importform").validate({
        ignore: "",
        rules: {
            importedproduct_description: {
                required: true
            },
            imported_unit_cost: {
                required: true,
                number: true
            },
            imported_markup_on_cost: {
                required: true,
                number: true
            },
            ImportQuantity: {
                required: true,
                number: true
            },
            importedexchange_range: {
                required: true,
                number: true
            },
            import_duty:{
                required: true
            },
            comp_fee:{
                required: true
            },
            cargo_auto:{
                required: true
            },
            custom_clearance_fee:{
                required: true
            },
            aqis_fee:{
                required: true
            },
            dec_processing_fee:{
                required: true
            },
            delivery_order:{
                required: true
            },
            lcl_transport_fee:{
                required: true
            },
            port_service_fee:{
                required: true
            },
            transport_fues_fee:{
                required: true
            },
            insurance_fee:{
                required: true
            },
            misc_fee:{
                required: true
            },
            other_fee_1:{
                required: true
            },
            other_fee_2:{
                required: true
            },
            other_fee_3:{
                required: true
            },
        }
    });
});

var save_method;
var table;
$(document).ready(function () {
    drawCollectionView();

    $('#list').click(function (event) {
        event.preventDefault();
        $('#importedproducts .item').removeClass('grid-group-item');
        $('#importedproducts .item').addClass('list-group-item');
    });
    $('#grid').click(function (event) {
        event.preventDefault();
        $('#importedproducts .item').removeClass('list-group-item');
        $('#importedproducts .item').addClass('grid-group-item');
    });
    //datatables
    $("#btnSaveImport").click(function (e) {
        e.preventDefault();
        if ($("#importform").valid()) {
            saveimported();
        }
    });
});

function drawCollectionView(){

    $.ajax({
        url: "<?php echo site_url('ImportedProducts/ajax_list') ?>/",
        type: "GET",
        dataType: "JSON",
        success: function (data)
        {
            loadcollectionview(data);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert(errorThrown);
        }
    });
}
function loadcollectionview(importedproducts) {

    var html = "";
    $(importedproducts.data).each(function (key, val) {
    html += '<div class="item  col-sm-12 col-md-6 col-lg-4">'
            + '<div class="thumbnail itemThumbnail">'
            	+ '<img style="height:95%" class="group list-group-image img-responsive" src="' + this["importedproductthumbnail"] + '" alt="" />'
            + '</div>'
            + '<div class="itemInfo caption">'
            	+ '<div class="row">'
            		+ '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 txt-bold">'
            			+ '<label style="font-weight:600;">UnitCost:</label>'
            		+ '</div>'
            		+ '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="font-weight: normal;">' + this["unitcost"] + '</div>'
            	+ '</div>'
            	+ '<div class="row">'
            		+ '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">'
            			+ '<label style="font-weight:600;">Qty:</label>'
            		+ '</div>'
            		+ '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="font-weight: normal;">' + this["qty"] + '</div>'
            	+ '</div>'
            	+ '<div class="row">'
            		+ '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">'
            			+ '<label style="font-weight:600;">TotalCost:</label>'
            		+ '</div>'
            		+ '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="font-weight: normal;">' + this["totalcost"] + '</div>'
            	+ '</div>'
            	+ '<div class="row">'
            		+ '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">'
            			+ '<label style="font-weight:600;">Exchange Rate:</label>'
            		+ '</div>'
            		+ '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="font-weight: normal;">' + this["exchangerate"] + '</div>'
            	+ '</div>'
            	+ '<div class="row">'
            		+ '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">'
            			+ '<label style="font-weight:600;">Total Landed Cost:</label>'
            		+ '</div>'
            		+ '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="font-weight: normal;">' + this["totallandedcost"] + '</div>'
        		+ '</div>'
            	+ '<div>'
            		+ '<h4 class="group inner list-group-item-heading"><strong>Description:-</strong></h4>'
            		+ '<p style="color: #333" class="group inner list-group-item-text item-desc">' + this["importedproductdescription"] + '</p>'
        		+ '</div>'
        	+ '</div>'
            + '<div class="itemFooter" style="margin-bottom:10px;" >'
            	+ '<a  style="margin-left: 1%;" class="btn btn-sm btn-primary pull-right" href="javascript:void(0)" title="Edit" onclick="edit_importedproduct(' + this["importedproductid"] + ')">'
            		+ '<i class="glyphicon glyphicon-pencil"></i>'
            	+ '</a>'
            	+ '<a  class="btn btn-sm btn-danger pull-right" href="javascript:void(0)" title="Delete" onclick="delete_importedproduct(' + this["importedproductid"] + ')">'
            		+ '<i class="glyphicon glyphicon-trash"></i>'
            	+ '</a>'
            	+ '<a  style="margin-right: 1%;" class="btn btn-sm btn-primary pull-right" href="javascript:void(0)" title="Information" onclick="loaddetail(' + this["importedproductid"] + ')">'
            		+ '<i class="glyphicon glyphicon-exclamation-sign"></i>'
            	+ '</a>'
            + '</div>'
        + '</div>';
    });
    $('#importedproducts').html(html);

    $('#importedproducts').easyPaginate({
        paginateElement: '.item',
        elementsPerPage: 9,
        firstButton: true,
        lastButton: true
                // effect: 'climb'
    });
    $('.item-desc').each(function () {
        if ($(this).text().length > 130) {
            $(this).text($(this).text().substring(0, 130) + "...");
        }
    });
}
</script>
<div class="modal fade" id="impmodal_form_Detail" role="dialog">
	<div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="padding: 15px 15px 0 15px;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="panel-white">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-4 thumbnail itemThumbnail">
                                    <img style="height:95%" class="group list-group-image img-responsive" id="imgthumbnail" src="" alt="">
                                </div>
                                <label class="control-label col-md-4">Description :</label>
                                <div class="control-label col-md-8" id="impdesc"></div>
                            </div>
                        </div>
                    </div>
                </div><!--end panel white-->
                <ul class="nav nav-tabs nav-pills" role="tablist">
                    <li role="presentation" class="active"><a href="#tab1" data-toggle="tab"><i class="fa fa-user m-r-xs"></i>Basic Information</a></li>
                    <li role="presentation"><a href="#tab2" data-toggle="tab"><i class="fa fa-user m-r-xs"></i>Import Expenses</a></li>
                    <li role="presentation"><a href="#tab3" data-toggle="tab"><i class="fa fa-user m-r-xs"></i>Others</a></li>
                </ul>
            </div> <!--end modal header-->

            <div class="modal-body form">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-white">
                            <div class="panel-body">
                                <div id="rootwizard">
                                    <form id="wizardForm" novalidate="novalidate">
                                        <div class="tab-content" style="margin:0">
                                            <div class="tab-pane active fade in" id="tab1">
                                                <div class="row m-b-lg">
                                                    <div class="col-xs-6">
                                                        <label class="control-label col-xs-8">Unit Cost</label>
                                                        <div class="control-label col-xs-4" id="impUC"></div>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <label class="control-label col-xs-8">Weekly Sold Qty</label>
                                                        <div class="control-label col-xs-4" id="impSolQnt"></div>
                                                    </div>
                                                </div>
                                                <div class="row m-b-lg">
                                                    <div class="col-xs-6">
                                                        <label class="control-label col-xs-8">Total</label>
                                                        <div class="control-label col-xs-4" id="imptotal"></div>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <label class="control-label col-xs-8">Exchange Rate</label>
                                                        <div class="control-label col-xs-4" id="impExchangeRange"></div>
                                                    </div>
                                                </div>
                                                <div class="row m-b-lg">
                                                    <div class="col-xs-6">
                                                        <label class="control-label col-xs-8">Total Landed Cost</label>
                                                        <div class="control-label col-xs-4" id="impTotalLandedCost"></div>
                                                    </div>
                                                     <div class="col-xs-6">
                                                        <label class="control-label col-xs-8">Total Product Cost</label>
                                                        <div class="control-label col-xs-4" id="impTPc"></div>
                                                    </div>
                                                </div>
                                                <div class="row m-b-lg">
                                                    <div class="col-xs-6">
                                                        <label class="control-label col-xs-8">Avg. Unit Cost</label>
                                                        <div class="control-label col-xs-4" id="impunitcost"></div>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <label class="control-label col-xs-8">Mark Up On Cost</label>
                                                        <div class="control-label col-xs-4" id="impMarkup"></div>
                                                    </div>
                                                </div>
                                                <div class="row m-b-lg">
                                                    <div class="col-xs-6">
                                                        <label class="control-label col-xs-8">WholeSale Price</label>
                                                        <div class="control-label col-xs-4" id="impWp"></div>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <label class="control-label col-xs-8">Gross Profit</label>
                                                        <div class="control-label col-xs-4" id="impGP"></div>
                                                    </div>
                                                </div>
                                                <div class="row m-b-lg">
                                                    <div class="col-xs-6">
                                                        <label class="control-label col-xs-8">Total Revenue</label>
                                                        <div class="control-label col-xs-4" id="impTotalRevenue"></div>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <label class="control-label col-xs-8">Total Cost</label>
                                                        <div class="control-label col-xs-4" id="impTC"></div>
                                                    </div>
                                                </div>
                                                <div class="row m-b-lg">
                                                    <div class="col-xs-6">
                                                        <label class="control-label col-xs-8">Gross Total</label>
                                                        <div class="control-label col-xs-4" id="impgross"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="tab2">
                                                <div class="row m-b-lg">
                                                    <div class="col-xs-6">
                                                        <label class="control-label col-xs-8">Import Duty</label>
                                                        <div class="control-label col-xs-4" id="impID"></div>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <label class="control-label col-xs-8">CMR Comp Fee</label>
                                                        <div class="control-label col-xs-4" id="impCmF"></div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <label class="control-label col-xs-8">LCL Transport Fee</label>
                                                        <div class="control-label col-xs-4" id="impLTF"></div>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <label class="control-label col-xs-8">Cargo Auto Fee</label>
                                                        <div class="control-label col-xs-4" id="impca"></div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <label class="control-label col-xs-8">Port Service Fee</label>
                                                        <div class="control-label col-xs-4" id="impps"></div>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <label class="control-label col-xs-8">Customs Clearance</label>
                                                        <div class="control-label col-xs-4" id="impcc"></div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <label class="control-label col-xs-8">Transport Fee</label>
                                                        <div class="control-label col-xs-4" id="imptp"></div>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <label class="control-label col-xs-8">AQIS Fee</label>
                                                        <div class="control-label col-xs-4" id="impAFt"></div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <label class="control-label col-xs-8">Insurance Fee</label>
                                                        <div class="control-label col-xs-4" id="impIf"></div>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <label class="control-label col-xs-8">Dec Processing Fee</label>
                                                        <div class="control-label col-xs-4" id="impDp"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="tab3">
                                                <div class="row m-b-lg">
                                                    <div class="col-xs-6">
                                                        <label class="control-label col-xs-8">Delivery Order</label>
                                                        <div class="control-label col-xs-4" id="impDo"></div>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <label class="control-label col-xs-8">Misc. Fee</label>
                                                        <div class="control-label col-xs-4" id="impMf"></div>
                                                    </div>
                                                </div>
                                                <div class="row m-b-lg">
                                                    <div class="col-xs-6">
                                                        <label class="control-label col-xs-8">Other 1</label>
                                                        <div class="control-label col-xs-4" id="impo1"></div>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <label class="control-label col-xs-8">Other 2</label>
                                                        <div class="control-label col-xs-4" id="impo2"></div>
                                                    </div>
                                                </div>
                                                <div class="row m-b-lg">
                                                    <div class="col-xs-6">
                                                        <label class="control-label col-xs-8">Other 3</label>
                                                        <div class="control-label col-xs-4" id="impo3"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>