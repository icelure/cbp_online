<div class="col-md-12" style="">
    <div class="row">
        <div class="col-md-6 col-sm-12">
            <canvas id="ChartDisplay"  width="300" height="200" ></canvas>
        </div>
        <div class="col-md-6 col-sm-12 RangeSelector"> 
            <div style="padding: 15px;">
                <h4 style="text-align: center;padding: 30px 23px;display: table; margin: 0 auto;border: 1px solid gray;border-radius: 50%;width: 80px; height: 80px;background: #5bc0de; color: white; margin-top: 35px;" ><span class="ServiceIncomePers">0</span>%</h4>
                <input id="ex8" type="range" min="0" max="100" value="50" class="RangeSelectorInput" style="margin-top: 25px;"/>
                <h2 class="text-center">Sales Income Range</h2>
                <br><br>
                <div class="text-center"><input type="button" value="Save" class="btn btn-info" id="save"/></div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12" id="one_time_cost" style="">

    <div class="table-responsive">
        <table id="tablelocalproductsummary" class="table table-striped table-bordered">
            <colgroup><col width="5%"><col width="22%"><col width="23%"><col width="6%"><col width="6%"><col width="7%"><col width="10%"><col width="10%"><col width="10%"></colgroup>
            <thead>
                <tr>
                    <th>ThumbNail</th>
                    <th>Product ID</th>
                    <th>Description</th>
                    <th>Qty</th>
                    <th>Unit Cost</th>
                    <th>Total</th>
                    <th>Mark Up on Cost</th>
                    <th>R.R.P</th>
                    <th>Total Cost</th>
                    <th>Total Revenue</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot>
                <tr>
                    <th colspan="2" style="text-align: right;">Total Monthly Costs</th>
                    <th></th>
                    <th class="SumQtyTotal">0</th>
                    <th></th>
                    <th class="SumTotal">0</th>
                    <th></th>
                    <th class="SumRRP">0</th>
                    <th class="SumTotalCost">0</th>
                    <th class="SumTotalRev">0</th>
                </tr>
            </tfoot>

        </table>
    </div>
</div>

<script type="text/javascript">
    function number_format(number, decimals, decPoint, thousandsSep) {
        decimals = decimals || 0;
        number = parseFloat(number);

        if (!decPoint || !thousandsSep) {
            decPoint = '.';
            thousandsSep = ',';
        }

        var roundedNumber = Math.round(Math.abs(number) * ('1e' + decimals)) + '';
        var numbersString = decimals ? roundedNumber.slice(0, decimals * -1) : roundedNumber;
        var decimalsString = decimals ? roundedNumber.slice(decimals * -1) : '';
        var formattedNumber = "";

        while (numbersString.length > 3) {
            formattedNumber += thousandsSep + numbersString.slice(-3)
            numbersString = numbersString.slice(0, -3);
        }

        return (number < 0 ? '-' : '') + numbersString + formattedNumber + (decimalsString ? (decPoint + decimalsString) : '');
    }
    var table1;

    $(document).ready(function () {

        $('#save').click(function () {
            vi = $('.RangeSelectorInput').val();
            var datapass = 'si=' + vi;

            $.ajax({
                type: 'POST',
                data: datapass,
                url: "<?php echo site_url('Products/update_sales_income_increase'); ?>",
                datatype: 'json',
                success:
                        function (data) {
                            //console.log(data);
                            var datas = jQuery.parseJSON(data);
                            //alert(datas);
                            if (datas.status == 'yes') {
                                alert("Updated Successfully");
                                console.log("updated");
                            }
                            if (datas.status == 'no') {
                                alert("Not Updated");
                                console.log("not updated");
                            }
                        }
            });
        });

        //datatables
        table1 = $('#tablelocalproductsummary').DataTable({
            "Processing": true, //Feature control the processing indicator.
            "ServerSide": true, //Feature control DataTables' server-side processing mode.
            //"order": [], //Initial no order.

            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo site_url('Products/localproductsummary') ?>",
                "type": "GET"
            },
            "drawCallback": function (d) {
                if (d.json !== undefined)
                {
                    setTimeout(function () {
                        CalcSerVal(d.json.Service_income, d.json.currency);
                    }, 300);
                    $('.RangeSelectorInput').val(parseInt(d.json.Service_income));
                    $('.ServiceIncomePers').text(parseInt(d.json.Service_income));
                    $('.RangeSelectorInput').on("input change", function () {
                    $('#ChartDisplay').html('');
                        var vi = $(this).val();
                        //var vi = $('.ServiceIncomePers').text();
                        $('.ServiceIncomePers').text(vi);
                        //$("#ChartDisplay").find("div").remove();
                        setTimeout(function () {
                            CalcSerVal(vi, d.json.currency);
                        }, 300);
                    });
                }
            }
        });

    });
    function reload_tablelocalproductsummary()
    {
        table1.ajax.reload(null, false); //reload datatable ajax 
    }
    var myChart;
    function CalcSerVal(ser, cur) {
        $('#ChartDisplay').html('');
        var tb = $('#tablelocalproductsummary');
        ser = parseInt(ser);
        var trrp = 0, ttc = 0, ttr = 0, tq = 0, tt = 0;
        tb.find('tbody tr').each(function () {
            var rrp = parseInt($(this).find('td:eq(7)').text().replace(/,/g, '').replace(/\$|\€|\₹/g, ''));
            var tc = parseInt($(this).find('td:eq(8)').text().replace(/,/g, '').replace(/\$|\€|\₹/g, ''));
            var tr = parseInt($(this).find('td:eq(9)').text().replace(/,/g, '').replace(/\$|\€|\₹/g, ''));
            var q = parseInt($(this).find('td:eq(3)').text());
            var t = parseInt($(this).find('td:eq(5)').text().replace(/,/g, '').replace(/\$|\€|\₹/g, ''));
            /* var y2 = parseInt((y1 * (ser / 100)) + y1); contoh penggunaan service income*/ 
            trrp += rrp;
            ttc += tc;
            ttr += tr;
            tq += q;    
            tt += t;        
        });
        $('.SumQtyTotal').text(number_format(tq, 0, '.', ','))
        $('.SumTotal').text(cur + number_format(tt, 0, '.', ','))
        $('.SumRRP').text(cur + number_format(trrp, 0, '.', ','));
        $('.SumTotalCost').text(cur + number_format(ttc, 0, '.', ','));
        $('.SumTotalRev').text(cur + ttr);
        
        //ChartDisplay

        var ctx = document.getElementById("ChartDisplay");
        if (myChart != undefined) {
            myChart.destroy();
        }
        myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ["R.R.P", "Total Cost", "Total Revenue"],
                datasets: [{
                        data: [trrp, ttc, ttr],
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255,99,132,1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)'
                        ],
                        borderWidth: 1
                    }]
            },
            options: {
                legend: {
                    display: false
                },
                scales: {
                    yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                }
            }
        });
    }

</script>
