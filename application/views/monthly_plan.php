<section class="content-header">
  <h1>
    Manage Plans
    <!-- <small>advanced tables</small> -->
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Plans</a></li>
    <!-- <li class="active">Data tables</li> -->
  </ol>
</section>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title"><?php print ucfirst($plans[0]['type']); ?> Plan</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" method="post" action="<?php print base_url();?>Manage_plans">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Title</label>
                      <div class="col-sm-6">
                        <input type="text" class="form-control" id="title" name="title" value="<?php print $plans[0]['title'] ?>" placeholder="Title">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputPassword3" class="col-sm-2 control-label">Subtitle</label>
                      <div class="col-sm-6">
                        <input type="text" class="form-control" id="subtitle" name="subtitle" value="<?php print $plans[0]['subtitle'] ?>" placeholder="Subtitle">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputPassword3" class="col-sm-2 control-label">Price</label>
                      <div class="col-sm-6">
                        <input type="text" class="form-control" id="price" name="price" value="<?php print $plans[0]['price'] ?>" placeholder="Price">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputPassword3" class="col-sm-2 control-label">Feature 1</label>
                      <div class="col-sm-6">
                        <input type="text" class="form-control" id="feature1" name="feature1" value="<?php print $plans[0]['feature1'] ?>" placeholder="Feature">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputPassword3" class="col-sm-2 control-label">Feature 2</label>
                      <div class="col-sm-6">
                        <input type="text" class="form-control" id="feature2" name="feature2" value="<?php print $plans[0]['feature2'] ?>" placeholder="Feature">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputPassword3" class="col-sm-2 control-label">Feature 3</label>
                      <div class="col-sm-6">
                        <input type="text" class="form-control" id="feature3" name="feature3" value="<?php print $plans[0]['feature3'] ?>" placeholder="Feature">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputPassword3" class="col-sm-2 control-label">Feature 4</label>
                      <div class="col-sm-6">
                        <input type="text" class="form-control" id="feature4" name="feature4" value="<?php print $plans[0]['feature4'] ?>" placeholder="Feature">
                      </div>
                    </div>
                    
                  </div><!-- /.box-body -->
                  <div class="box-footer" style="margin-left:16%">
                    <a href="<?php echo base_url() ?>manage_plans" class="btn btn-default">Cancel</a>
                    <input type="hidden" name="type" value="<?php print $plans[0]['type'] ?>">
                    <input type="hidden" name="id" value="<?php print $plans[0]['id'] ?>">
                    <button type="submit" class="btn btn-info">Submit</button>
                  </div><!-- /.box-footer -->
                </form>
              </div><!-- /.box -->
    </div><!-- /.row -->
  </section><!-- /.content -->
  