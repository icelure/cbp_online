<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/daterangepicker/daterangepicker-bs3.css">
<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/iCheck/all.css">
<style type="text/css">
    .process-step .btn:focus{outline:none}
    .process{display:table;width:100%;position:relative}
    .process-row{padding-top: 5px}
    .process-step button[disabled]{opacity:1 !important;filter: alpha(opacity=100) !important}
    .process-row:before{top:40px;bottom:0;position:absolute;content:" ";width:100%;height:1px;background-color:#ccc;z-order:0}
    .process-step{display:table-cell;text-align:center;position:relative; width:25%;}
    .process-step p{margin-top:4px}
    .btn-circle{width:65px;height:65px;text-align:center;font-size:12px;border-radius:50%}
    /*.tab-content{margin: 0 10% 0 10%;}*/
    .error{color:rgba(255, 0, 0, 0.62);}
    .input-group{width: 100%}
    .img-circle {
        border-radius: 50%;
    }
    .input-group-addon{
        border:none;
    }
    tr.selected {
        background-color: #B0BED9 !important;
    }
    .fa {
            display: inline-block;
            font-family: FontAwesome;
            font-style: normal;
            font-weight: normal;
            line-height: 1;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
    }

    .nav>li>a {
    padding: 7px 3px;
}


.tab-header{
  background-color:#3c8dbc;
}

</style>
<section class="content-header">
    <h1>Local Products </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Local Products</a></li>
    </ol>
</section>
<section class="content">

<div class="tab-header">
    <ul class="process-row nav nav-tabs">
      <li class="nav-item active">
        <a href="#menu0" data-toggle="tab"><p style="color:rgba(0, 0, 0, 1.0); border: 0px solid blue; padding: 0px;"><strong>About Products</strong></p>
    </a>
      </li>
      <li class="nav-item">
        <a href="#menu1" data-toggle="tab"><p style="color:rgba(0, 0, 0, 1.0); border: 0px solid blue; padding: 0px;"><strong>Local Products</strong></p></a>
      </li>
      <li class="nav-item">
        <a href="#menu3" data-toggle="tab"><p  style="color:rgba(0, 0, 0, 1.0); border: 0px solid blue; padding: 0px;" ><strong>Summary</strong></p></a>
      </li>
      <li class="nav-item">
        <a href="#menu4" data-toggle="tab"><p  style="color:rgba(0, 0, 0, 1.0); border: 0px solid blue; padding: 0px;" ><strong>Done</strong></p></a>
      </li>

    </ul>
  </div>


        <div class="tab-content clearfix">
            <!-- Menu0 getStarted Start -->
            <div id="menu0" class="tab-pane fade active in">
                <div style="background-color: rgba(250, 250, 250, 1.00); border: 0px solid blue; padding: 0px;">
                    <div class="box-header with-border col-sm-12 col-md-12 col-lg-12">
                        <ul class="list-unstyled list-inline pull-right">
                            <li><a class="btn btn-primary view-pdf pull-right" href="<?php echo asset_url(); ?>HTML/ExpensesHelp.html">Help</a></li>
                        </ul>
                        <h3 class="box-title" style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-size:22px;font-weight:thin;"><strong>Online Business Planning</strong></h3>
                        <ul class="list-unstyled list-inline pull-right"></ul>
                        <h2 style="color:#3c8dbc;font-size:22px;font-weight:bold;">Planning is the key to your success !</h2>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <p style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-weight:light;">For a company, this is the total amount of revenue received by the company for services rendered fo s specific period of time </p>
                        <p style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-weight:light;">Personal Services Income (PSI) is income mainly derived from an individual’s personal efforts and skill.Generally, consultants and contractors operate as a sole trader or work through a company,that charges fees on an hourly basis or a fixes job rate. </p17
                        <p style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-weight:light;">In this moduel , you can enter a service, based on a number of hour your worded during w weekly cycle, and wnter the rate per hour that you may chagerad you can include a call out fee fro this service .  </p>
                        <h3 class="box-title"style="color:#3c8dbc;font-size:22px;font-weight:light;">What's next </strong></h3>
                        <p style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-weight:light;">In this Module,you can enter a service,based on a number of hours you worked during a weekly cycle,and the rate per hour that you charge your customer, you can also include a call out fee for this service.Click on the Projected Service Income to enter your service or services that you intend to provide</p>
                    </div><!-- /.box-body -->
                    <div class="box-footer">
                        <ul class="list-unstyled list-inline pull-right">
                    <li><button type="button" class="btn btn-info next-step">Next <i class="fa fa-chevron-right"></i></button></li>
                </ul>
                    </div>
                </div>


            </div>
            <!-- Menu1 local product Start -->
            <div id="menu1" class="tab-pane fade in">
                <!-- Menu One End -->
                <div style="background-color: rgba(250, 250, 250, 1.00); border: 0px solid blue; padding: 0px;">
                <div class="box-header with-border">
                    <h2 style="color:#3c8dbc;font-size:22px;font-weight:bold;">Local Products</h2>
                </div><!-- /.box-header -->

                <div class="box-body">
                    <!-- text input -->
                    <div id="show_local_product_row">
                        <?php include 'product_view.php'; ?>
                    </div>

                </div>
                <div class="box-footer">
                    <ul class="list-unstyled list-inline pull-right">
                        <li><button type="button" class="btn btn-default prev-step"><i class="fa fa-chevron-left"></i> Back</button></li>
                        <li><button type="button" class="btn btn-info next-step" id="report_section">Next <i class="fa fa-chevron-right"></i></button></li>
                    </ul>
                </div>
                </div>
            </div>
            <!-- Menu3 Reports Start -->
                <div id="menu3" class="tab-pane fade">
                <div class="box box-warning"style="background-color: rgba(250, 250, 250, 1.00); border: 0px solid blue; padding: 0px;">
                    <div class="box-header with-border">
                        <ul class="list-unstyled list-inline pull-right">
                            <li>
                                <a class="btn btn-primary view-pdf pull-right" href="<?php echo asset_url(); ?>HTML/ExpensesHelp.html">Help</a>
                            </li>
                        </ul>
                        <h3 class="box-title" style="color:rgba(250, 250, 250, 1.00);font-size:17px;font-size:22px;font-weight:thin;"><strong>Online Business Planning</strong></h3>
                        <ul class="list-unstyled list-inline pull-right"></ul>
                        <h2 style="color:#3c8dbc;font-size:22px;font-weight:bold;">Planning is the key to your success !</h2>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="row" id="local_product_summary">
                            <?php include 'LocalProductSummary_View.php'; ?>
                        </div>
                    </div>
                    <div class="box-footer">
                        <ul class="list-unstyled list-inline pull-right">
                            <li><button type="button" class="btn btn-default prev-step"><i class="fa fa-chevron-left"></i> Back</button></li>
                            <li><button type="button" class="btn btn-info next-step" id="summary_section">Next <i class="fa fa-chevron-right"></i></button></li>
                        </ul>
                    </div>
                </div>

            </div>
            <!-- Menu Three End -->
            <!-- Menu4 Summary Start -->
            <div id="menu4" class="tab-pane fade">
                <div class="box box-warning" style="background-color: rgba(250, 250, 250, 1.00); border: 0px solid blue; padding: 0px;">
                    <div class="box-header with-border">
                        <ul class="list-unstyled list-inline pull-right">
                            <li><a class="btn btn-primary view-pdf pull-right" href="<?php echo asset_url(); ?>HTML/ExpensesHelp.html">Help</a></li>
                        </ul>
                        <h3 class="box-title"style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-size:22px;font-weight:thin;"><strong>Online Business Planning</strong></h3>
                        <h2 style="color:#3c8dbc;font-size:22px;font-weight:bold;">Planning is the key to your success !</h2>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <p style="color:rgba(0, 0, 0, 1.0);font-size:22px;font-weight:light;">For a company, this is the total amount of revenue received by the company for services rendered fo s specific period of time </p>
                        <p style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-weight:light;">Personal Services Income (PSI) is income mainly derived from an individual’s personal efforts and skill.Generally, consultants and contractors operate as a sole trader or work through a company,that charges fees on an hourly basis or a fixes job rate. </p>
                        <p style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-weight:light;">In this moduel , you can enter a service, based on a number of hour your worded during w weekly cycle, and wnter the rate per hour that you may chager ad you can include a call out fee fro this service .  </p>
                        <h3 class="box-title"style="color:#3c8dbc;font-size:22px;font-weight:light;">What's next </strong></h3>
                        <p style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-weight:light;">In this Module,you can enter a service,based on a number of hours you worked during a weekly cycle,and the rate per hour that you charge your customer, you can also include a call out fee for this service.Click on the Projected Service Income to enter your service or services that you intend to provide</p>
                    </div><!-- /.box-body -->
                    <div class="box-footer">
                        <ul class="list-unstyled list-inline pull-right">
                            <li><button type="button" class="btn btn-default prev-step"><i class="fa fa-chevron-left"></i> Back</button></li>
                            <li><button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Done!</button></li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section><!-- /.content -->

<script src="<?php echo asset_url(); ?>plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?php echo asset_url(); ?>plugins/iCheck/icheck.min.js"></script>
<script type="text/javascript">
    $(function () {

        //$('#start_date').daterangepicker({singleDatePicker: true});
        $("[data-mask]").inputmask();
        $('.btn-circle').on('click', function () {
            $('.btn-circle.btn-info').removeClass('btn-info').addClass('btn-default');
            $(this).addClass('btn-info').removeClass('btn-default').blur();
        });
        $('.next-step, .prev-step').on('click', function (e) {
            var $activeTab = $('.tab-pane.active');
            $('.btn-circle.btn-info').removeClass('btn-info').addClass('btn-default');
            if ($(e.target).hasClass('next-step'))
            {
                var nextTab = $activeTab.next('.tab-pane').attr('id');
                $('[href="#' + nextTab + '"]').addClass('btn-info').removeClass('btn-default');
                $('[href="#' + nextTab + '"]').tab('show');
            } else
            {
                var prevTab = $activeTab.prev('.tab-pane').attr('id');
                $('[href="#' + prevTab + '"]').addClass('btn-info').removeClass('btn-default');
                $('[href="#' + prevTab + '"]').tab('show');
            }
        });
    });
</script>
