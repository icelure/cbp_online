<style type="text/css">
.process-step .btn:focus{outline:none}
.process{display:table;width:100%;position:relative}
.process-row{padding-top: 5px}
.process-step button[disabled]{opacity:1 !important;filter: alpha(opacity=100) !important}
.process-row:before{top:40px;bottom:0;position:absolute;content:" ";width:100%;height:1px;background-color:#ccc;z-order:0}
.process-step{display:table-cell;text-align:center;position:relative}
.process-step p{margin-top:4px}
.btn-circle{width:65px;height:65px;text-align:center;font-size:12px;border-radius:50%}
/*.tab-content{margin: 0 10% 0 10%;}*/
.error{color:rgba(255, 0, 0, 0.62);}
.input-group{width: 100%}
.img-circle {
    border-radius: 50%;
}

}
tr.selected {
    background-color: #B0BED9 !important;
}
}
h1,p{
color:white;
}
.bordered th, .bordered td{
padding:10px;
}
.bordered tbody tr:nth-child(odd){
background:#eee;
color:#000;
}
.bordered tbody tr:nth-child(even){
color:#fff;

}
.nav>li>a {
    padding: 7px 3px;
}


.tab-header{
  background-color:#ecf0f5; border: 0px solid blue; padding: 1px;
}


.nav-pills>li{
    margin-right: 5px;
    margin-bottom: -1px;

}
.nav-pills>li.active>a, .nav-pills>li.active>a:focus, .nav-pills>li.active>a:hover {
    color: #fff;
    background-color: #fafafa;
}
.nav-pills>li.active>a, .nav-pills>li.active>a:hover, .nav-pills>li.active>a:focus {
    border-top-color: #60979c;
    border-bottom-color: transparent;
}

.nav-pills>li>a, .nav-pills>li>a:hover {
    background-color: #d4d7dc;
}

.nav-pills>li>a {
    border-radius: 0;
}

@media (max-width: 920px){
  #tabs {
    display: none;
  }
  .nav-pills>li {
    width: 75px;
    text-align: center;
  }
}

@media (max-width: 525px){
  #tabs {
    display: none;
  }
  .nav-pills>li {
    width: 40px;
    text-align: center;
  }
}

 .input-group .input-group-addon {

        border: 1px solid #ccc;
        width: 48px !important;
    }

</style>

<section class="content-header">
    <h1>Imported Products</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Imported Products</a></li>
    </ol>
</section>

<section class="content">
  <div class="tab-header">
    <ul class="process-row nav nav-pills">
      <li class="nav-item active">
        <a href="#menu0" data-toggle="tab"><p style="color:rgba(0, 0, 0, 1.0); border: 0px solid blue; margin:0 5px 10px;"><i class="glyphicon glyphicon-info-sign"></i> <strong id="tabs">About</strong></p>
      </a>
      </li>
      <li class="nav-item">
        <a href="#menu2" data-toggle="tab"><p style="color:rgba(0, 0, 0, 1.0); border: 0px solid blue; margin:0 5px 10px;"><i class="glyphicon glyphicon-lamp"></i> <strong id="tabs">Products</strong></p>
        </a>
      </li>
      <li class="nav-item">
        <a href="#menu3" data-toggle="tab"><p  style="color:rgba(0, 0, 0, 1.0); border: 0px solid blue; margin:0 5px 10px;"><i class="glyphicon glyphicon-thumbs-up"></i> <strong id="tabs">Interaction</strong></p>
        </a>
      </li>
      <li class="nav-item">
        <a href="#menu4" data-toggle="tab"><p  style="color:rgba(0, 0, 0, 1.0); border: 0px solid blue; margin:0 5px 10px;"><i class="glyphicon glyphicon-save-file"></i> <strong id="tabs">Summary</strong></p>
        </a>
      </li>
    </ul>
  </div> <!-- end tab header-->

  <div class="tab-content clearfix">
    <div id="menu0" class="tab-pane fade active in">
      <div style="background-color: rgba(250, 250, 250, 1.00); border: 0px solid blue; padding: 0px;">
        <div class="box-header with-border">
            <h3 class="box-title" style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-size:22px;font-weight:thin;"><strong>What is products Income ?</strong></h3>
            <ul class="list-unstyled list-inline pull-right">
                <li><a class="btn btn-primary view-pdf pull-right" href="<?php echo asset_url(); ?>HTML/ExpensesHelp.html">Help</a></li>
            </ul>
        </div><!-- /.box-header -->
        <div class="box-body">
            <p style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-weight:light;">For a company, this is the total amount of revenue received by the company for services rendered fo s specific period of time </p>
            <p style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-weight:light;">Personal Services Income (PSI) is income mainly derived from an individual’s personal efforts and skill.Generally, consultants and contractors operate as a sole trader or work through a company,that charges fees on an hourly basis or a fixes job rate. </p>
            <p style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-weight:light;">In this moduel , you can enter a service, based on a number of hour your worded during w weekly cycle, and wnter the rate per hour that you may chager ad you can include a call out fee fro this service .  </p>
            <p style="font-size:22px;font-weight:light;"><strong>Note:</strong> click on each option and enter your text </p>
        </div><!-- /.box-body -->
        <div class="box-footer">
            <ul class="list-unstyled list-inline pull-right">
              <li><button type="button" class="btn btn-info next-step">Next <i class="fa fa-chevron-right"></i></button></li>
            </ul>
        </div>
      </div>
    </div> <!-- end menu0-->
    <div id="menu2" class="tab-pane fade in">
      <div style="background-color: rgba(250, 250, 250, 1.00); border: 0px solid blue; padding: 0px;">
        <div class="box-header with-border">
            <h2 style="color:#3c8dbc;font-size:22px;font-weight:bold;">Imported Products</h2>
        </div><!-- /.box-header -->
        <div class="box-body">
            <!-- text input -->
          <div id="show_imported_products">
              <?php include 'importedProduct_view.php'; ?>
          </div>
        </div>
        <div class="box-footer">
            <ul class="list-unstyled list-inline pull-right">
              <li><button type="button" class="btn btn-default prev-step"><i class="fa fa-chevron-left"></i> Back</button></li>
              <li><button type="button" class="btn btn-info next-step" id="report_section">Next <i class="fa fa-chevron-right"></i></button></li>
            </ul>
        </div>
      </div>
    </div><!-- end menu2-->
    <div id="menu3" class="tab-pane fade in">
      <!--<div class="container">-->
      <div class="box box-warning"style="background-color: rgba(250, 250, 250, 1.00); border: 0px solid blue; padding: 0px;">
        <div class="box-header with-border">
            <ul class="list-unstyled list-inline pull-right">
                <li>
                    <a class="btn btn-primary view-pdf pull-right" href="<?php echo asset_url(); ?>HTML/ExpensesHelp.html">Help</a>
                </li>
            </ul>
            <h3 class="box-title" style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-size:22px;font-weight:thin;"><strong>Online Business Planning</strong></h3>
            <ul class="list-unstyled list-inline pull-right"></ul>
            <h2 style="color:#3c8dbc;font-size:22px;font-weight:bold;">Planning is the key to your success !</h2>
        </div><!-- /.box-header -->
        <div class="box-body">
            <div class="row" id="imported_product_summary">
              <?php include 'importedproductsummary_view.php'; ?>
            </div>
        </div>
        <div class="box-footer">
            <ul class="list-unstyled list-inline pull-right">
              <li><button type="button" class="btn btn-default prev-step"><i class="fa fa-chevron-left"></i> Back</button></li>
              <li><button type="button" class="btn btn-info next-step" id="summary_section">Next <i class="fa fa-chevron-right"></i></button></li>
            </ul>
        </div>
      </div>
    </div><!-- end menu3-->
    <div id="menu4" class="tab-pane fade in">
      <div style="background-color: rgba(250, 250, 250, 1.00);; border: 0px solid blue; padding: 0px;">
        <div class="box-header with-border box-header with-border col-sm-12 col-md-12 col-lg-12">
            <h3 class="box-title"style="font-size:17px;font-weight:light;"><strong>You have completed setting up your products</strong></h3>
            <ul class="list-unstyled list-inline pull-right">
                <li><a class="btn btn-primary view-pdf pull-right" href="<?php echo asset_url(); ?>HTML/ExpensesHelp.html">Help</a></li>
            </ul>
            <h2 style="font-size:25px;font-weight:light;"></h2>
            <p style="font-size:17px;font-weight:light;"><strong>Note:</strong>click on each option and enter your text </p>
        </div><!-- /.box-header -->
        <div class="box-body">
            <p style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-weight:light;">For a company, this is the total amount of revenue received by the company for services rendered fo s specific period of time </p>
            <p style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-weight:light;">Personal Services Income (PSI) is income mainly derived from an individual’s personal efforts and skill.Generally, consultants and contractors operate as a sole trader or work through a company,that charges fees on an hourly basis or a fixes job rate. </p>
            <p style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-weight:light;">In this moduel , you can enter a service, based on a number of hour your worded during w weekly cycle, and wnter the rate per hour that you may chager ad you can include a call out fee fro this service .  </p>
        </div><!-- /.box-body -->
        <div class="box-footer">
          <ul class="list-unstyled list-inline pull-right">
            <li><button type="button" class="btn btn-default prev-step"><i class="fa fa-chevron-left"></i> Back</button></li>
            <li><button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Done!</button></li>
          </ul>
        </div>
      </div>
    </div><!-- end menu4-->
  </div><!-- end tab content-->
</section> <!-- end sction content-->
<div class="modal fade" id="impmodal_form" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Imported Product Form</h3>
      </div>
      <div class="modal-body form">
        <div class="tab-header">
          <ul class="nav nav-pills" style="top:40px;">
            <li class="nav-item active">
              <a href="#form1" data-toggle="tab"><p style="color:rgba(0, 0, 0, 1.0); border: 0px solid blue; margin:0 5px 10px;"><i class="glyphicon glyphicon-info-sign"></i> <strong id="tabs">About</strong></p>
              </a>
            </li>
            <li class="nav-item">
              <a href="#form2" data-toggle="tab"><p style="color:rgba(0, 0, 0, 1.0); border: 0px solid blue; margin:0 5px 10px;"><i class="glyphicon glyphicon-user"></i> <strong id="tabs">Import Costs I</strong></p></a>
            </li>
            <li class="nav-item">
              <a href="#form3" data-toggle="tab"><p style="color:rgba(0, 0, 0, 1.0); border: 0px solid blue; margin:0 5px 10px;"><i class="glyphicon glyphicon-user"></i> <strong id="tabs">Import Costs II</strong></p></a>
            </li>
          </ul>
        </div>
        <form action="#" id="importform" class="form-horizontal" enctype="multipart/form-data">
        <div class="tab-content clearfix">
          <div id="form1" class="tab-pane fade active in">
            <div style="border: 0px solid blue; margin: 10px;">
              <input type="hidden" value="" name="importedid"/>
              <div class="form-body">
                <div class="form-group">
                    <label class="control-label col-md-4">Description</label>
                    <div class="col-md-8">
                       <div class="input-group">
                          <div class="input-group-addon">
                              <i class="fa fa-pencil"></i>
                          </div>
                          <input name="importedproduct_description" placeholder="Product Description" class="form-control" type="text">
                          <span class="help-block"></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                <label class="control-label col-md-4">Quantity</label>
                <div class="col-md-8">
                     <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calculator"></i>
                      </div>
                      <input name="ImportQuantity" placeholder="Quantity" class="form-control" type="number">
                      <span class="help-block"></span>
                    </div>
                </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-4">Unit Cost</label>
                  <div class="col-md-8">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-dollar"></i>
                        </div>
                        <input name="imported_unit_cost" placeholder="Unit Cost" class="form-control" type="number">
                        <span class="help-block"></span>
                      </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-4">Exchange Rate</label>
                  <div class="col-md-8">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-gg-circle"></i>
                        </div>
                        <input name="importedexchange_range" placeholder="Exchange Rate" class="form-control" type="number">
                        <span class="help-block"></span>
                      </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-4">Markup on Cost</label>
                  <div class="col-md-8">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-percent"></i>
                        </div>
                        <input name="imported_markup_on_cost" placeholder="Markup on Cost" class="form-control" type="number">
                        <span class="help-block"></span>
                      </div>
                  </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4">Picture</label>
                    <div class="col-md-8">
                        <input name="importedflnFile" placeholder="Markup on Cost" class="form-control" type="file">
                        <span class="help-block"></span>
                    </div>
                </div>
              </div><!--End form body-->
            </div> <!-- End Style -->
          </div><!--end form1-->
          <div id="form2" class="tab-pane fade in">
            <div style="border: 0px solid blue; margin: 10px;">
              <div class="form-body">
                <div class="form-group">
                  <label class="control-label col-md-4">Import Duty</label>
                  <div class="col-md-8">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-percent"></i>
                        </div>
                        <input name="import_duty" placeholder="Import Duty" class="form-control" type="number">
                        <span class="help-block"></span>
                      </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-4">CMR Comp Fee</label>
                  <div class="col-md-8">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-percent"></i>
                        </div>
                        <input name="comp_fee" placeholder="CMR Comp Fee" class="form-control" type="number">
                        <span class="help-block"></span>
                      </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-4">Cargo Auto Fee</label>
                  <div class="col-md-8">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-percent"></i>
                        </div>
                        <input name="cargo_auto" placeholder="Cargo Auto Fee" class="form-control" type="number">
                        <span class="help-block"></span>
                      </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-4">Custom Cleareance Fee</label>
                  <div class="col-md-8">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-percent"></i>
                        </div>
                        <input name="custom_clearance_fee" placeholder="Custom Cleareance Fee" class="form-control" type="number">
                        <span class="help-block"></span>
                      </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-4">AQIS Fee</label>
                  <div class="col-md-8">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-percent"></i>
                        </div>
                        <input name="aqis_fee" placeholder="AQIS Fee" class="form-control" type="number">
                        <span class="help-block"></span>
                      </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-4">Dec Processing Fee</label>
                  <div class="col-md-8">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-percent"></i>
                        </div>
                        <input name="dec_processing_fee" placeholder="Dec Processing Fee" class="form-control" type="number">
                        <span class="help-block"></span>
                      </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-4">Delivery Order</label>
                  <div class="col-md-8">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-percent"></i>
                        </div>
                        <input name="delivery_order" placeholder="Delivery Order" class="form-control" type="number">
                        <span class="help-block"></span>
                      </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-4">LCL Transport Fee</label>
                  <div class="col-md-8">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-percent"></i>
                        </div>
                        <input name="lcl_transport_fee" placeholder="LCL Transport Fee" class="form-control" type="number">
                        <span class="help-block"></span>
                      </div>
                  </div>
                </div>
              </div><!--End form body-->
            </div> <!-- End Style -->
          </div><!--end form2-->
          <div id="form3" class="tab-pane fade in">
            <div style="border: 0px solid blue; margin: 10px;">
              <div class="form-body">
                <div class="form-group">
                  <label class="control-label col-md-4">Port Service Fee</label>
                  <div class="col-md-8">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-percent"></i>
                        </div>
                        <input name="port_service_fee" placeholder="Port Service Fee" class="form-control" type="number">
                        <span class="help-block"></span>
                      </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-4">Transport Fues Fee</label>
                  <div class="col-md-8">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-percent"></i>
                        </div>
                        <input name="transport_fues_fee" placeholder="Transport Fues Fee" class="form-control" type="number">
                        <span class="help-block"></span>
                      </div>
                  </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4">Insurance Fee</label>
                    <div class="col-md-8">
                        <div class="input-group">
                          <div class="input-group-addon">
                              <i class="fa fa-percent"></i>
                          </div>
                          <input name="insurance_fee" placeholder="Insurance Fee" class="form-control" type="number">
                          <span class="help-block"></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-4">Misc Fee</label>
                  <div class="col-md-8">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-percent"></i>
                        </div>
                        <input name="misc_fee" placeholder="Misc Fee" class="form-control" type="number">
                        <span class="help-block"></span>
                      </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-4">Others Fee 1</label>
                  <div class="col-md-8">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-percent"></i>
                        </div>
                        <input name="other_fee_1" placeholder="Others Fee 1" class="form-control" type="number">
                        <span class="help-block"></span>
                      </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-4">Others Fee 2</label>
                  <div class="col-md-8">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-percent"></i>
                        </div>
                        <input name="other_fee_2" placeholder="Others Fee 2" class="form-control" type="number">
                        <span class="help-block"></span>
                      </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-4">Others Fee 3</label>
                  <div class="col-md-8">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-percent"></i>
                        </div>
                        <input name="other_fee_3" placeholder="Others Fee 3" class="form-control" type="number">
                        <span class="help-block"></span>
                      </div>
                  </div>
                </div>
              </div><!--End form body-->
            </div> <!-- End Style -->
          </div><!--end form3-->
          </div><!--tab content-->
          <div class="modal-footer">
              <input type="submit" id="btnSaveImport" value="Save" class="btn btn-primary">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
          </div>
        </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript">
$(function () {
  //$('#start_date').daterangepicker({singleDatePicker: true});
  $("[data-mask]").inputmask();
  $('.btn-circle').on('click', function () {
      $('.btn-circle.btn-info').removeClass('btn-info').addClass('btn-default');
      $(this).addClass('btn-info').removeClass('btn-default').blur();
  });
  $('.next-step, .prev-step').on('click', function (e) {
      var $activeTab = $('.tab-pane.active');
      $('.btn-circle.btn-info').removeClass('btn-info').addClass('btn-default');
      if ($(e.target).hasClass('next-step'))
      {
          var nextTab = $activeTab.next('.tab-pane').attr('id');
          $('[href="#' + nextTab + '"]').removeClass('btn-default');
          $('[href="#' + nextTab + '"]').tab('show');
          $("body").scrollTop(0);
      } else
      {
          var prevTab = $activeTab.prev('.tab-pane').attr('id');
          $('[href="#' + prevTab + '"]').removeClass('btn-default');
          $('[href="#' + prevTab + '"]').tab('show');
          $("body").scrollTop(0);
      }
  });
  //redraw the datatables
  $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    var target = $(e.target).attr("href") // activated tab
    if ((target == '#menu3')) {

        table1.ajax.reload(null, false);

    }
  });
});
function add_importedproduct(){

  $("label.error").remove();
  save_method = 'add';
  $('#importform')[0].reset(); // reset form on modals
  $('.form-group').removeClass('has-error'); // clear error class
  $('.help-block').empty(); // clear error string
  $('#impmodal_form').modal('show'); // show bootstrap modal
  $('.modal-title').text('Add Imported Product'); // Set Title to Bootstrap modal title
}

function edit_importedproduct(id){

  $("label.error").remove();
  save_method = 'update';
  $('#importform')[0].reset(); // reset form on modals
  $('.form-group').removeClass('has-error'); // clear error class
  $('.help-block').empty(); // clear error string

  //Ajax Load data from ajax
  $.ajax({
    url: "<?php echo site_url('ImportedProducts/ajax_editimported') ?>/" + id,
    type: "GET",
    dataType: "JSON",
    success: function (data){

      $('[name="importedid"]').val(data.id);
      $('[name="importedproduct_description"]').val(data.Description);
      $('[name="ImportQuantity"]').val(data.Qty);
      $('[name="imported_unit_cost"]').val(data.UnitCost);
      $('[name="importedexchange_range"]').val(data.ExchangeRate);

      $('[name="imported_markup_on_cost"]').val(data.MarkUpOnCost);

      $('[name="import_duty"]').val(data.import_duty);
      $('[name="comp_fee"]').val(data.comp_fee);
      $('[name="cargo_auto"]').val(data.cargo_auto);
      $('[name="custom_clearance_fee"]').val(data.custom_clearance_fee);
      $('[name="aqis_fee"]').val(data.aqis_fee);
      $('[name="dec_processing_fee"]').val(data.dec_processing_fee);
      $('[name="delivery_order"]').val(data.delivery_order);
      $('[name="lcl_transport_fee"]').val(data.lcl_transport_fee);
      $('[name="port_service_fee"]').val(data.port_service_fee);
      $('[name="transport_fues_fee"]').val(data.transport_fues_fee);
      $('[name="insurance_fee"]').val(data.insurance_fee);
      $('[name="misc_fee"]').val(data.misc_fee);
      $('[name="other_fee_1"]').val(data.other_fee_1);
      $('[name="other_fee_2"]').val(data.other_fee_2);
      $('[name="other_fee_3"]').val(data.other_fee_3);

      $('#impmodal_form').modal('show'); // show bootstrap modal when complete loaded

      $('#impmodal_form').on('shown.bs.modal', function() {
          $('.nav a[href="#form1"]').tab('show');
      })

      $('.modal-title').text('Edit Imported Product'); // Set title to Bootstrap modal title

    },
    error: function (jqXHR, textStatus, errorThrown){

        alert(errorThrown);
    }
  });
}

function saveimported(){

  $('#btnSave').text('saving...'); //change button text
  $('#btnSave').attr('disabled', true); //set button disable
  var url;

  if (save_method == 'add') {
      url = "<?php echo site_url('ImportedProducts/ajax_addimported') ?>";
  } else {
      url = "<?php echo site_url('ImportedProducts/ajax_updateimported') ?>";
  }

  // ajax adding data to database
  $.ajax({
      url: url,
      type: "POST",
      data: new FormData($('#importform')[0]),
      contentType: false,
      cache: false,
      processData: false,
      success: function (data){
          var obj = jQuery.parseJSON(data);

          if (obj['status']) //if success close modal and reload ajax table
          {
              $('#impmodal_form').modal('hide');
              drawCollectionView()
          }

          $('#btnSave').text('save'); //change button text
          $('#btnSave').attr('disabled', false); //set button enable
      },
      error: function (jqXHR, textStatus, errorThrown){

          //alert('Error adding / update data');
          $('#btnSave').text('save'); //change button text
          $('#btnSave').attr('disabled', false); //set button enable

      }
  });
}

function loaddetail(id){
  //Ajax Load data from ajax
  $.ajax({
      url: "<?php echo site_url('ImportedProducts/ajax_get_imported_product') ?>/" + id,
      type: "GET",
      dataType: "JSON",
      success: function (data){

        var Total = data.Qty * data.UnitCost;
        var TotalLandedCost = Total * data.ExchangeRate;
        //alert(data.Qty);
        $("#imgthumbnail").attr("src", data.ThumbNail);
        $("#impdesc").html(data.Description);
        $("#impSolQnt").html(data.Qty);
        $("#impUC").html(data.UnitCost);
        //$("#imptotal").html(data.Qty * data.UnitCost);
        $("#imptotal").html(Total.toFixed(2));
        $("#impExchangeRange").html(data.ExchangeRate);
        $TotalLandedCost = data.Qty * data.UnitCost * data.ExchangeRate
        $("#impTotalLandedCost").html(TotalLandedCost.toFixed(2));
        //var ImportDuty=(TotalLandedCost*1)/100;
        // ImportDuty=round(ImportDuty,0);
        $("#impID").html((data.import_duty * TotalLandedCost).toFixed(2));
        //var DeliveryOrder=(TotalLandedCost*1)/100;
        $("#impDo").html((data.delivery_order * $TotalLandedCost).toFixed(2));
        //var impCmF=(TotalLandedCost*1)/100;
        $("#impCmF").html((data.cmr_comp_fee * $TotalLandedCost).toFixed(2));
        //var impLTF=(TotalLandedCost*1)/100;
        $("#impLTF").html((data.lcl_transport_fee * $TotalLandedCost).toFixed(2));
        //var impca=(TotalLandedCost*1)/100;
        $("#impca").html((data.cargo_auto_fee * $TotalLandedCost).toFixed(2));
        //var impps=(TotalLandedCost*1)/100;
        $("#impps").html((data.port_service_fee * $TotalLandedCost).toFixed(2));
        //var impcc=(TotalLandedCost*1)/100;
        $("#impcc").html((data.custom_clearance_fee * $TotalLandedCost).toFixed(2));
        //var imptp=(TotalLandedCost*1)/100;
        $("#imptp").html((data.transport_fues_fee * $TotalLandedCost).toFixed(2));
        //var impAFt=(TotalLandedCost*1)/100;
        $("#impAFt").html((data.aqis_fee * $TotalLandedCost).toFixed(2));
        //var impIf=(TotalLandedCost*1)/100;
        $("#impIf").html((data.insurance_fee * $TotalLandedCost).toFixed(2));
        //var impDp=(TotalLandedCost*1)/100;
        $("#impDp").html((data.dec_processing_fee * $TotalLandedCost).toFixed(2));
        //var impMf=(TotalLandedCost*1)/100;
        $("#impMf").html((data.misc_fee * $TotalLandedCost).toFixed(2));
        //var impo1=(TotalLandedCost*1)/100;
        $("#impo1").html((data.other_fee_1 * $TotalLandedCost).toFixed(2));
        //var impo2=(TotalLandedCost*1)/100;
        $("#impo2").html((data.other_fee_2 * $TotalLandedCost).toFixed(2));
        //var impo3=(TotalLandedCost*1)/100;
        $("#impo3").html((data.other_fee_3 * $TotalLandedCost).toFixed(2));
//            var impTPc=parseFloat(TotalLandedCost)+parseFloat(ImportDuty)+parseFloat(DeliveryOrder)+parseFloat(impCmF)+parseFloat(impLTF)+parseFloat(impDp)+parseFloat(impMf)+parseFloat(impo1)+parseFloat(impo2)+parseFloat(impo3)+parseFloat(impca)+parseFloat(impps)+parseFloat(imptp)+parseFloat(impIf)+parseFloat(impAFt)+parseFloat(impcc);
//           impTPc=Math.round(impTPc * 100) / 100;
        $ToalProductCost = ($TotalLandedCost) + (data.import_duty * $TotalLandedCost) + (data.delivery_order * $TotalLandedCost) +
                (data.cmr_comp_fee * $TotalLandedCost) + (data.lcl_transport_fee * $TotalLandedCost) + (data.cargo_auto_fee * $TotalLandedCost)
                + (data.port_service_fee * $TotalLandedCost) + (data.custom_clearance_fee * $TotalLandedCost) + (data.transport_fues_fee * $TotalLandedCost)
                + (data.aqis_fee * $TotalLandedCost) + (data.insurance_fee * $TotalLandedCost) + (data.dec_processing_fee * $TotalLandedCost)
                + (data.misc_fee * $TotalLandedCost) + (data.other_fee_1 * $TotalLandedCost) + (data.other_fee_2 * $TotalLandedCost) + (data.other_fee_3 * $TotalLandedCost)
        //$("#impTPc").html(data.TotalProductCost);
        $("#impTPc").html($ToalProductCost.toFixed(2));


//            var impunitcost=parseFloat(impTPc)/parseFloat(data.Qty);
//              impunitcost=Math.round(impunitcost * 100) / 100;
        var UnitCost = $ToalProductCost / data.Qty;
        $("#impunitcost").html(UnitCost.toFixed(2));

//            var impMarkup=data.MarkUpOnCost;
//            impMarkup=Math.round(impMarkup * 100) / 100;
        $("#impMarkup").html((data.MarkUpOnCost * UnitCost).toFixed(2));

        var impWp = (data.MarkUpOnCost * UnitCost) + UnitCost;
//            impWp=impWp+impunitcost;
//             impWp=Math.round(impWp * 100) / 100;
        $("#impWp").html(impWp.toFixed(2));
        var impGP = ((data.MarkUpOnCost * ($ToalProductCost / data.Qty)) + ($ToalProductCost / data.Qty)) - ($ToalProductCost / data.Qty);
//            impGP=Math.round(impGP * 100) / 100;
        $("#impGP").html(impGP.toFixed(2));

        var impTotalRevenue = ((data.MarkUpOnCost * UnitCost) + UnitCost) * data.Qty;
//            impTotalRevenue=Math.round(impTotalRevenue * 100) / 100;
        $("#impTotalRevenue").html(impTotalRevenue.toFixed(2));

//            var impTC=impunitcost*data.Qty;
//            impTC=Math.round(impTC * 100) / 100;
        $("#impTC").html(($ToalProductCost * data.Qty).toFixed(2));

//            var impTPc=impTR-impTC;
//            impTPc=Math.round(impTPc * 100) / 100;
        var GrossTotal = (((data.MarkUpOnCost * ($ToalProductCost / data.Qty)) + ($ToalProductCost / data.Qty)) * data.Qty) - ($ToalProductCost * data.Qty);
        $("#impgross").html(GrossTotal.toFixed(2));
        //  alert(data.Description);

        $('#impmodal_form_Detail').modal('show'); // show bootstrap modal when complete loaded

        $('#impmodal_form_Detail').on('shown.bs.modal', function() {
            $('.nav a[href="#tab1"]').tab('show');
        })

        $('.modal-title').text('Imported Product Detail'); // Set title to Bootstrap modal title
      },
      error: function (jqXHR, textStatus, errorThrown){
          //alert(errorThrown);

      }
  });
}

function delete_importedproduct(id){

    if (confirm('Are you sure you want to delete this data?')){

      $.ajax({
        url: "<?php echo site_url('ImportedProducts/ajax_deleteimported') ?>/" + id,
        type: "POST",
        dataType: "JSON",
        success: function (data){
            //if success reload ajax table
            $('#impmodal_form').modal('hide');
            drawCollectionView()
        },
        error: function (jqXHR, textStatus, errorThrown){
            alert(errorThrown);
        }
      });
    }
}
</script>