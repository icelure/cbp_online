<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>CBPOnline | Dashboard</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?=asset_url()?>bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?=asset_url()?>dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?=asset_url()?>dist/css/skins/_all-skins.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?=asset_url()?>plugins/iCheck/flat/blue.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="<?=asset_url()?>plugins/morris/morris.css">

    <!-- Kartik File Input -->
    <link rel="stylesheet" href="<?=asset_url()?>plugins/kartik/css/fileinput.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="<?=asset_url()?>plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="<?=asset_url()?>plugins/datepicker/datepicker3.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="<?=asset_url()?>plugins/daterangepicker/daterangepicker-bs3.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="<?=asset_url()?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

    <link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/iCheck/all.css">

    <link href="<?php echo base_url('assets/datatables/css/dataTables.bootstrap.css')?>" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo asset_url(); ?>css/model.css">


	 <style>
	body {
		font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
		font-size: 14px;
		line-height: 1.42857143;
		color: #3c8dbc;
		/*background-color: rgba(250, 250, 250, 1.0);*/
    background-color: black;
	}

    .wrapper {
    /*min-height: 100%;
    position: relative;
    overflow: hidden;*/
    width: 100%;
    min-height: 100%;
    height: auto !important;
    position: absolute;
  }

  .input-group {
    width:100%;
  }

	.box{
			background-color: rgba(250, 250, 250, 1.0);
			border: 0px solid blue;
			padding: 0px;
	}
  .skin-blue .sidebar-menu>li.active>a {
    color: #fff;
    background: #1c5369;
    border-right-color: #3c8dbc;
  }


  .main-header>.navbar {
    height: 60px;
  }

  .main-header .logo {
    height: 60px;
  }

  .sidebar-toggle {
    height: 60px;
  }

  .user-panel {
    text-align: center;
    min-height: 125px;
  }

  .user-panel>.image>img {
    width: 100%;
    max-width: 60px;
    height: auto;
    padding: 5px;
  }

  .dropdown-menu>.user-img{
    text-align: center;
    background: #3c8dbc;
    height: 100px;
  }

  .dropdown-menu .divider {
    margin: 2px 0;
  }

  @media (max-width: 767px){

    .skin-blue .main-header .navbar .dropdown-menu li a {
      color: #3c8dbc;
    }

  }


	</style>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- jQuery 2.1.4 -->
    <script src="<?=asset_url()?>plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>

    <!-- Kartik File Input -->
    <script src="<?=asset_url()?>plugins/kartik/js/fileinput.js"></script>

    <script src="<?php echo base_url('assets/datatables/js/jquery.dataTables.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/datatables/js/dataTables.bootstrap.js') ?>"></script>
    <!-- daterangepicker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src="<?=asset_url()?>plugins/daterangepicker/daterangepicker.js"></script>
    <script src="<?php echo asset_url(); ?>plugins/input-mask/jquery.inputmask.js"></script>
    <script type="text/javascript" src="<?php echo js_url(); ?>jquery.validate.min.js"></script>
    <!-- Country -->
    <script src="<?=asset_url()?>js/country.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script src="<?php echo asset_url(); ?>js/model.js"></script>
    <script>
      $.widget.bridge('uibutton', $.ui.button);
      site_url = "<?php print base_url(); ?>";
    </script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?=asset_url()?>bootstrap/js/bootstrap.min.js"></script>
  </head>
  <body class="hold-transition skin-blue sidebar-mini">

    <div class="wrapper">

    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              <h3 id="myModalLabel">Confirm Logout ?</h3>
            </div>
            <div class="modal-body">
            <p class="error-text"><i class="icon-warning-sign modal-icon"></i>Are you sure you want clsoe your current sesssion ?</p>
            </div>
            <div class="modal-footer">
              <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
              <button class="btn btn-logout" id="btn-logout" data-dismiss="modal">Logout</button>
            </div>
          </div>
        </div>
      </div>
      <header class="main-header">
        <!-- Logo -->
        <a href="#" id="logo" class="logo" data-href="<?php echo base_url().'logout'; ?>" data-target="#confirm-delete" data-toggle="modal">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>CBP</b></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><img src="<?=img_url()?>cbpLogoSmall.png" style="margin-bottom: 6px;margin-right: 3px;"><b>CBP</b>Online</span>
        </a>


      <!-- Header Navbar: style can be found in header.less -->
      <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="sidebar-toggle fa fa-2x" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
              <li class="dropdown user user-menu">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="fa fa-cogs fa-2x"></span><span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li class="user-img">

                        <img src="<?php echo ($user->photo)? asset_url().'uploads/users/'.$user->photo:asset_url().'uploads/users/no-photo.jpg'?>" class="img-circle" alt="<?php echo $user->f_name.' '. $user->l_name ?>" style="padding:5px">

                    </li>
                    <li class="divider"></li>
                    <li>
                       <p align="center"><?php echo $user->f_name.' '. $user->l_name;?></p>
                       <p align="center"></small><?php echo (isset($company_detail['start_date']))? '<small>Plan start since '.date('F Y', strtotime($company_detail['start_date'])):''; ?></small></p>
                    </li>
                    <li class="divider"></li>
                    <li>
                      <a href="<?php echo base_url() ?>profile"><span class="glyphicon glyphicon-user"></span>Edit Profile</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                      <a href="<?php echo base_url(); ?>company_setup"><span class="glyphicon glyphicon-cog"></span>General Settings</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                      <a href="<?php echo base_url() ?>logout"><span class="glyphicon glyphicon-off"></span>Logout</a>
                    </li>
                  </ul>
              </li>
          </ul>
        </div>
      </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">


            <div class="image">
              <img src="<?php echo ($user->photo)? asset_url().'uploads/users/'.$user->photo:asset_url().'uploads/users/no-photo.jpg'?>" class="img-circle" alt="User Image">
            </div>
            <div class="info">
              <p><?php echo $user->f_name.' '. $user->l_name ?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>

          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>

            <?php if($user->type == 'a') { ?>
            <li class="treeview <?php ($page == "dashboard" || $page == "Dashboard") ? print 'active' : '' ?>">
              <a href="<?php echo base_url(); ?>Dashboard">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span> <!-- <i class="fa fa-angle-left pull-right"></i> -->
              </a>
            </li>
            <li class="treeview <?php ($page == "users" || $page == "Users") ? print 'active' : '' ?>">
              <a href="#">
                <i class="fa fa-users"></i>
                  <span>Users</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo base_url(); ?>users">Users</a>
                <li><a href="">Menu 2</a>
              </ul>
            </li>
            <li class="treeview <?php ($page == "payments" || $page == "Payments") ? print 'active' : '' ?>">
              <a href="#">
                <i class="fa fa-money"></i>
                  <span>Payments</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li>
                  <a href="<?php echo base_url(); ?>payments">Payments</a>
                </li>
                <li>
                  <a href="#">Payments 2</a>
                </li>
              </ul>
            </li>

            <li class="treeview <?php ($page == "Manage_plans" || $page == "manage_plans") ? print 'active' : '' ?>"">
              <a href="#">
                <i class="fa fa-th"></i>
                  <span>Manage Plans</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li>
                  <a href="<?php echo base_url(); ?>manage_plans">Plans</a>
                </li>
              </ul>
            </li>

            <?php } else{ ?>
            <li class="treeview <?php ($page == "User_dashboard" || $page == "user_dashboard") ? print 'active' : '' ?>">
              <a href="<?php echo base_url(); ?>user_dashboard">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
              </a>
            </li>

            <li class="treeview <?php ($page == "Company_setup" || $page == "company_setup") ? print 'active' : '' ?>">
              <a href="">
                <i class="fa fa-cogs"></i>
                  <span>Settings</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li>
                  <a href="<?php echo base_url(); ?>company_setup">Company Setup</a>
                </li>
                <li>
                  <a href="#">Company Setup 2</a>
                </li>
              </ul>
            </li>
            <li>

            <li class="treeview <?php ($page == "startup" || $page == "Startup") ? print 'active' : '' ?>">
              <a href="#">
                <i class="fa fa-wrench"></i>
                  <span>Startup</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li>
                  <a href="<?php echo base_url(); ?>startup">Start up</a>
                </li>
                <li>
                  <a href="#">Start up 2</a>
                </li>
              </ul>
            </li>
			       <li class="treeview <?php ($page == "planning_setup" || $page == "Planning_setup") ? print 'active' : '' ?>">
              <a href="#">
                <i class="fa fa-pencil-square-o"></i>
                  <span> Planning </span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li>
                  <a href="<?php echo base_url(); ?>Planning_setup">Planning Setup</a>
                </li>
                <li>
                  <a href="#">Planning Setup 2</a>
                </li>
              </ul>
            </li>
              <li class="treeview <?php ($page == "expenses" || $page == "Expenses") ? print 'active' : ''?>">
                <a href="#">
                  <i class="fa fa-calculator"></i>
                    <span>Expenses</span>
                  <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li>
                    <a href="<?php echo base_url(); ?>expenses">Expenses</a>
                  </li>
                  <li>
                    <a href="#">Expenses 2</a>
                  </li>
                </ul>
            </li>

            <li class="treeview <?php ($page == "products" || $page == "Products" || $page == "ImportedProducts" || $page == "importedproduct") ? print 'active' : '' ?>">
              <a href="#">
                <i class="fa fa-picture-o"></i>
                  <span>Products</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li>
                  <a href="<?php echo base_url(); ?>ImportedProducts">Imported Products</a>
                </li>
                <li>
                  <a href="<?php echo base_url(); ?>Products">Products Local</a>
                </li>
              </ul>
            </li>

            <li class="treeview <?php ($page == "service_setup" || $page == "Service_setup") ? print 'active' : '' ?>">
               <a href="#">
                <i class="fa fa-tasks"></i>
                  <span>Services</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li>
                  <a href="<?php echo base_url(); ?>Service_setup">Service Setup</a>
                </li>
                <li>
                  <a href="#">Service Setup 2</a>
                </li>
              </ul>
            </li>
            <li class="treeview <?php ($page == "people" || $page == "People") ? print 'active' : '' ?>">
               <a href="#">
                <i class="fa fa-users"></i>
                  <span>Contacts</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">

                <li>
                <a href="#">All</a>
                </li>

                <li>
                  <a href="<?php echo base_url(); ?>people">People</a>
                </li>
                <li>
                  <a href="#">Customers</a>
                </li>
                <li>
                <a href="#">Suppliers</a>
                </li>

              </ul>
            </li>
            <li class="treeview <?php ($page == "reports" || $page == "Reports") ? print 'active' : '' ?>">
               <a href="#">
                <i class="fa fa-wordpress"></i>
                  <span>Report</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li>
                  <a href="#">Contact 1</a>
                </li>
                <li>
                  <a href="#">Contact 2</a>
                </li>
              </ul>
            </li>

            <?php } ?>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">

      <script>
        $('#btn-logout').click(function() {
            // handle redirect here
            var link = $('#logo').data('href');

            location.href = link;
            $('#confirm-logout').modal('hide');
        });



          // an assumption is made here that the targeted div will have a static identifier, like class="navbar"
          // this initializes the navbar on screen load with an appropriate class
          if (window.innerWidth <= 767) {
            $(".navbar").addClass("navbar-fixed-top");
          } else {
            $(".navbar").addClass("navbar-static-top");
          }

          // if you want these classes to toggle when a desktop user shrinks the browser width to an xs width - or from xs to larger
          $(window).resize(function() {
            if (window.innerWidth <= 767) {
              $(".navbar").removeClass("navbar-static-top");
              $(".navbar").addClass("navbar-fixed-top");
            } else {
              $(".navbar").removeClass("navbar-fixed-top");
              $(".navbar").addClass("navbar-static-top");
            }
          });
      </script>