<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>Dashboard - <small>Control panel</small></h1>
    <ol class="breadcrumb">

      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

      <li class="active">Dashboard</li>

    </ol>


</section>



<!-- Main content -->
<section class="content">

  <!-- Small boxes (Stat box) -->

<?php

$flashdata= $this->session->flashdata('response');

if(!empty($flashdata)){

  if($flashdata['status'] == 'success'){

    ?>

    <div class="callout callout-success">

      <?php echo $flashdata['message']; ?>

    </div>

    <?php

  }

  if($flashdata['status'] == 'failed'){

    ?>

    <div class="callout callout-danger">

      <?php echo $flashdata['message']; ?>

    </div>

    <?php

  }

}

?>


<div class="row">
  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="info-box">

      <span class="info-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span>

      <div class="info-box-content">

        <span class="info-box-text">SALES</span>

        <span class="info-box-number">90<small>%</small></span>

      </div><!-- /.info-box-content -->

    </div><!-- /.info-box -->

  </div><!-- /.col -->

  <div class="col-md-3 col-sm-6 col-xs-12">

    <div class="info-box">

      <span class="info-box-icon bg-red"><i class="fa fa-google-plus"></i></span>

      <div class="info-box-content">

        <span class="info-box-text">COST OF <br>GOOD SOLD</span>

        <span class="info-box-number">41,410</span>

      </div><!-- /.info-box-content -->

    </div><!-- /.info-box -->

  </div><!-- /.col -->

  <!-- fix for small devices only -->

  <div class="clearfix visible-sm-block"></div>

    <div class="col-md-3 col-sm-6 col-xs-12">

      <div class="info-box">

        <span class="info-box-icon bg-green"><i class="ion ion-ios-people-outline  "></i></span>

        <div class="info-box-content">

          <span class="info-box-text">PAYROLL <br>COST OF<br>SERVICES</span>

          <span class="info-box-number">760</span>

        </div><!-- /.info-box-content -->

      </div><!-- /.info-box -->

    </div><!-- /.col -->

    <div class="col-md-3 col-sm-6 col-xs-12">

      <div class="info-box">

        <span class="info-box-icon bg-yellow"><i class="ion ion-ios-cart-outline"></i></span>

        <div class="info-box-content">

          <span class="info-box-text">OPERATING <br>EXPENSES</span>

          <span class="info-box-number">2,000</span>

        </div><!-- /.info-box-content -->

      </div><!-- /.info-box -->

    </div><!-- /.col -->
</div><!-- /.row -->

<div class="row">
  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="info-box">

      <span class="info-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span>

      <div class="info-box-content">

        <span class="info-box-text">GROSS PRPOFIT</span>

        <span class="info-box-number">90<small>%</small></span>

      </div><!-- /.info-box-content -->

    </div><!-- /.info-box -->

  </div><!-- /.col -->

  <div class="col-md-3 col-sm-6 col-xs-12">

    <div class="info-box">

      <span class="info-box-icon bg-red"><i class="fa fa-google-plus"></i></span>

      <div class="info-box-content">

        <span class="info-box-text">NET PROFIT</span>

        <span class="info-box-number">41,410</span>

      </div><!-- /.info-box-content -->

    </div><!-- /.info-box -->

  </div><!-- /.col -->

  <!-- fix for small devices only -->

  <div class="clearfix visible-sm-block"></div>

    <div class="col-md-3 col-sm-6 col-xs-12">

      <div class="info-box">

        <span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>

        <div class="info-box-content">

          <span class="info-box-text">Sales</span>

          <span class="info-box-number">760</span>

        </div><!-- /.info-box-content -->

      </div><!-- /.info-box -->

    </div><!-- /.col -->

    <div class="col-md-3 col-sm-6 col-xs-12">

      <div class="info-box">

        <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>

        <div class="info-box-content">

          <span class="info-box-text">New Members</span>

          <span class="info-box-number">2,000</span>

        </div><!-- /.info-box-content -->

      </div><!-- /.info-box -->

    </div><!-- /.col -->

</div><!-- /.row -->

  <!-- Main row -->
<div class="row">
<section class="col-lg-7 connectedSortable">
    <div class="box box-info">
      <div class="box-header">
        <i class="fa fa-envelope"></i>
        <h3 class="box-title">Quick Email</h3>
        <!-- tools box -->
        <div class="pull-right box-tools">
          <button class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
        </div><!-- /. tools -->
      </div>
      <div class="box-body">
        <form action="#" method="post">
          <div class="form-group">
            <input type="email" class="form-control" name="emailto" placeholder="Email to:">
          </div>
          <div class="form-group">
            <input type="text" class="form-control" name="subject" placeholder="Subject">
          </div>
          <div>
            <textarea class="textarea" placeholder="Message" style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
          </div>
        </form>
      </div>
      <div class="box-footer clearfix">
        <button class="pull-right btn btn-default" id="sendEmail">Send <i class="fa fa-arrow-circle-right"></i></button>
      </div>
    </div>
  </section>
</div>
</section><!-- /.content -->